import matplotlib.pyplot as plt

from Engines.SimpleMechanicsEngine import *
from Objects.Conectors.BasicWatcher import *


class SimplePlot(threading.Thread):

    def __init__(self, watcher:BasicWatcher):
        super(SimplePlot, self).__init__()
        self.watcher = watcher
        self.cancelled = False
        self.f = None
        self.xkey = ""
        self.ykey = ""

    def setKeys(self, xkey:str, ykey:str):
        self.xkey = xkey
        self.ykey = ykey

    def run(self):
        x = np.arange(-10.0, 80.0, 10)
        x2 = np.arange(-40.0, 40.0, 10)
        x3 = np.arange(-50.0, 10.0, 10)

        self.f = plt.figure()
        ax = self.f.gca()
        ax.set_title("x-y plot of the first mass point with 30Deg plane")
        self.f.show()
        while(not self.cancelled):
            #ax.plot(x,0.5*x+0.5, color="#31a354")
            #ax.plot(x3,-0.5*x3+0.5, color="#31a354")
            #ax.plot(x2,5+0*x2, color="#31a354")
            #ax.plot(np.transpose(self.watcher.listOfData)[0], np.transpose(self.watcher.listOfData)[1], color='#d95f0e')
            ax.plot(self.watcher.getObservedList(self.xkey), self.watcher.getObservedList(self.ykey), color='#d95f0e')
            self.f.canvas.draw()
            #time.sleep(0.01)

    def cancel(self):
        """End this timer thread"""
        self.cancelled = True
        f.close()