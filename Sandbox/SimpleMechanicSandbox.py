#class Name:  SimpleMechanicSandbox
#Author: djk

from Sandbox.BasicSandbox import *
from Objects.forceFields.BasicForceField import *
from Objects.Interactions.BasicInteraction import *

class SimpleMechanicSandbox(BasicSandbox):
    """
    Contains all Objects of the Simmulation
    """



    def __init__(self):
        """ constructor         """
        super( SimpleMechanicSandbox, self ).__init__()
        #List for global force field object like gravity, this fields acts on ALL object in the simmulation!
        self.globalForceFields = {}
        self.drawForceField = False
        #List of interaction Forces
        self.interactionForces = {}
        print("SimpleMechanicSandbox created")

    def reset(self, other):
        super( SimpleMechanicSandbox, self ).reset(other)
        self.globalForceFields.clear()
        self.globalForceFields.update(other.globalForceFields)
        self.interactionForces.clear()
        self.interactionForces.update(other.interactionForces)


#Manipulators
    def addNewForceField(self, newForceField):
        """ @brief Method to add an new object to the dictionary with the given id (this have to be unique!)
            @param  newObject """
        #if(newForceField==type(newForceField)):
        if(isinstance(newForceField,BasicInteraction)):
            self.interactionForces.update({newForceField.getObjectID():newForceField})
            print("add interaction", newForceField.getDescription(), "to Sandbox")
            return
        if(isinstance(newForceField, BasicForceField)):
            self.globalForceFields.update({newForceField.getObjectID():newForceField})
            print("add ForceField", newForceField.getDescription(), "to Sandbox")

    def getForceFieldList(self):
        return self.globalForceFields

    def getInteractionForceList(self):
        return self.interactionForces.values()

    def getDescription(self) -> str:
        """ @brief  intern description for debugging  """
        return "This is a simple Sandbox for newtonian mechanics!"

    def isForceFieldVisible(self):
        return self.drawForceField

    def hideForceField(self):
        self.drawForceField = False

    def showForceField(self):
        self.drawForceField = True

    def __str__(self):
        return self.getDescription()