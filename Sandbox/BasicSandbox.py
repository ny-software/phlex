#class Name:  BasicSandbox
#Author: djk

from Objects.BasicObject import *
import copy
from Objects.EventSubject import *
from Objects.MassPoint import *

class BasicSandbox:
    """
    Contains all Objects of the Simmulation
    """

    def __init__(self):
        """@brief constructor """
        #Dictionary of objects which are contained by the sandbox (does not include watchers)
        self.objectList = {}
        self.watcherList = {}
        self.modifierList = {}

        #the set of plotable objects, independently of the special data type
        self.guiElements = {}
        print("BasicSandbox created")



#Manipulators
    def reset(self, other):
        self.objectList.clear()
        self.objectList.update(other.objectList)
        self.watcherList.clear()
        self.watcherList.update(other.watcherList)
        self.modifierList.clear()
        self.modifierList.update(other.modifierList)
        self.guiElements.clear()
        self.guiElements.update(other.guiElements)
        for obj in self.guiElements.values():
            obj.reset()

    def initializeObjects(self) -> None:
        """
        all objects will be initialized, this is called before the simulation starts
        """
        for obj in self.objectList.values():
            obj.initialize()


    def getObjectList(self) -> dict:
        return self.objectList

    def getListOfGUIElements(self) -> dict:
        return self.guiElements

    def setListOfGUIElements(self, newGUI):
        self.guiElements.clear()
        self.guiElements.update(newGUI)

    def deleteObject(self, obj):
        if(obj != None):
            del self.objectList[obj.getObjectID()]

    def deleteObjects(self, objList):
        for obj in objList:
            del self.objectList[obj.getObjectID()]

    def selectObject(self, obj):
        try:
            obj.selected = True
        except:
            print("in Basic Sandbox: no object to select")
            print(obj)

    def deselectAllObjects(self):
        for obj in self.getObjectList().values():
            self.deselectObject(obj)

    def deselectObject(self, obj):
        try:
            obj.selected = False
        except:
            pass

    def getWatcherList(self):
        return self.watcherList

    def getModifierList(self):
        return self.modifierList

    def getWatcherByName(self, name):
        for obj in self.getWatcherList().values():
            if(obj.getObjectID() == name):
                return obj

    def getModifierByName(self, name):
        for obj in self.getModifierList().values():
            if(obj.getObjectID() == name):
                return obj

    def getObjectById(self, id):
        for obj in self.getObjectList().values():
            if(obj.getObjectID() == id):
                return obj
        return None


    def addNewObject(self, newObject: BasicObject):
        """ @brief Method to add an new object to the dictionary with the given id (this have to be unique!)
            @param  newObject """
       # if(newObject is BasicObject):
        self.objectList.update({newObject.getObjectID():newObject})
        print("add Object", newObject.getDescription(), "to Sandbox with the ID:", newObject.getObjectID())

    def addNewWatcher(self, newWatcher):
        self.watcherList.update({newWatcher.getObjectID():newWatcher})
        print("add Watcher", newWatcher.getDescription(), "to Sandbox with the ID:", newWatcher.getObjectID())

    def addNewModifier(self, newModifier):
        self.modifierList.update({newModifier.getObjectID():newModifier})
        print("add Modifier", newModifier.getDescription(), "to Sandbox with the ID:", newModifier.getObjectID())

    def getDescription(self) -> str:
        """ @brief  intern description for debugging  """
        return "This is a basic Sandbox!"

    def __str__(self):
        return self.getDescription()