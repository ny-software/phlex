from distutils.core import setup
import py2exe, sys

sys.argv.append('py2exe')

setup(
	options={'py2exe': {'bundle_files':1, "includes":["numpy", "PySide"]} },
	windows=[{'script':'ITA PhleX Demo.py'}],
	zipfile=None
)