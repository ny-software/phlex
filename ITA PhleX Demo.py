from PySideGui.mainViewer import *
import sys


def main():
    app = QtGui.QApplication(sys.argv)
    app.setStyle('cleanlooks')
    ex = ITAMainWindow(app)
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
