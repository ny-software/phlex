
from PySide import QtGui
from PySide.QtCore import *
from Objects.Conectors.BasicAnchor import *
from PySideGui.Visualisation.ObjectVisualisation import *

class BasicAnchorVisualisation:

    size = 50

    def __init__(self):

        self.linewidth = 4
        self.linewidthSel = 4
        self.pen = QtGui.QPen(QtGui.QColor(20, 20, 20), self.linewidth, QtCore.Qt.SolidLine)
        self.penSel = QtGui.QPen(QtGui.QColor(20, 20, 200), self.linewidthSel, QtCore.Qt.SolidLine)
        self.bgColor = QtGui.QColor(200,50,30)
        self.bgColorSel = QtGui.QColor(100,200,100)

        self.velocityPen = QtGui.QPen(QtGui.QColor(20, 20, 200), 5, QtCore.Qt.SolidLine)
        self.forcePen = QtGui.QPen(QtGui.QColor(200, 20, 20), 5, QtCore.Qt.SolidLine)
        self.velocityVectorScale = 1.0/5.0
        self.forceVectorScale = 2.0/10.0


    def isInside(self,obj:MassPoint, x, y, wProp):
        x=(x-wProp.offset[0])/wProp.scale[0]
        y=(y-wProp.offset[1])/wProp.scale[1]
        com = obj.getCenterOfMass()
        if ( (x-com[0])*(x-com[0]) + (y-com[1])*(y-com[1]) < self.mpSize*self.mpSize/4.0 ):
            return True
        return False

    def draw(self,anchor:BasicAnchor, qp:QtGui.QPainter, wProp):
        if(anchor.visible == False):
            if(anchor.getOwnerObject().selected == False):
                return
        com = anchor.getCoordinates()*wProp.scale + wProp.offset

        if(anchor.selected):
            qp.setPen(self.penSel)
            qp.setBrush(self.bgColorSel)
        else:
            qp.setPen(self.pen)
            qp.setBrush(self.bgColor)
        lengthx = self.size * wProp.scale[0]
        lengthy = self.size * wProp.scale[1]
        qp.drawEllipse (com[0]-0.5*lengthx, com[1]-0.5*lengthy, lengthx, lengthy)
        qp.drawLine(QPointF(com[0]-0.707*lengthx/2, com[1]-0.707*lengthy/2),QPointF(com[0]+0.707*lengthx/2, com[1]+0.707*lengthy/2))
        qp.drawLine(QPointF(com[0]+0.707*lengthx/2, com[1]-0.707*lengthy/2),QPointF(com[0]-0.707*lengthx/2, com[1]+0.707*lengthy/2))

