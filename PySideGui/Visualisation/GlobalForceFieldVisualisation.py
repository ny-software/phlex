
from PySide import QtGui, QtCore
from PySideGui.Visualisation.ObjectVisualisation import *


class GlobalForceFieldVisualisation:

    def __init__(self):
        self.linewidth = 2
        self.distance=50
        self.pen = QtGui.QPen(QtGui.QColor(254,196,79), self.linewidth, QtCore.Qt.SolidLine)

        self.forceScaling = 1.0/5

        self.visible=False
        self.forceFieldArray = []
        self.qtPointList = []
        self.windowProperties = None

    def show(self):
        self.visible=True

    def hide(self):
        self.visible=False

    def isVisible(self):
        return self.visible

    def generateArrowPointList(self, point, direction):
        p = np.array(point)
        d = np.array(direction)
        vA = p-0.5*d
        vB = p+0.5*d
        self.qtPointList.extend([QPointF(vA[0], vA[1]),QPointF(vB[0], vB[1])])
        v2 = -0.3*d
        v2n = np.array([v2[1], -v2[0]])
        vC = p+0.2*d+v2n
        vB2 = p+0.55*d
        vD = p+0.2*d-v2n
        self.qtPointList.extend([QPointF(vC[0], vC[1]),QPointF(vB2[0], vB2[1]),QPointF(vB2[0], vB2[1]),QPointF(vD[0], vD[1])])


    def calculateForceField(self, listOfForceFields:list, wProperties:WindowProperties):
        self.qtPointList=[]
        self.windowProperties= wProperties
        for x in range(int(self.distance/2),int(wProperties.width), int(self.distance)):
            for y in range(int(self.distance/2),int(wProperties.height), int(self.distance)):
                tmpForce = 0.0
                for forcefield in listOfForceFields.values():
                    tmpForce += self.forceScaling*forcefield.getForce([(x-self.windowProperties.offset[0])/self.windowProperties.scale[0],(y-self.windowProperties.offset[1])/self.windowProperties.scale[1]])
                try:
                    self.generateArrowPointList([x,y],[tmpForce[0], -tmpForce[1]]) #direction visualisation is different in the y direction
                except:
                    pass
    def draw(self, qp:QtGui.QPainter):
        if(self.isVisible()):
            qp.setPen(self.pen)
            qp.drawLines(self.qtPointList)
