
from PySide import QtGui, QtCore
from Objects.MassPoint import *
from PySideGui.Visualisation.ObjectVisualisation import *
from PySideGui.Visualisation.Connectors.BasicAnchorVisualisation import *

class MassPointVisulaisation:

    mpSize = 15

    def __init__(self, objVis):

        self.objVis = objVis

        self.linewidth = 2
        self.linewidthSel = 4
        self.pen = QtGui.QPen(QtGui.QColor(20, 20, 20), self.linewidth, QtCore.Qt.SolidLine)
        self.penSel = QtGui.QPen(QtGui.QColor(20, 20, 200), self.linewidthSel, QtCore.Qt.SolidLine)
        self.bgColor = QtGui.QColor(100,100,100)
        self.bgColorSel = QtGui.QColor(100,200,100)

        self.velocityPen = QtGui.QPen(QtGui.QColor(20, 20, 200), 10, QtCore.Qt.SolidLine, Qt.RoundCap)
        self.forcePen = QtGui.QPen(QtGui.QColor(200, 20, 20), 10, QtCore.Qt.SolidLine, Qt.RoundCap)
        self.velocityVectorScale = 5.0/1.0
        self.forceVectorScale = 2.0/10.0

        self.basicAnchorVis = BasicAnchorVisualisation()

    def generateArrow(self, point, direction):
        replacment = 0.0
        arrowList = []
        p = np.array(point)
        d = np.array(direction)
        vA = p-(replacment)*d
        vB = p+(1.0-replacment)*d
        arrowList.extend([QtCore.QPointF(vA[0], vA[1]),QtCore.QPointF(vB[0], vB[1])])
        v2 = -0.3*d
        v2n = np.array([v2[1], -v2[0]])
        vC = p+(1.0-replacment-0.3)*d+v2n
        vB2 = p+(1.05-replacment)*d
        vD = p+(1.0-replacment-0.3)*d-v2n
        arrowList.extend([QtCore.QPointF(vC[0], vC[1]),QtCore.QPointF(vB2[0], vB2[1]),QtCore.QPointF(vB2[0], vB2[1]),QtCore.QPointF(vD[0], vD[1])])
        return arrowList

    def isInside(self,obj:MassPoint, x, y, wProp):
        x=(x-wProp.offset[0])/wProp.scale[0]
        y=(y-wProp.offset[1])/wProp.scale[1]
        com = obj.getCenterOfMass()
        if ( (x-com[0])*(x-com[0]) + (y-com[1])*(y-com[1]) < self.mpSize*self.mpSize/4.0 ):
            return True
        return False

    def draw(self,mp:MassPoint, qp:QtGui.QPainter, wProp):
        com = mp.getCenterOfMass()*wProp.scale + wProp.offset

        if(mp.selected):
            qp.setPen(self.penSel)
            qp.setBrush(self.bgColorSel)
        else:
            qp.setPen(self.pen)
            qp.setBrush(self.bgColor)
        lengthx = self.mpSize * wProp.scale[0]
        lengthy = self.mpSize * wProp.scale[1]
        try:
            qp.drawEllipse (com[0]-0.5*lengthx, com[1]-0.5*lengthy, lengthx, lengthy)
        except:
            print("Error in Masspoint Visualisation")
        if(mp.isCenterOfMassForceVectorVisible()):
            qp.setPen(self.forcePen)
            comF = mp.getCenterOfMassForce()*wProp.scale
            qp.drawLines(self.generateArrow(com, comF*self.forceVectorScale))
        if(mp.isCenterOfMassVelocityVectorVisible()):
            qp.setPen(self.velocityPen)
            comV = mp.getCenterOfMassVelocity()*wProp.scale
            qp.drawLines(self.generateArrow(com, comV*self.velocityVectorScale))
        if(mp.hasAnchors):
            for anchor in mp.anchors.values():
                self.basicAnchorVis.draw(anchor, qp, wProp)

