

from PySide import QtGui, QtCore
from Objects.forceFields.CentralGravity import *

class CentralGravityVisualisation:

    mpSize = 10

    def __init__(self,objVis):

        self.objVis = objVis

        self.pen = QtGui.QPen(QtGui.QColor(20, 20, 20), 1, QtCore.Qt.SolidLine)
        self.penSel = QtGui.QPen(QtGui.QColor(200, 200, 200), 4, QtCore.Qt.SolidLine)
        self.bgColor = QtGui.QColor(0,0,100)
        self.bgColorSel = QtGui.QColor(200,0,0)

    def isInside(self,obj:CentralGravity, x, y, wProp):
        print("inside")
        x=(x-wProp.offset[0])/wProp.scale[0]
        y=(y-wProp.offset[1])/wProp.scale[1]
        print(x,y)
        com = obj.getCenter()
        if(com[0] - self.mpSize <  x < com[0]+self.mpSize):
            if(com[1] - self.mpSize <  y < com[1]+self.mpSize):
                return True

    def draw(self,obj:CentralGravity, qp:QtGui.QPainter, wProp):
        if(obj.selected):
            qp.setPen(self.penSel)
            qp.setBrush(self.bgColorSel)
        else:
            qp.setPen(self.pen)
            qp.setBrush(self.bgColor)
        lengthx = self.mpSize * wProp.scale[0]
        lengthy = self.mpSize * wProp.scale[1]
        com = obj.getCenter()*wProp.scale + wProp.offset
        qp.drawEllipse (com[0]-0.5*lengthx, com[1]-0.5*lengthy, lengthx, lengthy)
