
from PySide import QtGui, QtCore
from Objects.Floor import *
from PySide.QtGui import *
from PySide.QtCore import *

class FloorVisulaisation:

    mpSize = 35

    def __init__(self,objVis):

        self.objVis = objVis

        self.pen = QtGui.QPen(QtGui.QColor(20, 20, 20), 1, QtCore.Qt.SolidLine)
        self.penSel = QtGui.QPen(QtGui.QColor(20, 20, 20), 2, QtCore.Qt.SolidLine)
        self.bgColor = QtGui.QColor(49,163,84)
        self.bgColorSel = QtGui.QColor(49,163,84*2)

        self.bgColorPoint = QtGui.QColor(150, 150, 150)

    def isInside(self,obj:Floor, x, y, wProp):
        x=(x-wProp.offset[0])/wProp.scale[0]
        y=(y-wProp.offset[1])/wProp.scale[1]
        direction = obj.getDirectionVector()
        projectionPostition = x * direction[0] + y*direction[1]
        if(projectionPostition < -obj.getPlaneParameter()):
            return True
        return False

    def draw(self,floor:Floor, qp:QtGui.QPainter, wProp):
        #ToDo: improve textureing!!!
        if(floor.selected):
            qp.setPen(self.penSel)
            qp.setBrush(self.bgColorSel)
        else:
            qp.setPen(self.pen)
            qp.setBrush(self.bgColor)

        point = floor.getSpecialPointPlain()*wProp.scale + wProp.offset
        direction = floor.getDirectionVector()* wProp.scale

        pointList = []
        textureRect = QRectF(0,0,500,500)
        angle = 0.0
        trans = [0.0,0.0]
        if(direction[0] > 0):
            if (direction[1] > 0 and direction[0] != 0):
                A = (point[0]+direction[1]/direction[0]*point[1], 0)
                B = (0, point[1]+direction[0]/direction[1]*point[0])
                pointList.extend([QPointF(A[0], A[1]) , QPointF(0, 0) , QPointF(B[0], B[1])])
                if(floor.hasTexture):
                    trans=A
                    angle = angleVec(direction)+90
            if (direction[1] < 0 and direction[0] != 0):
                A = (point[0]-direction[1]/direction[0]*(wProp.height-point[1]), wProp.height)
                B = (0, point[1]+direction[0]/direction[1]*(point[0]))
                pointList.extend([QPointF(A[0], A[1]) , QPointF(0, wProp.height) , QPointF(B[0], B[1])])
                if(floor.hasTexture):
                    trans=B
                    angle = angleVec(direction)+90+180
            if (direction[1] == 0):
                A = np.array([point[0], 0.0])
                B = np.array([point[0], wProp.height])
                pointList.extend([QPointF(A[0], A[1]) , QPointF(B[0], B[1]),  QPointF(0, wProp.height) ,QPointF(0, 0)  ])
                if(floor.hasTexture):
                    trans=point
                    angle = 90
        if(direction[0] < 0):
            if (direction[1] > 0 and direction[0] != 0):
                A = (point[0]+direction[1]/direction[0]*point[1], 0)
                B = (wProp.width, point[1]-direction[0]/direction[1]*(wProp.width-point[0]))
                pointList.extend([QPointF(A[0], A[1]) , QPointF(wProp.width, 0) , QPointF(B[0], B[1])])
                if(floor.hasTexture):
                    trans=A
                    angle = angleVec(direction)+90
            if (direction[1] < 0 and direction[0] != 0):
                A = (point[0]-direction[1]/direction[0]*(wProp.height-point[1]), wProp.height)
                B = (wProp.width, point[1]-direction[0]/direction[1]*(wProp.width-point[0]))
                pointList.extend([QPointF(A[0], A[1]) , QPointF(wProp.width, wProp.height) , QPointF(B[0], B[1])])
                if(floor.hasTexture):
                    trans=A
                    angle = angleVec(direction)+90
            if (direction[1] == 0):
                A = np.array([point[0], 0.0])
                B = np.array([point[0], wProp.height])
                pointList.extend([QPointF(A[0], A[1]) , QPointF(B[0], B[1]),  QPointF(wProp.width, wProp.height) ,QPointF(wProp.width, 0)  ])
                if(floor.hasTexture):
                    trans=point
                    angle = -90


        if (direction[0] == 0 and direction[1] > 0):
            pointList.extend([QPointF(0,0) , QPointF(0, point[1])  , QPointF(wProp.width, point[1]) , QPointF(wProp.width,0)])
            if(floor.hasTexture):
                trans=np.array([0, point[1]])
                angle = 180
        if (direction[0] == 0 and direction[1] < 0):
            pointList.extend([QPointF(0.0,wProp.height),QPointF(0.0, point[1]), QPointF(wProp.width, point[1]) , QPointF(wProp.width,wProp.height)])
            if(floor.hasTexture):
                trans=np.array([0, point[1]])

        polygon = QPolygonF(pointList)
        qp.drawPolygon(polygon)

        if(floor.hasTexture):
            qp.translate(trans[0], trans[1])
            qp.rotate(angle)
            textureRepetition = 4
            textureRect = QRectF(QPointF(-wProp.width,0), QSizeF(4*wProp.width, 2*wProp.height))
            #self.objVis.getGuiSettings().getTexture(floor.getTextureId()).drawRepeted(qp, textureRect, textureRepetition)
            self.objVis.getGuiSettings().getTexture(floor.textureId).drawRepeted(qp, textureRect, textureRepetition)
            qp.resetTransform()

        if(floor.selected):
            lengthx = self.mpSize
            lengthy = self.mpSize
            qp.setBrush(self.bgColorPoint)
            qp.drawEllipse (point[0]-0.5*lengthx, point[1]-0.5*lengthy, lengthx, lengthy)