from PySide import QtGui, QtCore

from Objects.Interactions.HarmonicSpring import *
import math


class HarmonicSpringVisualisation:

    mpSize = 20

    def __init__(self,objVis):

        self.objVis = objVis

        self.linewidth=4
        self.pen = QtGui.QPen(QtGui.QColor(60, 60, 60),self.linewidth, QtCore.Qt.SolidLine)
        self.bgColor = QtGui.QColor(0,0,0)

    def draw(self,obj:HarmonicSpring, qp:QtGui.QPainter, wProp):
        qp.setPen(self.pen)

        posA = obj.getAnchorPos(0)*wProp.scale+wProp.offset
        posB = obj.getAnchorPos(1)*wProp.scale+wProp.offset



        if(obj.hasTexture):
            #obj.getTexture().draw(qp, QtCore.QRectF(com[0]-0.5*lengthx, com[1]+0.5*lengthy, lengthx, -lengthy))
            texture = self.objVis.getGuiSettings().getTexture(obj.textureId)
            if(texture is not None and obj.springConstant != 0.0):

                angle = math.atan2( posB[1]-posA[1], posB[0]-posA[0])
                width = 5*math.sqrt(obj.springConstant)
                if(posB[0]<posA[0]):
                    sigx = 1
                else:
                    sigx = -1
                if(posB[1]>posA[1]):
                    sigy = 1
                else:
                    sigy = -1

                if(sigx*sigy>0):
                    x1 = posA[0]+width*math.sin(angle)
                    x2 = posB[0]-width*math.sin(angle)
                    y1 = posA[1]+width*math.cos(angle)
                    y2 = posB[1]-width*math.cos(angle)
                    #p1 = QtCore.QPointF(posA[0]+width*math.sin(angle), posA[1]+width*math.cos(angle))
                    #p2 =  QtCore.QPointF(posB[0]-width*math.sin(angle), posB[1]-width*math.cos(angle))
                    p1 = QtCore.QPointF(min(x1,x2), min(y1, y2))
                    p2 = QtCore.QPointF(max(x1,x2), max(y1, y2))
                else:
                    x1 = posA[0]-width*math.sin(angle)
                    x2 = posB[0]+width*math.sin(angle)
                    y1 = posA[1]-width*math.cos(angle)
                    y2 = posB[1]+width*math.cos(angle)
                    #p1 = QtCore.QPointF(posA[0]-width*math.sin(angle), posA[1]-width*math.cos(angle))
                    #p2 =  QtCore.QPointF(posB[0]+width*math.sin(angle), posB[1]+width*math.cos(angle))
                    p1 = QtCore.QPointF(min(x1,x2), min(y1, y2))
                    p2 = QtCore.QPointF(max(x1,x2), max(y1, y2))
                rect = QtCore.QRectF( p1 ,p2)
                #qp.drawRect(rect)
                texture.draw(qp,rect , scaley = 1.0- width/(y2-y1)*cos(angle),  angle=angle*180/math.pi)
        else:
            qp.setBrush(self.bgColor)
            try:
                qp.drawLine (posA[0], posA[1], posB[0], posB[1])
            except:
                print("error in drawing of HarmonicSpringVisualisation")