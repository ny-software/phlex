from PySideGui.GuiSettings import *
from PySideGui.Visualisation.CentralGravityVisualisation import *
from PySideGui.Visualisation.CircleObjectVisualisation import *
from PySideGui.Visualisation.FloorVisualisation import *
from PySideGui.Visualisation.HarmonicSpringVisualisation import *



class ObjectVisualisation:
    def __init__(self, guiSettings:GlobalGuiSettings):
        self.massPointVis = MassPointVisulaisation(self)
        self.circleObjectVis = CircleObjectVisualisation(self)
        self.floorVis = FloorVisulaisation(self)

        self.harmonicSpringVis = HarmonicSpringVisualisation(self)

        self.centralGravityVis = CentralGravityVisualisation(self)

        self.guiSettings = guiSettings

    def getGuiSettings(self):
        return self.guiSettings

    def isInside(self, obj, x:float, y:float, wProperties:WindowProperties):
        if type(obj) == MassPoint:
            return self.massPointVis.isInside(obj, x, y, wProperties)
        if type(obj) == CircleObject:
            return self.circleObjectVis.isInside(obj, x, y, wProperties)
        if type(obj) == Floor:
            return self.floorVis.isInside(obj, x, y, wProperties)
        if type(obj) == CentralGravity:
            return self.centralGravityVis.isInside(obj, x, y, wProperties)

    def drawObject(self,obj, qp:QtGui.QPainter, wProperties:WindowProperties):
        if type(obj) == MassPoint:
            self.massPointVis.draw(obj, qp, wProperties)
            return
        if type(obj) == CircleObject:
            self.circleObjectVis.draw(obj, qp, wProperties)
            return
        if type(obj) == Floor:
            self.floorVis.draw(obj, qp, wProperties)
            return
        if type(obj) == CentralGravity:
            self.centralGravityVis.draw(obj, qp, wProperties)
            return
        if type(obj) == HarmonicSpring:
            self.harmonicSpringVis.draw(obj, qp, wProperties)
            return
