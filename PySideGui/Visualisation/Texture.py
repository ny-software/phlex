from PySide import QtGui, QtCore
import sys
import numpy as np


#ToDo: masking:
#https://stackoverflow.com/questions/7539257/how-to-use-a-mask-with-qpainter


class Texture:

    def __init__(self, path=None):
        self.image = None
        self.imageMask = None
        self.imagePath = path
        self.size = None
        self.circleClipedImage = None
        self.maskedSize = None
        self.maskLoaded = False

        #offset and Scale from the object rectangle to the drawing rectanle due to masking
        self.maskScale = [1.0, 1.0]
        self.maskOffset = [0, 0]

        self.repeatedImage = {}
        try:
            self.loadImage(path)
            self.repeatedImage.update({1:self.image})
        except:
            print("[Texture] error in loading texture: ", self.imagePath)

        try:
            self.loadImageMask(path+".mask.png")
            self.calculateMaxSizeFromMask()
        except:
            print("[Texture] texture mast cound not be loaded: ", self.imagePath)

    def hasMask(self) -> bool:
        """
        was it possible to load a mask for this texture?
        :return true if a mask was loaded, false otherwise
        """
        return self.maskLoaded


    def transformMaskRect(self, rect:QtCore.QRect) -> QtCore.QRect:
        Fx = rect.width()/self.maskedSize.width()
        Fy = rect.height()/self.maskedSize.height()
        return QtCore.QRect(rect.x()-self.maskedSize.x()*Fx, rect.y()-self.maskedSize.y()*Fy, self.size.width()*Fx, self.size.height()*Fy)

    def getImage(self) -> QtGui.QImage:
        return self.image

    def loadImage(self, path:str):
        self.image = QtGui.QImage(path)
        self.size = self.image.size()
        print("[Texture] Texture loaded: ", path)

    def loadImageMask(self, path:str):
        self.imageMask = QtGui.QImage(path)
        print("[Texture] Texture Mask loaded: ", path, self.maskedSize)

    def calculateMaxSizeFromMask(self):
        if(self.imageMask.isNull()):
            return
        #ToDo: improve this
        #from left
        pixel = 1
        x = 0
        y = 0
        #while pixel != 0 or x!=self.maskedSize.height():
        #   for y in range(self.maskedSize.height()):
        #        print(x,y,pixel)
        #        pixel = self.imageMask.pixel(QtCore.QPoint(x,y))#

#                if pixel == 0:
 #                   break

  #      print("[Texture] Left bound: ", x)
        #white: 4294967295
        #black: 4278190080
        #from left
        for x in range(self.size.width()):
            for y in range(self.size.height()):
                pixel = QtGui.QColor(self.imageMask.pixel(QtCore.QPoint(x,y))).blackF()
                if pixel > 0.5:
                    break
            if pixel > 0.5:
                break
        #print("[Pixel] in ", self.imagePath, " is " , pixel, " at x ", x)
        x0=x

        #from right
        for x in reversed(range(self.size.width())):
            for y in range(self.size.height()):
                pixel = QtGui.QColor(self.imageMask.pixel(QtCore.QPoint(x,y))).blackF()
                if pixel > 0.5:
                    break
            if pixel > 0.5:
                break
        x1=x
        #print("[Pixel] in ", self.imagePath, " is " , pixel, " at x ", x)

        #from top
        for y in range(self.size.height()):
            for x in range(self.size.width()):
                pixel = QtGui.QColor(self.imageMask.pixel(QtCore.QPoint(x,y))).blackF()
                if pixel > 0.5:
                    break
            if pixel > 0.5:
                break
        y0=y
        #print("[Pixel] in ", self.imagePath, " is " , pixel, " at y ", y)

        #from bottom
        for y in reversed(range(self.size.height())):
            for x in range(self.size.width()):
                pixel = QtGui.QColor(self.imageMask.pixel(QtCore.QPoint(x,y))).blackF()
                if pixel > 0.5:
                    break
            if pixel > 0.5:
                break
        y1=y
        #print("[Pixel] in ", self.imagePath, " is " , pixel, " at y ", y)

        self.maskedSize = QtCore.QRect(x0, y0, x1-x0, y1-y0)
        print("[Texture] Mask measuremnts: ", x0, y0, x1, y1)
        self.maskLoaded = True


    def clipCircle(self):
        #ToDo: simplify, this is very inefficient

        #create mask
        self.circlePixMap = QtGui.QPixmap(self.size)
        self.circlePixMap.fill(QtGui.QColor(255,255,255))
        painter = QtGui.QPainter(self.circlePixMap)
        painter.setBrush(QtGui.QColor(0,0,0))
        painter.drawEllipse(QtCore.QRectF(QtCore.QPointF(0.0,0.0), self.size))

        #create cliped image
        self.imageClipPixMap = QtGui.QPixmap(self.size)
        #dont forget alpha=0!
        self.imageClipPixMap.fill(QtGui.QColor(0,0,0,0))
        painter2 = QtGui.QPainter(self.imageClipPixMap)
        painter2.setClipRegion(QtGui.QRegion(self.circlePixMap))
        painter2.drawImage(QtCore.QRectF(QtCore.QPointF(0.0,0.0), self.size),  self.image)
        #generate image
        self.circleClipedImage = self.imageClipPixMap.toImage()

    def generateRepeatedImage(self, N):
        self.repeatPM = QtGui.QPixmap(self.size*N)
        self.repeatPM.fill(QtGui.QColor(255,255,255))
        painter = QtGui.QPainter(self.repeatPM)
        for i in range(N):
            for j in range(N):
                painter.drawImage(QtCore.QRectF(QtCore.QPointF(i*self.size.width(),j*self.size.height()), self.size),  self.image)
        self.repeatedImage.update({N : self.repeatPM.toImage()})


    def draw(self, qp,  rect, scalex = 1.0, scaley = 1.0, angle=0):
        try:
            #if the object has a mask, rescale
            if(self.hasMask()):
                rect2 = self.transformMaskRect(rect)
                qp.drawImage(rect2,  self.circleClipedImage)
            if(angle==0):
                qp.drawImage(rect,  self.image)
            else:
                tmpimg = self.image.transformed(QtGui.QTransform().scale(scalex, scaley))
                qp.drawImage(rect,  tmpimg.transformed(QtGui.QTransform().rotate(angle)))
            #qp.drawPixmap(rect, self.imageClipPixMap)
        except:
            print("[Texture] problem in drawing texture (draw)", self)

    def drawCircleCliped(self, qp:QtGui.QPainter, rect):
        if(self.circleClipedImage is None):
            #create a circular cliped version
            self.clipCircle()
        try:
            #if the object has a mask, rescale
            if(self.hasMask()):
                rect2 = self.transformMaskRect(rect)
                qp.drawImage(rect2,  self.image)
            else:
                qp.drawImage(rect,  self.circleClipedImage)
        except:
            print("[Texture] problem in drawing texture (drawCircleCliped)", self)
            print(sys.exc_info()[0])

    def drawRepeted(self, qp, rect, N):
        if(self.repeatedImage.get(N) is None):
            self.generateRepeatedImage(N)
            print("blabla")
        try:
            #if the object has a mask, rescale
            if(self.hasMask()):
                rect = self.transformMaskRect(rect)
            qp.drawImage(rect,  self.repeatedImage.get(N))
        except:
            print("[Texture] problem in drawing texture (drawRepeted)", self)
