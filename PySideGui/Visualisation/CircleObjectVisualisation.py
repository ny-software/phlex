


from PySide import QtGui, QtCore
from Objects.CircleObject import *
from PySideGui.Visualisation.MassPointVisulaisation import *


class CircleObjectVisualisation(MassPointVisulaisation):

    def __init__(self,objVis):

        super(CircleObjectVisualisation, self).__init__(objVis)
        self.linewidth = 2
        self.linewidthSel = 4
        self.pen = QtGui.QPen(QtGui.QColor(20, 20, 20), self.linewidth, QtCore.Qt.SolidLine)
        self.penSel = QtGui.QPen(QtGui.QColor(20, 20, 200), self.linewidthSel, QtCore.Qt.SolidLine)
        self.bgColor = QtGui.QColor(100,100,100)
        self.bgColorSel = QtGui.QColor(100,200,100)


        self.massPointVisualisation = MassPointVisulaisation(self.objVis)

    def isInside(self,obj:CircleObject, x, y, wProp):
        self.size=obj.radius*2.0
        x=(x-wProp.offset[0])/wProp.scale[0]
        y=(y-wProp.offset[1])/wProp.scale[1]
        com = obj.getCenterOfMass()
        if ( (x-com[0])*(x-com[0]) + (y-com[1])*(y-com[1]) < obj.radius*obj.radius ):
            return True
        return False


    def draw(self,obj:CircleObject, qp:QtGui.QPainter, wProp):
        self.size=obj.radius*2.0
        lengthx = self.size * wProp.scale[0]
        lengthy = self.size * wProp.scale[1]
        com = obj.getCenterOfMass()*wProp.scale + wProp.offset

        if(obj.hasTexture):
            #obj.getTexture().draw(qp, QtCore.QRectF(com[0]-0.5*lengthx, com[1]+0.5*lengthy, lengthx, -lengthy))
            texture = self.objVis.getGuiSettings().getTexture(obj.textureId)
            if(texture is not None):
                texture.drawCircleCliped(qp, QtCore.QRectF(com[0]-0.5*lengthx, com[1]+0.5*lengthy, lengthx, -lengthy))
                #texture.draw(qp, QtCore.QRectF(com[0]-0.5*lengthx, com[1]+0.5*lengthy, lengthx, -lengthy))
            if(obj.selected):
                qp.setPen(self.penSel)
                qp.setBrush(QtCore.Qt.NoBrush)
                qp.drawEllipse (com[0]-0.5*lengthx, com[1]-0.5*lengthy, lengthx, lengthy)

        else:
            if(obj.selected):
                qp.setPen(self.penSel)
                qp.setBrush(self.bgColorSel)
            else:
                qp.setPen(self.pen)
                qp.setBrush(self.bgColor)
            qp.drawEllipse (com[0]-0.5*lengthx, com[1]-0.5*lengthy, lengthx, lengthy)

        if(obj.isCenterOfMassForceVectorVisible()):
            qp.setPen(self.forcePen)
            comF = obj.getCenterOfMassForce()*wProp.scale
            if(obj.isCenterOfMassForceSplitted()):
                qp.drawLines(self.generateArrow(com, [self.forceVectorScale*comF[0],0.0]))
                qp.drawLines(self.generateArrow(com, [0.0, self.forceVectorScale*comF[1]]))
            else:
                qp.drawLines(self.generateArrow(com, comF*self.forceVectorScale))
        if(obj.isCenterOfMassVelocityVectorVisible()):
            qp.setPen(self.velocityPen)
            comV = obj.getCenterOfMassVelocity()*wProp.scale
            if(obj.isCenterOfMassVelocitySplitted()):
                qp.drawLines(self.generateArrow(com, [comV[0]*self.velocityVectorScale, 0.0]))
                qp.drawLines(self.generateArrow(com, [0.0, comV[1]*self.velocityVectorScale]))
            else:
                qp.drawLines(self.generateArrow(com, comV*self.velocityVectorScale))

        #if(obj.hasAnchors()):
        #    for anchor in obj.getAnchors():
        #        self.basicAnchorVis.draw(anchor, qp, wProp)


