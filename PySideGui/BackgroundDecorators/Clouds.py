from PySide import QtGui, QtCore
import random

from PySideGui.BackgroundDecorators.BasicBackgroundDecorator import *

class CloudsResources:
    def __init__(self, scaleHeight=50):

        self.paths = ["./images/cloud01.png", "./images/cloud02.png", "./images/cloud03.png",  "./images/cloud04.png",  "./images/cloud05.png",  "./images/cloud06.png",  "./images/cloud07.png",  "./images/cloud08.png",  "./images/cloud09.png",  "./images/cloud10.png"]
        self.images = []
        self.scaleHeight = scaleHeight


    def load(self):
        for p in self.paths:
            tmpImg = QtGui.QImage(p)
            try:
                self.images.append(tmpImg.scaledToHeight(self.scaleHeight))
            except:
                print("error 01 in CloudsResources")

    def getRandomImage(self):
        num = random.randrange(len(self.images))
        return self.images[num]

class Cloud:

    def __init__(self, windowProperties):

        self.windowProperties = windowProperties

        self.clouds = CloudsResources()
        self.clouds.load()

        self.image = None
        self.loadRandomImage()

        self.minSpeed=30
        self.maxSpeed=100


        self.time = 0.0

        self.initalized=False

    def initialize(self):
        self.speed = [random.randrange(self.minSpeed,self.maxSpeed,5), 0.0]
        self.position = [random.randrange(self.windowProperties.width), random.randrange(self.windowProperties.height)]


    def resetPosition(self):
        self.time=0
        self.position = [-random.randrange(int(0.1*self.windowProperties.width), int(0.4*self.windowProperties.width), 1), random.randrange(self.windowProperties.height)]
        self.loadRandomImage()

    def loadRandomImage(self):
        self.image = self.clouds.getRandomImage()

    def setTime(self, dt):
        self.time += dt

    def draw(self, qp:QtGui.QPainter):
        if(not self.initalized):
            self.initialize()
            self.initalized=True

        if(self.position[0]+self.speed[0]*self.time > self.windowProperties.width*1.1):
            self.resetPosition()

        if(self.image != None):
            qp.drawImage(QtCore.QPointF(self.position[0]+self.speed[0]*self.time,self.position[1]+self.speed[1]*self.time),  self.image)


class BackgroundDecoratorClouds(BasicBackgroundDecorator):

    def __init__(self, windowProperties, N=10):
        super(BackgroundDecoratorClouds, self).__init__(windowProperties)
        self.clouds = []
        self.N = N
        for i in range(self.N):
            self.clouds.append(Cloud(windowProperties))

    def setTime(self, dt):
        super(BackgroundDecoratorClouds, self).setTime(dt)
        for c in self.clouds:
            c.setTime(dt)

    def draw(self, qp):
        super(BackgroundDecoratorClouds, self).draw(qp)
        for c in self.clouds:
            c.draw(qp)