

class BasicBackgroundDecorator():

    def __init__(self, windowProperties, N=10):
        self.windowProperties = windowProperties
        self.time = 0

    def setTime(self, dt):
        self.time += dt

    def draw(self, qp):
        pass