from PySideGui.BackgroundDecorators.BasicBackgroundDecorator import *
from PySide import QtGui, QtCore

class MonochromeBackgroundDecorator(BasicBackgroundDecorator):

    def __init__(self, windowProperties, color:QtGui.QColor = QtGui.QColor(0,0,0)):
        super(MonochromeBackgroundDecorator, self).__init__(windowProperties)
        self.color = color


    def draw(self, qp):
        super(MonochromeBackgroundDecorator, self).draw(qp)
        qp.setBrush(self.color)
        qp.drawRect(QtCore.QRectF(QtCore.QPointF(0.0,0.0), QtCore.QSizeF(self.windowProperties.width, self.windowProperties.height)))