from PySideGui.BackgroundDecorators.BasicBackgroundDecorator import *
from PySide import QtGui, QtCore

class ImageBackgroundDecorator(BasicBackgroundDecorator):

    def __init__(self, windowProperties, guiSettings, imageid):
        super(ImageBackgroundDecorator, self).__init__(windowProperties)
        self.imageid = imageid
        self.guiSettings = guiSettings


    def draw(self, qp):
        super(ImageBackgroundDecorator, self).draw(qp)
        texture = self.guiSettings.getImage(self.imageid)
        if(texture is not None):
            texture.draw(qp, QtCore.QRectF(0, 0, self.windowProperties.width, self.windowProperties.height))