from Controller.CommandLineController import *
from PySideGui.GuiSettings import *
from PySideGui.PySideSimulationWidget import *
from Engines.EngineManager import *
from PySideGui.splashMenu import *

class Communicate(QtCore.QObject):

    updateBW = QtCore.Signal(int)

class ITAMainWindow(QtGui.QMainWindow):
    def __init__(self, app:QtGui.QApplication):
        super(ITAMainWindow, self).__init__(parent=None)
        self.app = app
        viewer = mainViewer(self)
        #self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        MainFrame = QtGui.QFrame(self)
        self.setCentralWidget(MainFrame)
        MainFrameLayout = QtGui.QVBoxLayout(MainFrame)
        #set the color layout
        palette = QtGui.QPalette()
        palette.setColor(QtGui.QPalette.Background, QtGui.QColor(255,255,255))
        palette.setColor(QtGui.QPalette.Foreground, QtGui.QColor(255,255,255))
        self.setPalette(palette)
        self.hideFrame()
        MainFrameLayout.addWidget(viewer)
        #set the possition
        availableGeometry = QRect(QApplication.desktop().availableGeometry())
        self.setGeometry((availableGeometry.width()-viewer.getButtonWidth())/2, (availableGeometry.height()-self.geometry().height())/2, 390, 210)

        self.show()

    def hideFrame(self):
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint | QtCore.Qt.WindowStaysOnTopHint)

    def showFrame(self):
        self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.FramelessWindowHint)
        self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowStaysOnTopHint)


    def onExit(self):
        sys.exit(self.app.exec_())
        pass


class mainViewer(QtGui.QWidget):

    def __init__(self, parent:ITAMainWindow):
        super(mainViewer, self).__init__()

        self.guiSettings = GlobalGuiSettings(GlobalGuiSettings.RUN_MODE_VIEWER)
        #self.enginemanager = EngineManager(self.mainSandbox)
        #self.enginemanager.attachEngine(self.engine)

        self.parent = parent
        self.initUI()

    def onExit(self):
        self.parent.onExit()

    def getButtonWidth(self):
        return self.buttonWidth

    def initUI(self):

#        self.c.updateBW[int].connect(self.wid.setValue)
        self.splashMenueWidget = splashMenue(self.guiSettings)
        self.splashMenueWidget.onDemoLoaded.connect(self.onDemoLoaded)
        self.splashMenueWidget.onExit.connect(self.onExit)
        self.buttonWidth = self.splashMenueWidget.getButtonWidth()

        self.hbox = QtGui.QHBoxLayout()
        #self.hbox.addWidget(self.simulationWidget)
        self.hbox.addWidget(self.splashMenueWidget)

        self.setLayout(self.hbox)

        #self.setGeometry(300, 300, 390, 210)
        #availableGeometry = QRect(QApplication.desktop().availableGeometry())
        #self.setGeometry((availableGeometry.width()-self.geometry().width())/2, (availableGeometry.height()-self.geometry().height())/2, 390, 210)
        self.parent.setWindowTitle(self.guiSettings.windowName)
        #self.show()

    def onDemoLoaded(self):
        print("on demo loaded")
        self.controller = self.splashMenueWidget.getEngineController()
        self.mainSandbox = self.splashMenueWidget.getSandbox()
        self.initSimulationUI()

    def initSimulationUI(self):
        self.parent.hide()
        self.model = PhlexApplicationModel(self.mainSandbox, self.controller)
        self.simulationWidget = PySideSimulationWidget(self.model, self.controller, self.guiSettings)

        palette = QtGui.QPalette()
        palette.setColor(QtGui.QPalette.Background, QtGui.QColor(18,34,41))
        palette.setColor(QtGui.QPalette.Foreground, QtGui.QColor(255,255,255))
        self.parent.setPalette(palette)

        #self.hbox = QtGui.QHBoxLayout()
        self.hbox.removeWidget(self.splashMenueWidget)
        self.hbox.addWidget(self.simulationWidget)
        self.setLayout(self.hbox)

        #maximize
        #availableGeometry = QRect(QApplication.desktop().availableGeometry())
        #self.setGeometry(availableGeometry)
        self.setGeometry(0,0,800,600)
        availableGeometry = QRect(QApplication.desktop().availableGeometry())
        self.parent.setGeometry((availableGeometry.width()-self.geometry().width())/2, (availableGeometry.height()-self.geometry().height()*1.3)/2, 800, 600)
        self.parent.showFrame()
        self.setWindowTitle(self.guiSettings.windowName)
        self.parent.show()

        timer = QTimer(self)
        self.connect(timer, SIGNAL("timeout()"), self.simulationWidget.animate)
        timer.start(1)  #timeout in ms


    def loadSimulationWindow(self):

        #load and or define sandbox an engine
        self.mainSandbox = SimpleMechanicSandbox()
        self.engine = SimpleMechanicsEngine()


        self.controller = CommandLineController(self.mainSandbox, self.engine)
#        self.controller.setupTestObjects()

        createTestSetupMechanicsMacke(self.mainSandbox)

        self.mainSandbox.setListOfGUIElements(listToDict(createTestSetupMechanicsMacke_GUI(self.controller, self.guiSettings, self.mainSandbox)))

        self.controller.initializeEngine(self.mainSandbox)


    def inputField_edit_text_changed(self):
        self.controller.processCommand(self.inputField.text())

    def mousePressEvent(self, event):
        print ("QGraphicsView mousePress")

    def mouseMoveEvent(self, event):
        print("QGraphicsView mouseMove")

    def mouseReleaseEvent(self, event):
        print("QGraphicsView mouseRelease")


