from PySide import QtGui
from PySideGui.MainEditorWidget import *
from Engines.SimpleMechanicsEngine import *
from Sandbox.SimpleMechanicSandbox import *
from Controller.CommandLineController import *
from Model import *
from Model.AbstractModel import *

class ApplicationWindow(QtGui.QMainWindow, ModelObserver):
    def __init__(self, model:PhlexApplicationModel, controller:CommandLineController):
        super(ApplicationWindow, self).__init__()
        # Set model and controller
        self.__model = model
        self.__model.registerObserver(self)
        self.__controller = controller

        self.simulationWidget = MainEditorWidget(self.__model, self.__controller)

        # Build menu bar
        menubar = self.menuBar()
        #   File menu
        fileMenu = self.__buildFileMenu()
        menubar.addMenu(fileMenu)
        #   Edit menu
        editMenu = self.__buildEditMenu()
        menubar.addMenu(editMenu)
        #   Help menu
        helpMenu = self.__buildHelpMenu()
        menubar.addMenu(helpMenu)

        # Build Toolbars
        #   Simulation
        self.simulationToolbar = self.__buildSimulationToolbar()
        self.addToolBar(self.simulationToolbar)
        #   Mouse mode
        self.mouseModeToolbar = self.__buildMouseModeToolbar()
        self.addToolBar(self.mouseModeToolbar)
        #   View
        self.viewToolbar = self.__buildViewToolbar()
        self.addToolBar(self.viewToolbar)

        # Add main widget
        self.setCentralWidget(self.simulationWidget)

        # Window stats and show
        self.setWindowTitle('ITA Phlex')
        self.show()

    def __buildHelpMenu(self) -> QtGui.QMenu:
        helpMenu = QtGui.QMenu('&Help')

        aboutAction = QtGui.QAction('&About', self)
        aboutAction.triggered.connect(self.showAboutDialog)
        helpMenu.addAction(aboutAction)

        return helpMenu

    def __buildEditMenu(self) -> QtGui.QMenu:
        editMenu = QtGui.QMenu('&Edit')

        # Undo/Redo actions
        undoAction = QtGui.QAction(QtGui.QIcon('edit-undo'), '&Undo', self)
        undoAction.setShortcut('Ctrl+Z')
        undoAction.triggered.connect(self.__controller.undoLastModificationAction)
        editMenu.addAction(undoAction)
        redoAction = QtGui.QAction(QtGui.QIcon('edit-redo'), '&Redo', self)
        redoAction.setShortcut('Ctrl+Shift+Z')
        redoAction.triggered.connect(self.__controller.redoLastModificationAction)
        editMenu.addAction(redoAction)

        return editMenu

    def __buildFileMenu(self) -> QtGui.QMenu:
        fileMenu = QtGui.QMenu('&File')

        exitAction = QtGui.QAction(QtGui.QIcon('exit.png'), '&Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.close)

        fileMenu.addAction(exitAction)
        return fileMenu

    def __buildViewToolbar(self) -> QtGui.QToolBar:
        viewToolbar = QtGui.QToolBar('View')
        # Show forcefields
        showForceField = QtGui.QAction('&Show Forcefield', self) #TODO: Add icon
        showForceField.triggered.connect(self.simulationWidget.ToggleForceFieldVisibility)
        viewToolbar.addAction(showForceField)

        return viewToolbar

    def __buildMouseModeToolbar(self) -> QtGui.QToolBar:
        mmToolbar = QtGui.QToolBar('Mouse Mode')

        # Object selection
        selectObject = QtGui.QAction(QtGui.QIcon.fromTheme("insert-object"), '&Select', self)
        selectObject.triggered.connect(self.simulationWidget.selectButtonSlot)
        mmToolbar.addAction(selectObject)

        # Object deletion
        deleteObject = QtGui.QAction(QtGui.QIcon.fromTheme("edit-delete"), 'Delete', self)
        deleteObject.triggered.connect(self.simulationWidget.deleteButtonSlot)
        mmToolbar.addAction(deleteObject)

        return mmToolbar

    def __buildSimulationToolbar(self) -> QtGui.QToolBar:
        simToolbar = QtGui.QToolBar('Simulation')

        # Start simulation action
        self.__startSimulationTbButton = QtGui.QAction(QtGui.QIcon.fromTheme("media-playback-start"), 'Start', self)
        self.__startSimulationTbButton.setShortcut('Ctrl+S')
        self.__startSimulationTbButton.triggered.connect(self.startStopSimulation)
        simToolbar.addAction(self.__startSimulationTbButton)

        # Reset Simulation Action
        resetSimulation = QtGui.QAction(QtGui.QIcon.fromTheme("view-refresh"), 'Reset', self)
        resetSimulation.setShortcut('Ctrl+R')
        resetSimulation.triggered.connect(self.resetSimulation)
        simToolbar.addAction(resetSimulation)

        return simToolbar

    def notifyModelChanged(self, notificationObject:PhlexModelNotificationObject):
        if notificationObject.simulationRunning:
            self.__startSimulationTbButton.setText("Pause")
        else:
            self.__startSimulationTbButton.setText("Play")

        if notificationObject.simulationReset:
            self.__startSimulationTbButton.setText("Start")

    def startStopSimulation(self):
        if self.__model.isRunning():
            self.__controller.stopSimulation()
        else:
            self.__controller.startSimulation()

    def resetSimulation(self):
        self.__controller.resetSimulation()

    def showAboutDialog(self):
        pass