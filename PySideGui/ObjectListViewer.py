from Controller.CommandLineController import *
from Objects.EventSubject import *
from Model.PhlexApplicationModel import PhlexApplicationModel

class ObjectListViewerListItem(QtGui.QListWidgetItem):

    def __init__(self, object:BasicObject):

        self.object = object
        QtGui.QListWidgetItem.__init__(self, str(object))

    def getObject(self):
        return self.object

    def isSelected(self):
        return self.object.isSelected()

    def __str__(self):
        return str(self.object)



class ObjectListViewer(QtGui.QListWidget, EventObserver):

    def __init__(self, model:PhlexApplicationModel, controller:CommandLineController):
        QtGui.QListWidget.__init__(self)
        EventObserver.__init__(self)

        self.model = model
        self.controller = controller

        self.itemClicked.connect(self.onItemClicked)

        self.resetList()


    def resetList(self):
        self.clear()
        for obj in self.model.sandbox.getObjectList().values(): #TODO: REWRITE! the controller must not hold data
            self.addItem(ObjectListViewerListItem(obj))


    def onItemClicked(self, item:QtGui.QListWidgetItem):
        print(item)
#        obj = self.controller.getObjectByName(item.getObjectID())
#        obj.getObject().setSelected(True)
        self.controller.selectObject(item.getObject())



    def notifyEvent(self, subject:EventSubjectPrototype):
        print("Event from Controller")
        self.resetList()