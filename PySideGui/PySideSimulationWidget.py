import time
from Controller.CommandLineController import *

from PySideGui.BackgroundDecorators.MonochromBackgroundDecorator import *
from PySideGui.BackgroundDecorators.ImageBackgroundDecorator import *
from PySideGui.GuiSettings import *
from PySideGui.GuiTools.SimpleInlinePlots import *
from PySideGui.GuiTools.SimpleSlider import *
from PySideGui.GuiTools.SimulationControl import *
from PySideGui.GuiTools.ValueIndicator import *
from PySideGui.Visualisation.GlobalForceFieldVisualisation import *
from PySideGui.GuiTools.MouseInstance import *
from Model.PhlexApplicationModel import PhlexApplicationModel
from Controller.BasicController import BasicController


class PySideSimulationWidget(QtGui.QWidget):
    def __init__(self, model:PhlexApplicationModel, controller:BasicController, guiSettings: GlobalGuiSettings):
        super(PySideSimulationWidget, self).__init__()

        self.setMouseTracking(True)
        self.mouseInstance = MouseInstance(guiSettings)

        self.model = model
        self.controller = controller
        self.guiSettings = guiSettings

        # scaling and offset of the view: how the data is reorderd in the visualisation
        self.windowProperties = self.guiSettings.getWindowProperties()
        self.windowProperties.width = self.guiSettings.initAnimationWindowWidth
        self.windowProperties.height = self.guiSettings.initAnimationWindowHeight
        self.mouseWheelScrollingFactor = self.guiSettings.mouseWheelScrollingFactor

        #set the list of user interfaces and initialise the GUI
        self.staticGUIElementList = {}
        self.initUI()

        self.objectVisualisation = ObjectVisualisation(self.guiSettings)

        self.forceFieldArray = []

        self.tempMousePosForDrag = [0, 0]

        self.mouseMode = GuiConstants.MOUSE_SELECTION_MODE
        self.mouseModeStored = self.mouseMode

        # object which is in dragging
        self.dragObject = None

        self.selectedObject = None

        self.calculateBackgroundForceField(self.model.sandbox.getForceFieldList())

        self.drawForceField = self.model.sandbox.isForceFieldVisible()
        self.forceFieldVisualisation = GlobalForceFieldVisualisation()
        self.forceFieldVisualisation.calculateForceField(self.model.sandbox.getForceFieldList(), self.windowProperties)

        # background:
        self.cloudsImageOriginal = QtGui.QImage("../images/cloud01.png")
        self.cloudsImage = self.cloudsImageOriginal.scaledToHeight(50)
        self.oldTime = time.clock()

        #self.backgroundDecorator = MonochromeBackgroundDecorator(self.windowProperties, QtGui.QColor(190,186,218))
        self.backgroundDecorator = guiSettings.getBackgroundDecorator()
        if(self.backgroundDecorator is None):
            self.backgroundDecorator = ImageBackgroundDecorator(self.windowProperties, self.guiSettings, GlobalConstants.IMAGE_ID_BACKGROUND_01)

        #mouse test
        ball = CircleObject(90)
        ball.setMass(200)
        #self.model.sandbox.addNewObject(ball)

        #self.mouseInstance.attachObjectToMouse(ball)

        self.storedMouseEventButtons = None

    def initUI(self):
        self.setMinimumSize(self.windowProperties.width, self.windowProperties.height)
        #Todo: They have to be used in the viewer mode, but not in the editor mode!
        #simulationContollerButtons = SimulationControl(self.controller, self.guiSettings, [10, 10])
        #self.staticGUIElementList.update({simulationContollerButtons.getObjectID():simulationContollerButtons})

    def updateSize(self):
        size = self.size()
        self.windowProperties.width = size.width()
        self.windowProperties.height = size.height()

    def updateObjects(self):
        self.forceFieldVisualisation.calculateForceField(self.model.sandbox.getForceFieldList(), self.windowProperties)
        # if(self.drawForceField):
        #   self.calculateBackgroundForceField(self.model.sandbox.getForceFieldList())

    def getObjectAt(self, x, y):
        for obj in self.model.sandbox.getListOfGUIElements().values():
            if (obj.isInside(x, y)):
                return obj

        for obj in self.model.sandbox.getObjectList().values():
            if (self.objectVisualisation.isInside(obj, x, y, self.windowProperties)):
                return obj

        for obj in self.model.sandbox.getForceFieldList().values():
            if (self.objectVisualisation.isInside(obj, x, y, self.windowProperties)):
                return obj

        return None

    def setForceFieldVisibility(self, vis: bool):
        if (vis):
            self.forceFieldVisualisation.show()
        else:
            self.forceFieldVisualisation.hide()

    def getForceFieldVisibility(self) -> bool:
        return self.forceFieldVisualisation.isVisible()

    def setMouseMode(self, mode):
        self.mouseMode = mode

    def animate(self):
        self.repaint()

    def paintEvent(self, e):
        qp = QtGui.QPainter()
        qp.begin(self)
        qp.setRenderHints(QPainter.Antialiasing, QPainter.SmoothPixmapTransform)
        self.drawWidget(qp)
        qp.end()
        # we have to fire a on pain event eacht time something changes!!!

    def calculateBackgroundForceField(self, listOfForceFields):
        tmpForce = 0.0
        distance = 100
        self.forceFieldArray = []
        row = []
        for x in range(int((distance / 2 - self.windowProperties.offset[0]) / self.windowProperties.scale[0]), int(
                        (self.windowProperties.width - self.windowProperties.offset[0]) / self.windowProperties.scale[
                    0]), int(distance / self.windowProperties.scale[0])):
            row = []
            for y in range(int((distance / 2 - self.windowProperties.offset[1]) / self.windowProperties.scale[1]), int(
                            (self.windowProperties.width - self.windowProperties.offset[1]) /
                            self.windowProperties.scale[1]), int(distance / self.windowProperties.scale[1])):
                tmpForce = 0.0
                for forcefield in listOfForceFields.values():
                    tmpForce += 0.5 * forcefield.getForce([x, y])
                row.append([[x, y], tmpForce])
            self.forceFieldArray.append(row)

    def drawArrow(self, qp, point, direction):
        penArrow = QtGui.QPen(QtGui.QColor(20, 20, 20), 1, QtCore.Qt.SolidLine)
        qp.setPen(penArrow)
        p = np.array(point)
        d = np.array(direction)
        vA = p - 0.5 * d
        vB = p + 0.5 * d
        qp.drawLines([QPointF(vA[0], vA[1]), QPointF(vB[0], vB[1])])
        v2 = -0.3 * d
        v2n = np.array([v2[1], -v2[0]])
        vC = p + 0.2 * d + v2n
        vB2 = p + 0.55 * d
        vD = p + 0.2 * d - v2n
        qp.drawLines([QPointF(vC[0], vC[1]), QPointF(vB2[0], vB2[1]), QPointF(vB2[0], vB2[1]), QPointF(vD[0], vD[1])])

    def drawBackgroundForceField(self, qp):
        for x in range(len(self.forceFieldArray)):
            for y in range(len(self.forceFieldArray[x])):
                # self.drawArrow(qp,  np.array(scaleProduct(self.forceFieldArray[x][y][0],self.scale))+np.array(self.offset), np.array(scaleProduct(self.forceFieldArray[x][y][1],self.scale)))
                self.drawArrow(qp, np.array(self.forceFieldArray[x][y][0]) * self.windowProperties.scale + np.array(
                    self.windowProperties.offset),
                               np.array(self.forceFieldArray[x][y][1]) * self.windowProperties.scale)

    def drawWidget(self, qp):
        newTime = time.clock()
        deltaT = newTime - self.oldTime

        # set font
        font = QtGui.QFont('Serif', 7, QtGui.QFont.Light)
        qp.setFont(font)

        # get actual size of the widget
        self.updateSize()
        self.drawBackground(qp)

        self.backgroundDecorator.setTime(deltaT)
        self.backgroundDecorator.draw(qp)

        self.forceFieldVisualisation.draw(qp)
        # self.drawBackgroundForceField(qp)

        self.drawObjects(qp)

        for obj in self.model.sandbox.getListOfGUIElements().values():
            obj.draw(qp)

        #draw the static user interace on top
        for obj in self.staticGUIElementList.values():
            obj.draw(qp)


        # time.sleep(0.0002)
        # self.update()

        self.oldTime = newTime

    def mousePressEvent(self, event):
        self.mouseInstance.mousePressEvent(event)
        self.storedMouseEventButtons = event.buttons();
        #cheeck the static user interace first
        for obj in self.staticGUIElementList.values():
            if (obj.mousePressEvent(event)):
                return
        #then the normal user interace
        for obj in self.model.sandbox.getListOfGUIElements().values():
            if (obj.mousePressEvent(event)):
                return


        if (event.buttons() == QtCore.Qt.RightButton):
            self.tempMousePosForDrag = [event.x(), event.y()]
        if (event.buttons() == QtCore.Qt.LeftButton):
            obj = self.getObjectAt(event.x(), event.y())
            self.dragObject = obj
            # store mouse position
            self.tempMousePosForDrag[0] = event.x()
            self.tempMousePosForDrag[1] = event.y()

    def mouseMoveEvent(self, event):
        self.mouseInstance.mouseMoveEvent(event)

        for obj in self.staticGUIElementList.values():
            if (obj.mouseMoveEvent(event)):
                return
        for obj in self.model.sandbox.getListOfGUIElements().values():
            if (obj.mouseMoveEvent(event)):
                return

        if (event.buttons() == QtCore.Qt.RightButton):
            actualPos = [event.x(), event.y()]
            delPos = [self.tempMousePosForDrag[0] - actualPos[0], self.tempMousePosForDrag[1] - actualPos[1]]
            self.tempMousePosForDrag = actualPos
            self.windowProperties.offset[0] -= delPos[0]
            self.windowProperties.offset[1] -= delPos[1]
        if (event.buttons() == QtCore.Qt.LeftButton):
            if (self.dragObject != None):
                self.mouseModeStored = self.mouseModeStored
                self.mouseMode = GuiConstants.MOUSE_ON_DRAG_MODE
                actualPos = [event.x(), event.y()]
                delPos = [self.tempMousePosForDrag[0] - actualPos[0], self.tempMousePosForDrag[1] - actualPos[1]]
                self.tempMousePosForDrag = actualPos
                # ToDo: only the controler is allowed to performe a move -> change to controller method!
                if (isinstance(self.dragObject, PlotableObject)):
                    self.dragObject.move(-delPos[0], -delPos[1])
                else:
                    try:
                        self.dragObject.move(-delPos[0] / self.windowProperties.scale[0],
                                             -delPos[1] / self.windowProperties.scale[1])
                    except:
                        print("[onDrag]: ", self.dragObject, " is not moveable")

    def mouseReleaseEvent(self, event):
        self.mouseInstance.mouseReleaseEvent(event)

        for obj in self.staticGUIElementList.values():
            obj.mouseReleaseEvent(event)
        for obj in self.model.sandbox.getListOfGUIElements().values():
            obj.mouseReleaseEvent(event)

        if (self.storedMouseEventButtons == QtCore.Qt.RightButton):
            pass
        if (self.storedMouseEventButtons == QtCore.Qt.LeftButton):
            print("left")
            if (self.mouseMode == GuiConstants.MOUSE_SELECTION_MODE):
                obj = self.getObjectAt(event.x(), event.y())
                if (not obj == None):
                    # check if selected
                    if (not obj.selected):
                        self.model.sandbox.deselectObject(self.selectedObject)
                        self.model.sandbox.selectObject(obj)
                        self.selectedObject = obj

            if (self.mouseMode == GuiConstants.MOUSE_DELETE_MODE):
                obj = self.getObjectAt(event.x(), event.y())
                self.controller.deleteObject(obj)
                #ToDo: let the EngineManager handle this!

            if (self.mouseMode == GuiConstants.MOUSE_ON_DRAG_MODE):
                self.mouseMode = self.mouseModeStored
                self.dragObject = None
        self.updateObjects()

    def wheelEvent(self, event):
        self.mouseInstance.wheelEvent(event)

        for obj in self.staticGUIElementList.values():
            obj.wheelEvent(event)
        for obj in self.model.sandbox.getListOfGUIElements().values():
            obj.wheelEvent(event)
        self.windowProperties.scale[0] *= 1 + event.delta() * self.mouseWheelScrollingFactor
        self.windowProperties.scale[1] *= 1 + event.delta() * self.mouseWheelScrollingFactor
        self.updateObjects()

    ##############################################################
    #################   DRAW ROUTINES  ###########################
    ##############################################################
    def drawBackground(self, qp: QtGui.QPainter):

        # draw background
        if (self.guiSettings.runMode == GlobalGuiSettings.RUN_MODE_DAY):
            qp.setBrush(QtGui.QColor(GlobalGuiSettings.BG_COLOR_DAY_3UB[0], GlobalGuiSettings.BG_COLOR_DAY_3UB[1],
                                     GlobalGuiSettings.BG_COLOR_DAY_3UB[2]))
        else:
            qp.setBrush(QtGui.QColor(GlobalGuiSettings.BG_COLOR_NIGHT_3UB[0], GlobalGuiSettings.BG_COLOR_NIGHT_3UB[1],
                                     GlobalGuiSettings.BG_COLOR_NIGHT_3UB[2]))
        qp.drawRect(0, 0, self.windowProperties.width, self.windowProperties.height)

    ##############################################################

    def drawObjects(self, qp: QtGui.QPainter):
        # ToDo: draw in opposite order (from back to front)!
        for obj in self.model.sandbox.getForceFieldList().values():
            self.objectVisualisation.drawObject(obj, qp, self.windowProperties)
        for obj in self.model.sandbox.getInteractionForceList():
            self.objectVisualisation.drawObject(obj, qp, self.windowProperties)
        for obj in self.model.sandbox.getObjectList().values():
            self.objectVisualisation.drawObject(obj, qp, self.windowProperties)

