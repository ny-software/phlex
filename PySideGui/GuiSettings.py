from GlobalConstants import *
from PySideGui.Visualisation.Texture import *
from Units.SISystem import *

class WindowProperties:
    def __init__(self):
        self.width=0
        self.height=0
        self.offset = [200, 600]
        self.scale = [0.6, -0.6]


class GlobalGuiSettings:
    RUN_MODE_NIGHT=0
    RUN_MODE_DAY=1

    RUN_MODE_EDITOR = 2
    RUN_MODE_VIEWER = 3

    #BG_COLOR_NIGHT_3UB=[106,81,163]
    BG_COLOR_NIGHT_3UB=[116,169,207]
    BG_COLOR_DAY_3UB=[255,237,160]

    BG_COLOR_MAIN_MENUE_3UB=[255,255,255]

    #BG_COLOR_NIGHT_3F=[106.0/255, 81.0/255, 163.0/255]
    BG_COLOR_NIGHT_3F=[84.0/255, 39.0/255, 143.0/255]
    BG_COLOR_DAY_3F=[255.0/255, 237.0/255, 160.0/255]

    TEXTURE_PATHS = {
                     GlobalConstants.TEXTURE_ID_SPUTNIK : "./images/misc/sputnik.png",

                     GlobalConstants.TEXTURE_ID_FOOTBALL : "./images/balls/football.png",
                     GlobalConstants.TEXTURE_ID_BASEBALL : "./images/balls/baseball.png",
                     GlobalConstants.TEXTURE_ID_BILLARDBALL : "./images/balls/billardball.png",
                     GlobalConstants.TEXTURE_ID_BOWLINGBALL : "./images/balls/bowlingball.png",
                     GlobalConstants.TEXTURE_ID_GOLFBALL : "./images/balls/golfball.png",
                     GlobalConstants.TEXTURE_ID_PLAINBALL : "./images/balls/plainball.png",
                     GlobalConstants.TEXTURE_ID_TENISBALL : "./images/balls/tennisball.png",
                     GlobalConstants.TEXTURE_ID_VOLLEYBALL : "./images/balls/volleyball.png",
                     GlobalConstants.TEXTURE_ID_BUBBLE_BALL : "./images/balls/bubble.png",

                     GlobalConstants.TEXTURE_ID_BILLARD_01_BALL: "./images/balls/Billard/billard01.png",
                     GlobalConstants.TEXTURE_ID_BILLARD_02_BALL: "./images/balls/Billard/billard02.png",
                     GlobalConstants.TEXTURE_ID_BILLARD_03_BALL: "./images/balls/Billard/billard03.png",
                     GlobalConstants.TEXTURE_ID_BILLARD_04_BALL: "./images/balls/Billard/billard04.png",
                     GlobalConstants.TEXTURE_ID_BILLARD_05_BALL: "./images/balls/Billard/billard05.png",
                     GlobalConstants.TEXTURE_ID_BILLARD_06_BALL: "./images/balls/Billard/billard06.png",
                     GlobalConstants.TEXTURE_ID_BILLARD_07_BALL: "./images/balls/Billard/billard07.png",
                     GlobalConstants.TEXTURE_ID_BILLARD_08_BALL: "./images/balls/Billard/billard08.png",
                     GlobalConstants.TEXTURE_ID_BILLARD_09_BALL: "./images/balls/Billard/billard09.png",
                     GlobalConstants.TEXTURE_ID_BILLARD_10_BALL: "./images/balls/Billard/billard10.png",
                     GlobalConstants.TEXTURE_ID_BILLARD_11_BALL: "./images/balls/Billard/billard11.png",
                     GlobalConstants.TEXTURE_ID_BILLARD_12_BALL: "./images/balls/Billard/billard12.png",
                     GlobalConstants.TEXTURE_ID_BILLARD_13_BALL: "./images/balls/Billard/billard13.png",
                     GlobalConstants.TEXTURE_ID_BILLARD_14_BALL: "./images/balls/Billard/billard14.png",
                     GlobalConstants.TEXTURE_ID_BILLARD_15_BALL: "./images/balls/Billard/billard15.png",

                     GlobalConstants.TEXTURE_ID_MERCURY : "./images/solarsystem/mercury.png",
                     GlobalConstants.TEXTURE_ID_VENUS : "./images/solarsystem/venus.png",
                     GlobalConstants.TEXTURE_ID_EARTH : "./images/solarsystem/earth.png",
                     GlobalConstants.TEXTURE_ID_MARS : "./images/solarsystem/mars.png",
                     GlobalConstants.TEXTURE_ID_JUPITER : "./images/solarsystem/jupiter.png",
                     GlobalConstants.TEXTURE_ID_SATURN : "./images/solarsystem/saturn.png",
                     GlobalConstants.TEXTURE_ID_URANUS : "./images/solarsystem/uranus.png",
                     GlobalConstants.TEXTURE_ID_NEPTUNE : "./images/solarsystem/neptune.png",
                     GlobalConstants.TEXTURE_ID_PLUTO : "./images/solarsystem/pluto.png",

                     GlobalConstants.TEXTURE_ID_MAT_ROCK : "./images/materials/rock_ground.png",
                     GlobalConstants.TEXTURE_ID_MAT_BROWN_STONE : "./images/materials/stone_ground.png",
                     GlobalConstants.TEXTURE_ID_MAT_BRICK_WALL : "./images/materials/brick_wall.png",



                     GlobalConstants.TEXTURE_ID_TEX_LEASH : "./images/textures/leash.png",
                     GlobalConstants.TEXTURE_ID_TEX_OLDPAPER : "./images/textures/oldpaper.png",

                     GlobalConstants.TEXTURE_ID_MACKE_MR_E_GREEN : "./images/macke/MrE_G.png",
                     GlobalConstants.TEXTURE_ID_MACKE_MR_E_ORANGE: "./images/macke/MrE_O.png",


                     GlobalConstants.TEXTURE_ID_MACKE_SPRING_ORANGE: "./images/macke/SpringO.png",
                     GlobalConstants.TEXTURE_ID_MACKE_SPRING_GREEN : "./images/macke/SpringG.png",
                     GlobalConstants.TEXTURE_ID_MACKE_SPRING_ORANGE_GREEN: "./images/macke/SpringOG.png",
                     GlobalConstants.TEXTURE_ID_MACKE_SPRING_GREEN_ORANGE: "./images/macke/SpringGO.png"
                     }

    IMAGE_PATHS = {  GlobalConstants.IMAGE_ID_ITA_PHLEX_LOGO : "./images/ita_phlex_halfsymb_noncenter_whiteBG.png",
                     GlobalConstants.IMAGE_ID_ITA_PHLEX_DEMO_LOGO : "./images/ita_phlex_halfsymb_noncenter_whiteBG_DEMO.png",
                     GlobalConstants.IMAGE_ID_GUI_ARROW : "./images/misc/arrow.png",
                     GlobalConstants.IMAGE_ID_BACKGROUND_01 : "./images/backgrounds/Background01.png",
                     GlobalConstants.IMAGE_ID_BACKGROUND_02 : "./images/backgrounds/Background02.png",
                     GlobalConstants.IMAGE_ID_BACKGROUND_SPACE_01 : "./images/backgrounds/Background_Space.png"
                     }

    def __init__(self, runMode):
        self.runMode = runMode

        self.windowName="ITA PhleX"

        self.mouseWheelScrollingFactor = 0.001

        self.initAnimationWindowHeight = int(512*1.5)
        self.initAnimationWindowWidth = int(1024*1)

        self.TEXTURES = {}
        #self.loadTextures()
        self.loadImages()

        self.windowProperties = WindowProperties()

        self.unitSystem = SISystem()

        self.backgroundDecorator = None

    def getUnitSystem(self):
        return self.unitSystem

    def getWindowProperties(self) -> WindowProperties:
        return self.windowProperties

    def setWindowProperties(self, prop:WindowProperties):
        self.windowProperties = prop

    def getBackgroundDecorator(self):
        return self.backgroundDecorator

    def setBackgroundDecorator(self, bgDec):
        self.backgroundDecorator = bgDec

    def getTexture(self, id):
        if(self.TEXTURES.get(id) is None):
            self.loadSingleTexture(id)
        return self.TEXTURES.get(id)

    def loadSingleTexture(self, id):
        self.TEXTURES.update({id : Texture(GlobalGuiSettings.TEXTURE_PATHS.get(id))})

    def loadTextures(self):
        self.TEXTURES = {k: Texture(v) for k, v in GlobalGuiSettings.TEXTURE_PATHS.items()}

    def getImage(self, id):
        return self.IMAGES.get(id)

    def loadImages(self):
        self.IMAGES = {k: Texture(v) for k, v in GlobalGuiSettings.IMAGE_PATHS.items()}




#globalGuiSettingsInstance = GlobalGuiSettings(GlobalGuiSettings.RUN_MODE_VIEWER)