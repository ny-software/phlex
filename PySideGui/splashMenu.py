import time
from Controller.CommandLineController import *
from PySideGui.GuiTools.MouseInstance import *
from PySideGui.GuiTools.simplePushButton import *
from Model.PhlexApplicationModel import *
from Sandbox.SimpleMechanicSandbox import *
from Engines.SimpleMechanicsEngine import *


class SplashMenuePushButton(SimplePushButton):
    def __init__(self, text, parent):
        super(SplashMenuePushButton, self).__init__(text, parent)

        self.setStyleSheet("background-color: #122229; color: white")



class splashMenue(QtGui.QWidget):
    onDemoLoaded = Signal()
    onExit = Signal()

    def __init__(self, guiSettings: GlobalGuiSettings):
        super(splashMenue, self).__init__()

        self.oldTime = time.clock()

        self.sandbox = None
        self.engineController = None
        self.guiSettings = guiSettings

        self.initUI()

    def getButtonWidth(self):
        return self.buttonWidth

    def getSandbox(self):
        return self.sandbox

    def getEngineController(self):
        return self.engineController

    def initUI(self):
        self.buttonWidth = 400
        self.setFont(QtGui.QFont("Cabin", 18, QtGui.QFont.Bold))

        self.logo = QtGui.QLabel(self)
        self.logoPixmap = QtGui.QPixmap(self.guiSettings.getImage(GlobalConstants.IMAGE_ID_ITA_PHLEX_DEMO_LOGO).getImage()).scaledToWidth(self.buttonWidth )
        self.logo.setPixmap(self.logoPixmap)

        #self.emptySandboxButton = SimplePushButton('Leeres Arbeitsblatt', self)
        #self.emptySandboxButton.clicked[bool].connect(self.selectEmpltySandboxButtonSlot)

        self.linearCollisionButton = SplashMenuePushButton('Linearer Stoß Demo', self)
        self.linearCollisionButton.clicked[bool].connect(self.selectLinearCollisionButtonSlot)

        self.airDraggButton = SplashMenuePushButton('Fall mit Reibung Demo', self)
        self.airDraggButton.clicked[bool].connect(self.selectAirDraggDemoButtonSlot)

        self.mackeAwardButton = SplashMenuePushButton('Macke Award Demo', self)
        self.mackeAwardButton.clicked[bool].connect(self.selectMackeAwardButtonSlot)

        self.manyParticleDemoButton = SplashMenuePushButton('Vielteilchen Demo', self)
        self.manyParticleDemoButton.clicked[bool].connect(self.selectManyParticleDemoButtonSlot)

        self.spaceDemoButton = SplashMenuePushButton('Weltall Demo', self)
        self.spaceDemoButton.clicked[bool].connect(self.selectSpaceDemoButtonSlot)

        self.xmasDemoButton = SplashMenuePushButton('X-Mas', self)
        self.xmasDemoButton.clicked[bool].connect(self.selectXmasButtonSlot)

        #self.SandboxDemoButton = SimplePushButton('Sandbox Demo', self)
        #self.SandboxDemoButton.clicked[bool].connect(self.selectSandboxDemoButtonSlot)

        self.exitButton = SplashMenuePushButton('Exit', self)
        self.exitButton.setIcon(QtGui.QPixmap('images/GUI/Exit.png'))
        self.exitButton.clicked[bool].connect(self.onExitSlot)

        self.vbox = QtGui.QVBoxLayout()
        self.vbox.addStretch(1)
        self.vbox.addWidget(self.logo)
        #self.vbox.addWidget(self.emptySandboxButton)
        self.vbox.addWidget(self.linearCollisionButton)
        self.vbox.addWidget(self.airDraggButton)
        self.vbox.addWidget(self.mackeAwardButton)
        self.vbox.addWidget(self.manyParticleDemoButton)
        self.vbox.addWidget(self.spaceDemoButton)
        self.vbox.addWidget(self.xmasDemoButton)
        #self.vbox.addWidget(self.SandboxDemoButton)
        self.vbox.addWidget(self.exitButton)

        self.setLayout(self.vbox)



    def onExitSlot(self, state:bool):
        self.onExit.emit()

    def paintEvent(self, e):
        qp = QtGui.QPainter()
        qp.begin(self)
        self.drawWidget(qp)
        qp.end()

    def drawWidget(self, qp):
        newTime = time.clock()
        deltaT = newTime - self.oldTime

        # set font
        font = QtGui.QFont('Serif', 7, QtGui.QFont.Light)
        qp.setFont(font)

        # get actual size of the widget
        #self.updateSize()
        self.drawBackground(qp)

        # time.sleep(0.0002)
        # self.update()

        self.oldTime = newTime

    def selectEmpltySandboxButtonSlot(self, state:bool):
        print("Empty Sandbox Selected")
        self.sandbox = SimpleMechanicSandbox()
        self.engine = SimpleMechanicsEngine()
        self.engineController = CommandLineController(self.engine)
        createTestSetupEmptyBox(self.sandbox)
        self.sandbox.setListOfGUIElements(listToDict(createTestSetupEmptyBox_GUI(self.engineController, self.guiSettings, self.sandbox)))
        self.engineController.initializeEngine(self.sandbox)
        self.onDemoLoaded.emit()


    def selectLinearCollisionButtonSlot(self, state:bool):
        print("Empty Sandbox Selected")
        self.sandbox = SimpleMechanicSandbox()
        self.engine = SimpleMechanicsEngine()
        self.applicationModel = PhlexApplicationModel(self.sandbox, self.engine)
        self.engineController = CommandLineController(self.applicationModel)
        createTestSetupLinearCollision(self.sandbox)
        self.sandbox.setListOfGUIElements(listToDict(createTestSetupLinearCollision_GUI(self.engineController, self.guiSettings, self.sandbox)))
        self.engineController.initializeEngine(self.sandbox)
        self.onDemoLoaded.emit()

    def selectAirDraggDemoButtonSlot(self, state:bool):
        print("Empty Sandbox Selected")
        self.sandbox = SimpleMechanicSandbox()
        self.engine = SimpleMechanicsEngine()
        self.applicationModel = PhlexApplicationModel(self.sandbox, self.engine)
        self.engineController = CommandLineController(self.applicationModel)
        createTestSetupMechanicsAirDragg(self.sandbox)
        self.sandbox.setListOfGUIElements(listToDict(createTestSetupMechanicsAirDragg_GUI(self.engineController, self.guiSettings, self.sandbox)))
        self.engineController.initializeEngine(self.sandbox)
        self.onDemoLoaded.emit()

    def selectMackeAwardButtonSlot(self, state:bool):
        print("Macke Award Demo Selected")
        self.sandbox = SimpleMechanicSandbox()
        self.engine = SimpleMechanicsEngine()
        self.applicationModel = PhlexApplicationModel(self.sandbox, self.engine)
        self.engineController = CommandLineController(self.applicationModel)
        createTestSetupMechanicsMacke(self.sandbox)
        self.sandbox.setListOfGUIElements(listToDict(createTestSetupMechanicsMacke_GUI(self.engineController, self.guiSettings, self.sandbox)))
        self.engineController.initializeEngine(self.sandbox)
        self.onDemoLoaded.emit()

    def selectSpaceDemoButtonSlot(self, state:bool):
        print("Macke Award Demo Selected")
        self.sandbox = SimpleMechanicSandbox()
        self.engine = SimpleMechanicsEngine()
        self.applicationModel = PhlexApplicationModel(self.sandbox, self.engine)
        self.engineController = CommandLineController(self.applicationModel)
        createTestSetupMechanicsSpace01(self.sandbox)
        self.sandbox.setListOfGUIElements(listToDict(createTestSetupMechanicsSpace01_GUI(self.engineController, self.guiSettings, self.sandbox)))
        self.engineController.initializeEngine(self.sandbox)
        self.onDemoLoaded.emit()

    def selectManyParticleDemoButtonSlot(self, state:bool):
        print("Many Particle Demo Selected")
        self.sandbox = SimpleMechanicSandbox()
        self.engine = SimpleMechanicsEngine()
        self.applicationModel = PhlexApplicationModel(self.sandbox, self.engine)
        self.engineController = CommandLineController(self.applicationModel)
        createTestSetupMechanicsManyParticle(self.sandbox)
        self.sandbox.setListOfGUIElements(listToDict(createTestSetupMechanicsManyParticle_GUI(self.engineController, self.guiSettings, self.sandbox)))
        self.engineController.initializeEngine(self.sandbox)
        self.onDemoLoaded.emit()

    def selectSandboxDemoButtonSlot(self, state:bool):
        print("Many Particle Demo Selected")
        self.sandbox = SimpleMechanicSandbox()
        self.engine = SimpleMechanicsEngine()
        self.applicationModel = PhlexApplicationModel(self.sandbox, self.engine)
        self.engineController = CommandLineController(self.applicationModel)
        createTestSetupMechanicsSanbox(self.sandbox)
        self.sandbox.setListOfGUIElements(listToDict(createTestSetupMechanicsSandbox_GUI(self.engineController, self.guiSettings, self.sandbox)))
        self.engineController.initializeEngine(self.sandbox)
        self.onDemoLoaded.emit()

    def selectXmasButtonSlot(self, state:bool):
        print("Empty Sandbox Selected")
        self.sandbox = SimpleMechanicSandbox()
        self.engine = SimpleMechanicsEngine()
        self.applicationModel = PhlexApplicationModel(self.sandbox, self.engine)
        self.engineController = CommandLineController(self.applicationModel)
        createTestSetupXmas(self.sandbox)
        self.sandbox.setListOfGUIElements(listToDict(createTestSetupXmas_GUI(self.engineController, self.guiSettings, self.sandbox)))
        self.engineController.initializeEngine(self.sandbox)
        self.onDemoLoaded.emit()

    ##############################################################
    #################   DRAW ROUTINES  ###########################
    ##############################################################
    def drawBackground(self, qp: QtGui.QPainter):
        size = self.size()
        # draw background
        qp.setBrush(QtGui.QColor(GlobalGuiSettings.BG_COLOR_MAIN_MENUE_3UB[0], GlobalGuiSettings.BG_COLOR_MAIN_MENUE_3UB[1],
                                     GlobalGuiSettings.BG_COLOR_MAIN_MENUE_3UB[2]))
        qp.drawRect(0, 0, size.width(), size.height())

    ##############################################################
