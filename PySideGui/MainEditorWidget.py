from PySideGui.ObjectListViewer import *
from PySideGui.PySideSimulationWidget import *
from PySideGui.GuiConstants import *
from Model.PhlexApplicationModel import PhlexApplicationModel

class MainEditorWidget(QtGui.QWidget):
    def __init__(self, model:PhlexApplicationModel, controller:BasicController):
        super(MainEditorWidget, self).__init__()

        self.model = model
        self.controller = controller

        self.guiSettings = GlobalGuiSettings(GlobalGuiSettings.RUN_MODE_EDITOR)
        createTestSetupMechanicsMacke(self.model.sandbox)
        self.model.sandbox.setListOfGUIElements(listToDict(createTestSetupMechanicsMacke_GUI(self.controller, self.guiSettings, self.model.sandbox)))
        self.simulationWidget = PySideSimulationWidget(self.model, self.controller, self.guiSettings)
        self.controller.initializeEngine(self.model.sandbox)

        self.inputField = QLineEdit()
        self.inputField.editingFinished.connect(self.inputField_edit_text_changed)

        self.objectListField = ObjectListViewer(self.model, self.controller)
        self.objectListField.setFixedWidth(300)

#        sld.valueChanged[int].connect(self.changeValue)
        self.hbox = QtGui.QHBoxLayout()
        self.hbox.addWidget(self.simulationWidget)

        self.vbox = QtGui.QVBoxLayout()
        self.vbox.addStretch(1)
        self.vbox.addLayout(self.hbox)
        self.vbox.addWidget(self.inputField)

        self.verticalListBox = QtGui.QVBoxLayout()
        self.verticalListBox.addWidget(self.objectListField)

        self.mainLayout = QtGui.QGridLayout()

        self.mainLayout.setSpacing(10)

        self.mainLayout.addLayout(self.vbox, 1, 0)
        self.mainLayout.addLayout(self.verticalListBox, 1, 1)

        self.setLayout(self.mainLayout)

        self.setGeometry(10, 10, 390, 210)
        self.setWindowTitle(self.guiSettings.windowName)
        self.show()

        timer = QTimer(self)
        self.connect(timer, SIGNAL("timeout()"), self.simulationWidget.animate)
        timer.start(1)  #timeout in ms

    def changeValue(self, value):
        pass

    def inputField_edit_text_changed(self):
        self.controller.processCommand(self.inputField.text())

    def mousePressEvent(self, event):
        print ("QGraphicsView mousePress")

    def mouseMoveEvent(self, event):
        print("QMainWindow mouseMove")

    def mouseReleaseEvent(self, event):
        print("QMainWindow mouseRelease")

    def startStopSimulation(self):
        if self.controller.isRunning():
            self.controller.stopSimulation()
            self.startStopButton.setText("Start")
        else:
            self.controller.startSimulation()
            self.startStopButton.setText("Pause")

    def resetSimulation(self):
        self.controller.resetSimulation()
        self.resetButton.setChecked(False)
        self.startStopButton.setText("Start")


    def selectButtonSlot(self):
        print("Mousemode is now SELECTION")
        self.simulationWidget.setMouseMode(GuiConstants.MOUSE_SELECTION_MODE)


    def deleteButtonSlot(self):
        print("Mousemode is now DELETION")
        self.simulationWidget.setMouseMode(GuiConstants.MOUSE_DELETE_MODE)

    def ToggleForceFieldVisibility(self):
        state = self.simulationWidget.getForceFieldVisibility()
        state = not state
        self.simulationWidget.setForceFieldVisibility(state)