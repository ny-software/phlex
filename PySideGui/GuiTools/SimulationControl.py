from PySide import QtGui, QtCore
from PySideGui.GuiSettings import *
from PySideGui.GuiTools.PlotableObject import *
from Controller.BasicController import *
from PySideGui.Visualisation.Texture import *

class SimulationControl(PlotableObject):

    def __init__(self, controller:BasicController, guiSettings:GlobalGuiSettings, windowPos:list=[400, 400]):
        PlotableObject.__init__(self, guiSettings, windowPos)

        self.controller = controller


        self.height = 50
        self.buttonWidth = 50
        self.timeBarWidth = 80
        self.width = 2*self.buttonWidth + self.timeBarWidth
        self.sep = 2

        self.resetButtonRect = QtCore.QRectF( self.getCoordinates().x(), self.getCoordinates().y(), self.buttonWidth, self.height)
        self.timeBarRect = QtCore.QRectF( self.getCoordinates().x()+self.buttonWidth+self.sep, self.getCoordinates().y(), self.timeBarWidth-2*self.sep, self.height)
        self.playButtonRect = QtCore.QRectF( self.getCoordinates().x()+self.timeBarWidth+self.buttonWidth, self.getCoordinates().y(), self.buttonWidth, self.height)

        self.playTexture = Texture("images/play.png")
        self.pauseTexture = Texture("images/pause.png")
        self.resetTexture = Texture("images/reset.png")
        self.timebarTexture = Texture("images/timebar.png")


    def isInside(self, xin, yin):
        return False

    def draw(self, qp:QtGui.QPainter):
        #qp.drawRect(self.resetButtonRect)
        #qp.drawRect(self.playButtonRect)
        if(self.controller.isRunning()):
            self.pauseTexture.draw(qp, self.playButtonRect)
        else:
            self.playTexture.draw(qp, self.playButtonRect)
        self.timebarTexture.draw(qp, self.timeBarRect)
        self.resetTexture.draw(qp, self.resetButtonRect)

        font = qp.font()
        font.setPixelSize(18)
        qp.setFont(font)
        qp.setPen(QtGui.QColor(255, 255, 255, 255))
        qp.drawText(self.timeBarRect, QtCore.Qt.AlignCenter | QtCore.Qt.AlignHCenter, "{:4.2f}".format(self.controller.getEngine().getSimulationTime()))

        self.drawChilds(qp)


    #plotable objects are not selectable by definition
    def isSelected(self):
        return False

    def mousePressEvent(self, event):
        pass
    def mouseMoveEvent(self, event):
        pass
    def mouseReleaseEvent(self, event):
        mouse = QtCore.QPointF(event.x(), event.y())
        if(self.resetButtonRect.contains(mouse)):
            self.controller.resetSimulation()
            return
        if(self.playButtonRect.contains(mouse)):
            if(self.controller.isRunning()):
                self.controller.stopSimulation()
            else:
                self.controller.startSimulation()


    def wheelEvent(self,event):
        pass
