from PySideGui.GuiTools.PlotableObject import *
from Objects.Conectors.BasicWatcher import *

class BasicIndicator(PlotableObject):


    def __init__(self, watcher:BasicWatcher, guiSettings:GlobalGuiSettings, windowPos:list=[400, 400]):
        PlotableObject.__init__(self, guiSettings, windowPos)

        self.watcher = watcher
        self.observableKey = None

    def getObservableKey(self):
        return self.obserbableKey

    def setObservableKey(self, key:str):
        self.obserbableKey = key
