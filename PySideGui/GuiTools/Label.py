from PySideGui.GuiTools.PlotableObject import *



class Label(PlotableObject):

    def __init__(self, text:str, guiSettings:GlobalGuiSettings, windowPos:list=[0, 0], fontcolor:QtGui.QColor=QtGui.QColor(255, 255, 255, 255),  parent=None):
        PlotableObject.__init__(self,guiSettings, windowPos, parent)


        self.text = text
        self.fontColor = fontcolor

    def reset(self):
        PlotableObject.reset(self)

    def setFontColor(self, color:QtGui.QColor):
        self.fontColor = color

    def isInside(self, xin, yin):
        return False
#        return self.printRect.contains(xin, yin)

    def draw(self, qp:QtGui.QPainter):
        font = qp.font()
        font.setPixelSize(18)
        font.setBold(True)
        qp.setFont(font)
        qp.setPen(self.fontColor)
        qp.drawText(self.getCoordinates(), self.text)

        self.drawChilds(qp)
