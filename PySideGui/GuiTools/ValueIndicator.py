from PySideGui.GuiTools.BasicIndicator import *



class ValueIndicator(BasicIndicator):

    def __init__(self, watcher:BasicWatcher, guiSettings:GlobalGuiSettings, windowPos:list=[400, 400]):
        BasicIndicator.__init__(self,watcher, guiSettings, windowPos)


        self.height = 50
        self.width = 100
        self.size = QSizeF(self.width, self.height)
        self.texture = Texture("images/timebar.png")

        self.reset()
        #unit which is used for representation
        self.unit = None

    def reset(self):
        BasicIndicator.reset(self)
        self.size = QSizeF(self.width, self.height)
        self.textureRect = QtCore.QRectF(self.getCoordinates(), self.size)
        self.texture = Texture("images/timebar.png")

    def setUnit(self, u):
        self.unit = u

    def setHeight(self, h):
        if(h>=0):
            self.height = h
            self.reset()
    def setWidth(self, w):
        if(w>=0):
            self.width = w
            self.reset()


    def isInside(self, xin, yin):
        return self.textureRect.contains(QtCore.QPointF(xin, yin))


    def draw(self, qp:QtGui.QPainter):
        self.texture.draw(qp, self.textureRect)

        font = qp.font()
        font.setPixelSize(18)
        font.setBold(True)
        qp.setFont(font)
        qp.setPen(QtGui.QColor(255, 255, 255, 255))
        if self.watcher is not None:
            if(self.unit is None):
                unit=self.guiSettings.getUnitSystem().unit(self.watcher.getObservableType(self.obserbableKey))
            else:
                unit = self.unit
            factor=unit.getFactorToSI()
            value = factor*self.watcher.getLastObservalbel(self.obserbableKey)
            text = "{:4.2f}".format(value)+" "+unit.toString()
            qp.drawText(self.textureRect, QtCore.Qt.AlignCenter | QtCore.Qt.AlignHCenter, text)
            #qp.drawText(self.textureRect, QtCore.Qt.AlignCenter | QtCore.Qt.AlignHCenter, "{:4.2f}".format(self.watcher.getLastObservalbel(self.obserbableKey)))

        self.drawChilds(qp)


