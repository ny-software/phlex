from PySide.QtGui import *
from PySide.QtCore import *
from PySideGui.GuiSettings import *
import numpy as np

class PlotableObject():

    plotableObjectCounter=0

    def __init__(self, guiSettings:GlobalGuiSettings, windowPos:list=[1000, 100], parent=None, id=None):
        PlotableObject.plotableObjectCounter+=1
        if(id==None):
            id="GUI_OBJ_"+str(PlotableObject.plotableObjectCounter)
        self.objectId = id

        self.relativePosition = QPointF(windowPos[0], windowPos[1])

        self.guiSettings = guiSettings


        self.childList = []
        self.parent = parent

    def getObjectID(self) -> str:
        return self.objectId

    def isInside(self, xin, yin):
        return False

    def getCoordinates(self):
        try:
            return self.parent.getCoordinates()+self.getRelativeCoordinates()
        except:
            return self.getRelativeCoordinates()

    def getRelativeCoordinates(self):
        return self.relativePosition

    def addChild(self, child):
        self.childList.append(child)
        child.setParent(self)

    def setParent(self, parent):
        self.parent = parent

    def draw(self, qp:QtGui.QPainter):
        pass

    def drawChilds(self, qp:QtGui.QPainter):
        for obj in self.childList:
            obj.draw(qp)

    def move(self, dx, dy):
        if(self.guiSettings.runMode==GlobalGuiSettings.RUN_MODE_EDITOR):
            self.relativePosition += QPointF(dx,dy)
            self.reset()

    def reset(self):
        for obj in self.childList:
            obj.reset()

    #plotable objects are not selectable by definition
    def isSelected(self):
        return False

    def mousePressEvent(self, event):
        pass
    def mouseMoveEvent(self, event):
        pass
    def mouseReleaseEvent(self, event):
        pass
    def wheelEvent(self,event):
        pass

    def updateWatcher(self, listOfWatchers):
        pass