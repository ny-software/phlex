from Objects.BasicObject import *
from Objects.Conectors.BasicAnchor import *
from PySideGui.GuiSettings import  *

class MouseInstance(BasicObject):
    """
    instance of the mouse, here all mouse actions will take place. Further, the mouse is an basic object which holds an anchor
    on which other objects can be attached
    """
    def __init__(self,  guiSettings: GlobalGuiSettings):
        super(MouseInstance, self).__init__("MouseInstance")

        self.guiSettings = guiSettings
        self.windowProp = guiSettings.getWindowProperties()
        #the anchor on which other object can be attached
        self.mouseAnchor = BasicAnchor(self,"MouseInstanceAnchor")
        self.addAnchor(self.mouseAnchor)

    def getMouseAnchor(self) -> BasicAnchor:
        return self.anchors.get("MouseInstanceAnchor")

    def attachObjectToMouse(self, obj:BasicObject) -> None:
        """
        attach all anchors of the Basic object to the mouse anchor
        :param obj: object which will be attached
        """
        for anchor in obj.getAnchors():
            self.getMouseAnchor().attachAnchor(anchor)

    def mousePressEvent(self, event):
        #dettach all anchors
        for anchor in  self.mouseAnchor.getAttachedAnchors() :
            anchor.dettachAnchor(self.mouseAnchor)

    def mouseMoveEvent(self, event):
        self.coordinates = np.array([event.x(), event.y()])
        for anchor in self.getMouseAnchor().getAttachedAnchors() :
            anchor.setOwnerCoordinates(np.array([(event.x()-self.windowProp.offset[0])/self.windowProp.scale[0], (event.y()-self.windowProp.offset[1])/self.windowProp.scale[1]]))
        pass

    def mouseReleaseEvent(self, event):
        pass

    def wheelEvent(self, event):
        pass