from PySide import QtGui, QtCore

from PySideGui.GuiTools.PlotableObject import *
from Objects.Conectors.BasicWatcher import *


class SimpleTracer(PlotableObject):
    def __init__(self, watcher,  guiSettings:GlobalGuiSettings, windowPos:list=[1000, 100]):
        PlotableObject.__init__(self, guiSettings, windowPos)
        self.watcher = None

        self.windowProperties = self.guiSettings.getWindowProperties()
        self.xList = []
        self.yList = []

        self.qPointList = []
        self.reset()
        self.setWatcher(watcher)

    #workaournd due to pyside - deepcopy problem
    def initPens(self):
        self.pen01 = QtGui.QPen(QtGui.QColor(166,97,26),4, QtCore.Qt.SolidLine)

    def update(self):
        pass

    def reset(self):
        PlotableObject.reset(self)
        self.initPens()
        self.qPointList = []
        self.update()



    def getSize(self):
        return None


    def setWatcher(self, watcher:BasicWatcher, color:QtGui.QColor=QtGui.QColor(166,97,26)):
        #for watcher in self.watcherList:
        if watcher is not None:
            self.watcher = watcher
            self.penColor = color
            self.xList =  watcher.getObservedList("COM_XCOORD")
            self.yList =  watcher.getObservedList("COM_YCOORD")


    def getWatcher(self):
        return self.watcher

    def updateWatcher(self, listOfWatchers):
        for watcher in listOfWatchers.values():
            if(self.watcher.getObjectID() == watcher.getObjectID()):
                self.setWatcher(watcher)

    def draw(self, qp:QtGui.QPainter):
        qp.setPen(self.pen01)
        self.reset()
        #if(thisEndPoint+2 < self.lastEndPoint or 0 > self.lastEndPoint ):
        #    self.reset()
        #    self.draw(qp)
        #    return
        for i in range(len(self.qPointList)+1, len(self.xList), 1):
            self.qPointList.append(QtCore.QPointF(self.xList[i-1]*self.windowProperties.scale[0]+self.windowProperties.offset[0],self.yList[i-1]*self.windowProperties.scale[1]+self.windowProperties.offset[1]))
            self.qPointList.append(QtCore.QPointF(self.xList[i]*self.windowProperties.scale[0]+self.windowProperties.offset[0],self.yList[i]*self.windowProperties.scale[1]+self.windowProperties.offset[1]))
        qp.drawLines(self.qPointList)

        self.drawChilds(qp)



    def onDragRelease(self):
        self.reset()

    def isInside(self, xin, yin):
        return False

    def mousePressEvent(self, event):
        pass
    def mouseMoveEvent(self, event):
        pass
    def mouseReleaseEvent(self, event):
        pass
    def wheelEvent(self,event):
        pass
