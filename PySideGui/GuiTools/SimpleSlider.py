from PySide import QtGui, QtCore
from PySideGui.GuiTools.PlotableObject import *
from PySideGui.GuiConstants import *
from PySideGui.GuiTools.Label import *

class SimpleSlider(PlotableObject):

    DIRECTION_VERTICAL = 0
    DIRECTION_HORIZONTAL = 0

    def __init__(self, manipulator, guiSettings:GlobalGuiSettings, windowPos:list=[400, 400]):
        PlotableObject.__init__(self, guiSettings, windowPos)

        self.length = 300
        self.bonusLength=20
        self.direction = SimpleSlider.DIRECTION_HORIZONTAL

        self.manipulator = manipulator

        self.unit = None

        self.sliderPosPC = 0.50  # in percent!

        self.sliderDim = [20, 10]
        self.barDiameter = 10

        #minimal and maximal value of the slider which is possible
        self.minVal = 0
        self.maxVal = 100
        self.step = 10

        #should the value be shown
        self.showValue = True

        self.storedMouseEventButtons=None
        #self.mouseMode = None

        self.updateSilderPosition()

        #pens and brushes
        self.initPens()
        self.sliderColor = QtGui.QColor(189,215,231)
        self.carrageColor = QtGui.QColor(49,130,189)
        #self.sliderColor = QtGui.QColor(239,101,72)
        #self.carrageColor = QtGui.QColor(215,48,31)

        #self.fontColor = QtGui.QColor(215,48,31)
        self.fontColor = self.sliderColor

    def initPens(self):
        self.pen = QtGui.QPen(QtGui.QColor(0, 0, 0), 1, QtCore.Qt.SolidLine)

    def reset(self):
        PlotableObject.reset(self)
        self.initPens()

    def setUnit(self, u):
        self.unit = u

    def updateSilderPosition(self):
        '''
        sets the slider position to the value corresponding to the acutal value of the manipulator
        :return: None
        '''
        if self.manipulator is not None:
            self.sliderPosPC = (self.manipulator.getValueFromObject()-self.minVal)/(self.maxVal-self.minVal)

    def setSliderPositionRel(self, pc):
        """
        sets the possition of the slider carrage to the relative position pc
        :param pc: position to set the carrage in %
        """
        pc = pc - ((pc*(self.maxVal-self.minVal))%self.step)/(self.maxVal-self.minVal)

        if(pc>1.0):
            self.sliderPosPC=1.0
        elif(pc<0.0):
            self.sliderPosPC=0.0
        else:
            self.sliderPosPC = pc
        self.changeManipulator()

    def setBounds(self, min, max, step=None):
        self.minVal = min
        self.maxVal = max
        if(step is not None):
            self.step = step
        else:
            self.step = (max-min)/10
        self.updateSilderPosition()

    def calcSliderPositionRel(self, val):
        if(self.direction == SimpleSlider.DIRECTION_HORIZONTAL):
            return(val-self.getCoordinates().x()+self.sliderDim[1]/2)/self.length

    def changeManipulator(self):
        self.manipulator.setValue(self.minVal + (self.maxVal-self.minVal)*self.sliderPosPC)

    def isInside(self, xin, yin):
        if(self.direction == SimpleSlider.DIRECTION_HORIZONTAL):
            if(0 < xin-self.getCoordinates().x() < self.length):
                if( -self.barDiameter/2 < yin - self.getCoordinates().y() < self.barDiameter/2 ):
                    return True
        return False

    def isInsideSlider(self, xm, ym):
        if(self.direction == SimpleSlider.DIRECTION_HORIZONTAL):
            if(-self.sliderDim[0]/2 < xm-(self.getCoordinates().x()+self.sliderPosPC*self.length) < self.sliderDim[0]/2):
                if( -self.sliderDim[0]/2  < ym - self.getCoordinates().y() <  self.sliderDim[0]/2):
                    return True
        return False
        

    def draw(self, qp:QtGui.QPainter):
        if(self.direction == SimpleSlider.DIRECTION_HORIZONTAL):
            qp.setPen(self.pen)
            qp.setBrush(self.sliderColor)
            qp.drawRoundedRect(self.getCoordinates().x()-self.bonusLength/2-self.sliderDim[1]/2, int(self.getCoordinates().y()-self.barDiameter/2), self.length+self.bonusLength, self.barDiameter, int(self.barDiameter/2) , int(self.barDiameter/2))
            qp.setBrush(self.carrageColor)
            qp.drawRoundedRect(int(self.getCoordinates().x()+self.sliderPosPC*self.length-self.sliderDim[1]), int(self.getCoordinates().y()-self.sliderDim[0]/2), self.sliderDim[1], self.sliderDim[0], int(self.sliderDim[1]/2), int(self.sliderDim[1]/2))

            if(self.showValue==True):
                font = qp.font()
                font.setPixelSize(18)
                font.setBold(True)
                qp.setFont(font)
                qp.setPen(self.fontColor)
                if(self.unit is None):
                    unit=self.guiSettings.getUnitSystem().unit(self.manipulator.getObservableType())
                else:
                    unit = self.unit
                factor=unit.getFactorToSI()
                value = factor*self.manipulator.getValueFromObject()
                text = str('%.2f' % value)+" "+unit.toString()
                qp.drawText(QPointF(self.getCoordinates().x()+self.sliderPosPC*self.length-self.sliderDim[1] -10 ,self.getCoordinates().y()+30), text)

        self.drawChilds(qp)

    def mousePressEvent(self, event):
        if(self.isInsideSlider(event.x(), event.y())):
            if (event.buttons() == QtCore.Qt.LeftButton):
                self.storedMouseEventButtons = event.buttons();
                return True



    def mouseMoveEvent(self, event):
        if(self.storedMouseEventButtons == QtCore.Qt.LeftButton):
            #self.mouseMode = GuiConstants.MOUSE_ON_DRAG_MODE
            self.setSliderPositionRel(self.calcSliderPositionRel(event.x()))
            return True


    def mouseReleaseEvent(self, event):
        self.storedMouseEventButtons = None

    def wheelEvent(self,event):
        pass