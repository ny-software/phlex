from PySide import QtGui, QtCore

from PySideGui.GuiTools.PlotableObject import *
from Objects.Conectors.BasicWatcher import *


class SimpleInlinePlot(PlotableObject):
    def __init__(self, guiSettings:GlobalGuiSettings, windowPos:list=[1000, 100], size:QSizeF =QSizeF(400,200)):
        PlotableObject.__init__(self, guiSettings, windowPos)
        self.watcherList = []


        self.setSize(size)
        self.widgetRect = QRectF(self.getCoordinates(), self.getSize())


        self.xListDict = {}
        self.yListDict = {}

        self.bgColor = QtGui.QColor(245,245,245)
        self.plotColor3b = [166,97,26]
        self.axisColor = [136,86,167]

        self.penDict = {}
        self.penColorDict = {}

        self.scaleX=10.0
        self.scaleY=1.5
        self.plotOffsetX = 0.0
        self.plotOffsetY = 30.0
        self.plotOriginX = 30.0
        self.plotOriginY = 30.0
        self.plotWidth=self.widgetRect.width()-60
        self.plotHeight=self.widgetRect.height()-60

        #self.plotableXArea = [10, 300]
        #self.plotableYArea = [10, 300]

        #self.qPointList = []
        #each element is determined by the watcher followed by the qPointList
        # {watcherid01 : [point_list], watcherid02 : [point_list]}
        self.qPointDict = {}

        self.axisPointList = []

        self.gridPointList = []

        #self.axisPlotBatch = pyglet.graphics.Batch()
        #self.addWatcher(watcher)
        self.reset()
        #self.update()

    #workaournd due to pyside - deepcopy problem
    def initPens(self):
        self.pen01 = QtGui.QPen(QtGui.QColor(166,97,26),4, QtCore.Qt.SolidLine)
        for watcherid in self.penColorDict.keys():
            self.penDict.update({watcherid : QtGui.QPen(self.penColorDict.get(watcherid),4, QtCore.Qt.SolidLine)})


        self.penAxis = QtGui.QPen(QtGui.QColor(0,0,0),2, QtCore.Qt.SolidLine)
        self.penGrid = QtGui.QPen(QtGui.QColor(200,200,200),1, QtCore.Qt.DashLine)

    def update(self):
#        self.setAxisPlotBatch()
        #self.lastEndPoint=1
        self.calculateGrid()
        pass

    def reset(self):
        PlotableObject.reset(self)
        #self.plotLineBatch = None
        #self.plotLineBatch = pyglet.graphics.Batch()
        self.initPens()
        #self.qPointList = []
        for pointList in self.qPointDict.values():
            pointList.clear()
#        self.lastEndPoint = 1
        self.widgetRect = QRectF(self.getCoordinates(), self.getSize())
        self.update()


    def updateWindowScaling(self):
        #print("[SimpleInlinePlot]: update Window Scaling to")
        try:
            #if(self.lastEndPoint<3):
            #   return
            xlist = list(self.xListDict.values())
            ylist = list(self.yListDict.values())

            if 0 >= len(ylist) or 0 >= len(xlist) or 0 >= len(ylist[0]) or 0 >= len(xlist[0]):
                return

            minx = xlist[0][0]
            maxx = xlist[0][0]
            miny = ylist[0][0]
            maxy = ylist[0][0]
            #minx = self.xList[0]
            #maxx = self.xList[0]
            #miny = self.yList[0]
            #maxy = self.yList[0]
            for xList in self.xListDict.values():
                for x in xList:
                    if(abs(x)<abs(minx)):
                        minx = x
                    if(abs(x)>abs(maxx)):
                        maxx = x
            for yList in self.yListDict.values():
                for y in yList:
                    if(abs(y)<abs(miny)):
                        miny = y
                    if(abs(y)>abs(maxy)):
                        maxy = y


            #self.plotOffsetX = -minx
            #self.plotOffsetY = -miny
            scaleFactorX = maxx - minx
            if 0 == scaleFactorX:
                scaleFactorX = 1 #Avoids division by zero

            scaleFactorY = maxy - miny
            if 0 == scaleFactorY:
                scaleFactorY = 1 #Avoids divsion by zero

            self.scaleX = self.plotWidth/scaleFactorX
            self.scaleY = self.plotHeight/scaleFactorY
            self.plotOffsetX  = self.plotOriginX-minx*self.scaleX - 0*self.getCoordinates().x()
            self.plotOffsetY  = -self.plotOriginY-(-miny*self.scaleY - 0*self.getCoordinates().y() - 0* self.height)
        except:
            print("[SimpleInlinePlot]: updateWindowScaling failed ")
        #print("coords: ", self.getCoordinates().x(), self.getCoordinates().y(), " scale: ", self.scaleX, self.scaleY, " offset ", self.plotOffsetX, self.plotOffsetY)

    def setSize(self, size:QSizeF):
        self.size = size
        self.width=size.width()
        self.height=size.height()

    def getSize(self):
        return self.size

    def setKeys(self, xkey:str, ykey:str):
        self.xkey = xkey
        self.ykey = ykey
        for watcher in self.watcherList:
            if watcher is not None:
                self.xListDict.update({watcher.getObjectID() : watcher.getObservedList(self.xkey)})
                self.yListDict.update({watcher.getObjectID() : watcher.getObservedList(self.ykey)})
                self.qPointDict.update({watcher.getObjectID(): []})

    def setPlotOffset(self, xoff, yoff):
        self.plotOffsetX = xoff
        self.plotOffsetY = yoff

    def setPlotScale(self, xsc, ysc):
        self.scaleX = xsc
        self.scaleY = ysc

#    def setWatcher(self, watcher:BasicWatcher):
#        self.watcher = watcher

    def addWatcher(self, watcher:BasicWatcher, color:QtGui.QColor=QtGui.QColor(166,97,26)):
        #for watcher in self.watcherList:
        if watcher is not None:
            self.watcherList.append(watcher)
            self.penColorDict.update({watcher.getObjectID(): color})
            self.qPointDict.update({watcher.getObjectID(): []})
            if self.xkey is not None and self.ykey is not None:
                self.xListDict.update({watcher.getObjectID() : watcher.getObservedList(self.xkey)})
                self.yListDict.update({watcher.getObjectID() : watcher.getObservedList(self.ykey)})


    def getWatcher(self):
        return self.watcher

    def updateWatcher(self, listOfWatchers):
        try:
            if self.watcher:
                for watcher in listOfWatchers.values():
                    if(self.watcher.getObjectID() == watcher.getObjectID()):
                        self.watcher = watcher
        except AttributeError:
            pass
        self.setKeys(self.xkey, self.ykey)


    def calculateGrid(self):
        self.gridPointList = []
        for x in range(int(self.plotOriginX), int(self.plotOriginX+self.plotWidth), 20):
            for y in range (int(self.plotOriginY), int(self.plotOriginY+self.plotHeight), 20):
                self.gridPointList.extend([QPointF(x, self.plotOriginY)+self.getCoordinates() , QPointF(x,self.plotOriginX+self.plotHeight) + self.getCoordinates()])
                self.gridPointList.extend([QPointF(self.plotOriginX, y)+self.getCoordinates() , QPointF(self.plotOriginX+self.plotWidth, y)+self.getCoordinates()])

    def drawGrid(self, qp):
        qp.setPen(self.penGrid)
        qp.drawLines(self.gridPointList)

    def drawAxis(self, qp:QtGui.QPainter):
        self.axisPointList  = []
        #x-axis
        #self.axisPointList.extend(self.generateArrow(QPointF(0.02*self.widgetRect.width(), 0.95*self.widgetRect.height())+self.getCoordinates(), QPointF(1.0*self.plotWidth, 0.0)))
        #self.axisPointList.extend([ QPointF(0.02*self.widgetRect.width(), 0.95*self.widgetRect.height())+self.getCoordinates(),QPointF(0.95*self.widgetRect.width(), 0.95*self.widgetRect.height())+self.getCoordinates()  ])
        #self.axisPointList.extend(self.generateArrow(QPointF(0.95*self.widgetRect.width(), 0.95*self.widgetRect.height())+self.getCoordinates(), QPointF(10.0, 0.0)))
        self.axisPointList.extend([ QPointF(self.plotOriginX, self.plotOriginY+self.plotHeight)+self.getCoordinates(),QPointF(self.plotOriginX+self.plotWidth, self.plotOriginY+self.plotHeight)+self.getCoordinates()  ])
        self.axisPointList.extend(self.generateArrow(QPointF(self.plotOriginX+self.plotWidth, self.plotOriginY+self.plotHeight)+self.getCoordinates(), QPointF(10.0, 0.0)))

#

        #self.axisPointList.extend(self.generateArrow(QPointF(0.05*self.widgetRect.width(), 0.98*self.widgetRect.height())+self.getCoordinates(), QPointF(0.0, -1.1*self.plotHeight)))
        self.axisPointList.extend([ QPointF(self.plotOriginX, self.plotOriginY+self.plotHeight)+self.getCoordinates(),QPointF(self.plotOriginX, self.plotOriginY)+self.getCoordinates()  ])
        self.axisPointList.extend(self.generateArrow(QPointF(self.plotOriginX, self.plotOriginY)+self.getCoordinates(), QPointF(0.0, -10.0)))
        qp.setPen(self.penAxis)
        qp.drawLines(self.axisPointList)

    def realToRelativePlotCoordsX(self, x):
        return x*self.scaleX + self.plotOffsetX
    def realToRelativePlotCoordsY(self, y):
        return self.height-y*self.scaleY  + self.plotOffsetY

    def realToAbsolutePlotCoordsX(self, x):
        return self.realToRelativePlotCoordsX(x) + self.getCoordinates().x()
    def realToAbsolutePlotCoordsY(self, y):
        return self.realToRelativePlotCoordsY(y) + self.getCoordinates().y()

    def draw(self, qp:QtGui.QPainter):
        qp.setPen(self.penAxis)
        qp.setBrush(self.bgColor)

        qp.drawRect(self.widgetRect)
        self.drawGrid(qp)

        self.drawAxis(qp)

        qp.setPen(self.pen01)
        #thisEndPoint = len(self.xList)-1
        #if the watched list became smaller -> reset and redraw!
        self.updateWindowScaling() #TODO Fix function
        self.reset()
        #if(thisEndPoint+2 < self.lastEndPoint or 0 > self.lastEndPoint ):
        #    self.reset()
        #    self.draw(qp)
        #    return

        #for i in range(self.lastEndPoint, thisEndPoint, 1):
        #        self.qPointList.append(QtCore.QPointF(self.realToAbsolutePlotCoordsX(self.xList[i-1]),self.realToAbsolutePlotCoordsY(self.yList[i-1])))
        #        #print("self.qPointList.append(QtCore.QPointF(self.realToAbsolutePlotCoordsX(self.xList[i-1]),self.realToAbsolutePlotCoordsY(self.yList[i-1])))")
        #        self.qPointList.append(QtCore.QPointF(self.realToAbsolutePlotCoordsX(self.xList[i]),self.realToAbsolutePlotCoordsY(self.yList[i])))
        #        #print(self.xList[i], self.yList[i], QtCore.QPointF(self.realToAbsolutePlotCoordsX(self.xList[i]),self.realToAbsolutePlotCoordsY(self.yList[i])))

        for watcherid in self.qPointDict.keys():
            for i in range(len(self.qPointDict.get(watcherid))+1, len(self.xListDict.get(watcherid)), 1):
                self.qPointDict.get(watcherid).append(QtCore.QPointF(self.realToAbsolutePlotCoordsX(self.xListDict.get(watcherid)[i-1]),self.realToAbsolutePlotCoordsY(self.yListDict.get(watcherid)[i-1])))
                self.qPointDict.get(watcherid).append(QtCore.QPointF(self.realToAbsolutePlotCoordsX(self.xListDict.get(watcherid)[i]),self.realToAbsolutePlotCoordsY(self.yListDict.get(watcherid)[i])))
            qp.setPen(self.penDict.get(watcherid))
            qp.drawLines(self.qPointDict.get(watcherid))

        #self.lastEndPoint = thisEndPoint

        #self.plotLineBatch.draw()
        #print(self.qPointList)
        #qp.drawLines(self.qPointList)

        self.drawChilds(qp)



    def onDragRelease(self):
        self.reset()

    def isInside(self, xin, yin):
        return self.widgetRect.contains(xin, yin)

    def mousePressEvent(self, event):
        pass
    def mouseMoveEvent(self, event):
        pass
    def mouseReleaseEvent(self, event):
        pass
    def wheelEvent(self,event):
        pass


    def generateArrow(self, point, direction):
        replacment = 0.0
        arrowList = []
        vA = point-(replacment)*direction
        vB = point+(1.0-replacment)*direction
        arrowList.extend([vA,vB])
        v2 = -0.5*direction #arrow width
        v2n = QPointF( v2.y(), -v2.x())
        vC = point+(1.0-replacment-1.0)*direction+v2n
        vB2 = point+(1.0-replacment)*direction
        vD = point+(1.0-replacment-1.0)*direction-v2n
        arrowList.extend([vC,vB,vB2,vD])
        return arrowList

