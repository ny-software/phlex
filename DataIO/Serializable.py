import abc
import uuid

class Serializeable(object):
    """
    Interface for serializing objects, needed for file management
    """
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        self.__uid = ""

    @property
    def uid(self) -> str:
        """
        Getter for this objects unique identifier string, if it does not exist a new UID will be generated
        :return: A string containing the unique identifier string
        """
        if 0 == len(self.__uid):
            self.__uid = uuid.uuid1()
        return self.__uid

    @uid.setter
    def uid(self, uidstr: str):
        self.__uid = uidstr

    @abc.abstractmethod
    def serialize(self) -> str:
        """
        Serializes this object into a XML string compatible to the asset schema

        :return:
        """
        pass

    @abc.abstractmethod
    def deserialize(self, xmlObj) -> None:
        """
        Processes an xmlObject and extracts its data payload

        :param xmlObj: An etree XMLNode containing an object
        """
        pass
