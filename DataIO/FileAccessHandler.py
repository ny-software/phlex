
import abc
from sandboxes import BasicSandbox

class FileAccessHandler(object):
    """This class is the file interface for data saving and loading from disk"""
    __metaclass__ = abc.ABCMeta

    @abc.abstractclassmethod
    def readFile(self, file:str, sandbox:BasicSandbox) -> bool:
        pass

    @abc.abstractclassmethod
    def writeFile(self, file:str, sandbox:BasicSandbox) -> bool:
        pass
