import xml.dom.minidom
from xml.dom.minidom import parse

from Asset import AssetValidator
from DataIO import FileAccessHandler
from Objects import ObjectFactory
from sandboxes import BasicSandbox


class XMLFileAccessHandler(FileAccessHandler):
    def __init__(self, assetValidator: AssetValidator):
        super(FileAccessHandler, self).__init__()
        self.__assetValidator = assetValidator

    def readFile(self, file:str, sandbox:BasicSandbox) -> bool:
        """
        Reads a string coherent to the asset schema and populates a sandbox with
        the contained objects.

        1. validate the xml string with the asset schema and try to convert it
           to the newest asset version if needed
        2. initialize empty id->object hash map
        3. Get the sandbox tag from the xml string
        4. For all object tags in the sandbox tag
            4.1 find the correct object type
            4.2 use the object type's serialization interface to build the object
            4.3 add object to the id->object hash map
        5. TODO

        :param xmlString: An xml string coherent to the asset schema
        :param sandbox: A sandbox object, parsed objects will be appended to this sandbox
        :return: True on success, False is an error state has beeen reached
        """

        if not self.__assetValidator.validate(file):
            return False

        objDict = dict()

        try:
            document = xml.dom.minidom.parse(file)
            Xsandbox = document.getElementsByTagName("sandbox")[0]

            for xmlObj in Xsandbox.getElementsByTagname("object"):
                classname = xmlObj.getElementsByTagname("objecttype")
                phlObj = ObjectFactory.produceSerializableObject(classname)
                phlObj.deserialize(xmlObj)
                if None != objDict[phlObj.getUid()]:
                    return False
                objDict[phlObj.getUid()] = phlObj

            #TODO: implement
        except FileNotFoundError:
            return False
        return True

    def writeFile(self, file:str, sandbox:BasicSandbox) -> bool:
        """
        Method to write a complete sandbox into a xml file

        1. open file, overwrite if exists
        2. for each sandbox item:
            2.1 serialize item payload
            2.2 generate item uid if it does not exist
            2.3 sort element into hash table <object reference> -> <uid>
            2.4 resolve all item dependencies to uids
            2.5 write xml item
            2.6 validate xml item to schema file
        3. close file
        4. validate file to schema

        :param file: A string describing the path to the target file
        :param sandbox: The source sandbox
        :return: True on success, False if an error state has been reached
        """
        #TODO: implement
        return True
