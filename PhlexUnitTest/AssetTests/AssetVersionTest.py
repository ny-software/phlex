import unittest
from Asset import AssetVersion

class AssetVersionTest_initialization(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def runTest(self):
        # Test empty initialization
        try:
            version = AssetVersion()
            vHash = version.__hash__()
            self.fail("Accessing version numbers of empty initializations should throw an AttributeError")
        except AttributeError:
            # Test string initialization fail
            try:
                version = AssetVersion("-1.0.0.0")
                self.fail("Initialization with negative numbers should throw an SyntaxError")
            except SyntaxError:
                # Test valid string initialization
                version = AssetVersion("0.0.0.0")
                vHash = version.__hash__()
                tHash = hash(hash("AssetVersion" + str(0) + str(0) + str(0) + str(0)))
                self.assertEqual(tHash, vHash)

            # Test incomplete number initialization
            try:
                version = AssetVersion(majorVersion=0,minorVersion=0,patchVersion=0)
                self.fail("Initialization with less than 4 numbers should throw a SyntaxError")
            except SyntaxError:
                # Test negative number initialization
                try:
                    version = AssetVersion(majorVersion=-1,minorVersion=0,patchVersion=0,tweakVersion=0)
                    self.fail("Initialization with negative numbers should throw a SyntaxError")
                except SyntaxError:
                    # Test correct number initialization
                    version = AssetVersion(majorVersion=1,minorVersion=2,patchVersion=3,tweakVersion=4)
                    vHash = version.__hash__()
                    tHash = hash(hash("AssetVersion" + str(1) + str(2) + str(3) + str(4)))
                    self.assertEqual(tHash, vHash)


class AssetVersionTest_parsing(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def runTest(self):
        version = AssetVersion()
        # Test with incorrect version strings
        self.assertFalse(version.parseVersionString("abdeser"), "Parsing incorrectly formated strings should result in "
                                                                "False as return value")
        self.assertFalse(version.parseVersionString("0.0.0"), "Parsing version strings with 3 numbers should result in "
                                                              "False as return value")
        self.assertFalse(version.parseVersionString("-1.0.0.0"), "Parsing version strings with negative numbers should "
                                                                 "result in False as return value")
        # Test with correct version strings
        self.assertTrue(version.parseVersionString("1.2.3.4"))
        self.assertEqual(1, version.majorVersion)
        self.assertEqual(2, version.minorVersion)
        self.assertEqual(3, version.patchVersion)
        self.assertEqual(4, version.tweakVersion)


def getTestInstances() -> [unittest.TestCase]:
    return [AssetVersionTest_initialization(), AssetVersionTest_parsing()]