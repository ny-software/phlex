import unittest
#ToDo: change that the __init__ file is eneought -> ask tobi....
from PhysicalQuantities.BasicQuantity import *
from Units.UnitFactory import *

class BasicQuantityTest_basic_math(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def runTest(self):
        bq1=BasicQuantity(1,UnitFactory.generate(UnitKeys.DISTANCE.METER))
        bq2=BasicQuantity(1,UnitFactory.generate(UnitKeys.DISTANCE.METER))

        #additon
        bq3Exp = BasicQuantity(2,UnitFactory.generate(UnitKeys.DISTANCE.METER))
        bq3=bq1 + bq2
        #check value
        self.assertEqual(bq3.value, bq3Exp.value)
        #check equality operator
        self.assertEqual(bq3, bq3Exp)

def getTestInstances() -> [unittest.TestCase]:
    return [BasicQuantityTest_basic_math()]

