import unittest
import os
import os.path as op
import importlib

__all__ = [
    "buildTestSuite"
]

def buildTestSuite() -> unittest.TestSuite:
    testSuite = unittest.TestSuite()
    for file in [
        f.split('.', 1)[0]
        for f in os.listdir(op.dirname(op.realpath(__file__)))  # list contents of current dir
        if not f.startswith('_') and
                not op.isdir(op.join(op.dirname(op.realpath(__file__)), f)) and
                f.endswith('.py')
        ]:
        subdir = op.basename(op.dirname(__file__))
        mod = "PhlexUnitTest." + subdir + "." + file
        module = importlib.import_module(mod)
        func = getattr(module, "getTestInstances")
        testSuite.addTests(func())
    return testSuite

if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    testsuite = buildTestSuite()
    runner.run(testsuite)
