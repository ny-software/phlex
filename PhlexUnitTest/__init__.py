import unittest
import os
import os.path as op
import importlib

__all__ = [
    f
    for f in os.listdir(op.dirname(op.realpath(__file__)))  # list contents of current dir
    if not f.startswith('_') and
       op.isdir(op.join(op.dirname(op.realpath(__file__)), f)) and
       op.isfile(op.join(op.dirname(op.realpath(__file__)), f, '__init__.py'))
]

phlexUnitTestSuite = unittest.TestSuite()
for subdir in __all__:
    functionPathStr = "PhlexUnitTest." + subdir + ".buildTestSuite"
    modName, funcName = functionPathStr.rsplit('.', 1)
    mod = importlib.import_module(modName)
    func = getattr(mod, funcName)
    phlexUnitTestSuite.addTests(func())

if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(phlexUnitTestSuite)
