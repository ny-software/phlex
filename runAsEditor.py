from PySideGui.ApplicationWindow import *
from Model import PhlexApplicationModel
from Controller.CommandLineController import *
from Engines.SimpleMechanicsEngine import *
from Sandbox.SimpleMechanicSandbox import *
import sys


def main():
    app = QtGui.QApplication(sys.argv)
    sandbox = SimpleMechanicSandbox()
    engine = SimpleMechanicsEngine()
    model = PhlexApplicationModel.PhlexApplicationModel(sandbox, engine)
    controller = CommandLineController(model)
    appWindow = ApplicationWindow(model, controller)
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
