#class Name:  CommandLineController
#Author: djk

#import matplotlib.pyplot as plt
from Objects.forceFields.AmbiousAir import *
from Objects.EventSubject import *
from Controller.ActionManager import *
from Controller.Action import *
from Model import *

class BasicController(object):
    """
    The controller in the MFC sceme
    It contains a Sandbox as well as the Simulationengine and takes care about
    creation, deletion of objects
    handling of the simulation
    general communication to the underlying model
    """

    def __init__(self, model:PhlexApplicationModel):
        super(BasicController, self).__init__()
        self.modificationActionManager = ActionManager()
        self.__model = model

    def initializeEngine(self, sandbox:BasicSandbox):
        self.__model.engine.start(sandbox)

    def addModificationAction(self, modificationAction:Action):
        success = self.modificationActionManager.doAction(modificationAction)
        return success

    def undoLastModificationAction(self) -> bool:
        success = self.modificationActionManager.undoLastAction()
        return success

    def redoLastModificationAction(self) -> bool:
        success = self.modificationActionManager.redoLastAction()
        return success

    def createObject(self, object:BasicObject) -> bool:
        """
        Adds a new Object to the dictionary in the sandbox.
        Important: the name of the object is not allowd to be used more than once
        :param object: Object which will be added to the Sandbox dictionary: should be of tye BasicObject
        :return: True: if the object was succsesfuly added, otherwise False
        """
        return self.__model.addObjectToSandbox(object)

    def deleteObject(self, obj:BasicObject):
        """
        Deletes an object from the Dictionary of the Sandbox
        :param obj: Object which will be deleted from the Sandbox dictionary: should be of tye BasicObject
        :return: True: if the object was successfully deleted, otherwise False
        """
        return self.__model.deleteObjectFromSandbox(object)

    def selectObject(self, obj):
        self.__model.deselectAllObjects()
        self.__model.setObjectSelectionStatus(obj, True)

    def deselectAllObjects(self):
        self.__model.deselectAllObjects()

    def deselectObject(self, obj):
        self.mainSandbox.setObjectSelectionStatus(obj, False)

    def startSimulation(self):
        self.__model.startSimulation()

    def stopSimulation(self):
        self.__model.stopSimulation()

    def resetSimulation(self):
        self.__model.resetSimulation()

