import abc
from Sandbox.BasicSandbox import BasicSandbox
from Objects.BasicObject import BasicObject

class Action(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        pass

    @abc.abstractmethod
    def do(self) -> bool:
        """Apply all changes the Action contains"""
        pass

    @abc.abstractmethod
    def undo(self) -> bool:
        """Unapply all changes the Action contains"""
        pass


class CreateObjectAction(Action):
    """Action to add a new object"""

    def __init__(self, manipulatedObject:BasicObject):
        super(CreateObjectAction, self).__init__()
        self._object = manipulatedObject

    def setSandbox(self, sandbox:BasicSandbox) -> None:
        self._sandbox = sandbox

    def do(self) -> bool:
        if self._sandbox:
            self._sandbox.addNewObject(self._object)
            return True #TODO get success out of sandbox
        return False

    def undo(self) -> bool:
        if self._sandbox:
            self._sandbox.deleteObject(self._object)
            return True #TODO get success out of sandbox
        return False

class DeleteObjectAction(Action):
    """Action to delete an object form a sandbox"""

    def __init__(self, manipulatedObject:BasicObject):
        super(DeleteObjectAction, self).__init__()
        self._object = manipulatedObject

    def setSandbox(self, sandbox:BasicSandbox) -> None:
        self._sandbox = sandbox

    def do(self) -> bool:
        if self._sandbox:
            self._sandbox.deleteObject(self._object)
            return True #TODO get success out of sandbox
        return False

    def undo(self) -> bool:
        if self._sandbox:
            self._sandbox.addNewObject(self._object)
            return True #TODO get success out of sandbox
        return False
