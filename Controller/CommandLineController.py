#class Name:  CommandLineController
#Author: djk

import re #regular expression

from Controller.BasicController import *
from Objects.ComplexObjects.TestSetupts import *
from Controller.Action import *
from Model import *

class CommandLineController(BasicController):
    """
    The controller in the MFC sceme
    It contains a Sandbox as well as the Simulationengine and takes care about
    creation, deletion of objects
    handling of the simulation
    general communication to the underlying model
    """

    def __init__(self, model:PhlexApplicationModel):
        BasicController.__init__(self, model)

        self.massPointWatcher=None
        self.massPointWatcher2=None

        self.daemon = True
        self.cancelled = False

        #only for testing
        self.thread = None
        self.thread2 = None
        print("Initialisation of Command Line Controller done!")


    def run(self):
        #self.mainSandbox.objectList.get("mp0").centerOfMassVelocity = np.array([0.5, 0.5])
        print("Command Line Controller Started!")
        inputCmd = input("Command:  ")
        try:
            self.processCommand(inputCmd)
        except:
            pass
        while(not self.cancelled):
            inputCmd = input("Command:  ")
            #try:
            result = self.processCommand(inputCmd)
            if not result:
                print("Failed!")


            #except:
            #    print("Input Error")


    def cancel(self):
        """End this timer thread"""
        self.cancelled = True

    def processCommand(self, command:str) -> bool:
        print(command)

        try:
            lhs, rhs = command.split("=")
        except:
            lhs=None
            rhs=command

        commandList = re.findall(r"[\w']+", rhs)
        print(commandList)
        if(len(commandList)<1):
            return False

        if (commandList[0]=="Start"):
            self.startSimulation()
            return True
        if (commandList[0]=="Pause"):
            self.__model.pauseSimulation()
            return True
        if (commandList[0]=="Stop"):
            self.stopSimulation()
            return True
        if (commandList[0]=="Exit"):
            self.stopSimulation()
            self.cancel()
            return True
        if (commandList[0]=="Test"):
            return True
        if (commandList[0]=="Create" and len(commandList)==5):
            print("create")
            #Create(Circle, x, y, r)
            if(commandList[1]=="Circle"):
                obj = CircleObject(radius=int(commandList[4]), id=lhs)
                obj.setCenterOfMass([int(commandList[2]), int(commandList[3])])
                action = CreateObjectAction(obj)
                action.setSandbox(self.mainSandbox)
                success = self.addModificationAction(action)
                return success
            return False
        if (commandList[0]=="Delete" and len(commandList)==2):
            print("delete")
            obj = self.mainSandbox.getObjectById(commandList[1])
            if None != obj:
                action = DeleteObjectAction(obj)
                success = self.addModificationAction(action)
                return success
            return False
        if (commandList[0]=="ObjectList"):
            for obj in self.mainSandbox.getObjectList().values():
                print(obj)
            return True
        if (commandList[0]=="Undo" and len(commandList)==1):
            return self.undoLastModificationAction()
        if (commandList[0]=="Redo" and len(commandList)==1):
            return self.redoLastModificationAction()