from Controller.Action import Action

class ActionManager(object):
    def __init__(self):
        self.doneStack = []
        self.undoneStack = []

    def doAction(self, action:Action) -> bool:
        success = action.do()
        if success:
            self.doneStack.append(action)
        return success

    def undoLastAction(self) -> bool:
        if 0 >= len(self.doneStack):
            return False
        action = self.doneStack.pop()
        success = action.undo()
        if success:
            self.undoneStack.append(action)
        else:
            self.doneStack.append(action)
        return success

    def redoLastAction(self) -> bool:
        if 0 >= len(self.undoneStack):
            return False
        action = self.undoneStack.pop()
        success = action.do()
        if success:
            self.doneStack.append(action)
        else:
            self.undoneStack.append(action)
        return success


