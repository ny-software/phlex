from Model import AbstractModel, PhlexModelNotificationObject
from Model.PhlexModelNotificationObject import PhlexModelNotificationObject
from Sandbox import *
from Engines import *
from Objects.BasicObject import *
import sys

class PhlexApplicationModel(AbstractModel.AbstractModel):
    """
    Phlex application main model
    """

    def __init__(self, sandbox:BasicSandbox, engine:Engine):
        super(PhlexApplicationModel, self).__init__()

        self.__sandbox = sandbox
        self.__engine = engine

    @property
    def sandbox(self) -> BasicSandbox:
        return self.__sandbox

    @property
    def engine(self) -> Engine:
        return self.__engine

    def buildDefaultNotificationObject(self) -> PhlexModelNotificationObject:
        nObj = PhlexModelNotificationObject()
        nObj.simulationRunning = self.isRunning()
        return nObj

    def addObjectToSandbox(self, object:BasicObject) -> bool:
        if isinstance(object, BasicObject) :
            self.__sandbox.addNewObject(object)
            self.notifyObservers(self.buildDefaultNotificationObject())
            return True
        print("Tried to add non physical object to sandbox: ", object, file=sys.stderr)
        return False

    def deleteObjectFromSandbox(self, object:BasicObject) -> bool:
        #ToDo: handle connection between objects linked to this one which is removed
        #ToDo: synchronize with engines
        #if isinstance(object, BasicObject) :
        #self.engine.queueObjectDeletion(object)
        success = self.sandbox.deleteObject(object)
        if success:
            self.notifyObservers(self.buildDefaultNotificationObject())
            print("deletion send to engine for object ", object)
            return True
        return False
        #print("Tried to delete non physical object from sandbox: ", object,  file=sys.stderr)

    def setObjectSelectionStatus(self, object:BasicObject, selected:bool) -> None:
        if selected:
            self.__sandbox.selectObject(object)
        else:
            self.__sandbox.deselectObject(object)
        self.notifyObservers(self.buildDefaultNotificationObject())

    def deselectAllObjects(self):
        self.__sandbox.deselectAllObjects()
        self.notifyObservers(self.buildDefaultNotificationObject())

    def startSimulation(self) -> None:
        self.__engine.startSimulation()
        self.notifyObservers(self.buildDefaultNotificationObject())

    def stopSimulation(self) -> None:
        self.__engine.pauseSimulation()
        notificationObject = PhlexModelNotificationObject()
        self.notifyObservers(self.buildDefaultNotificationObject())

    def resetSimulation(self) -> None:
        self.__engine.resetSimulation()
        notificationObject = self.buildDefaultNotificationObject()
        notificationObject.simulationReset = True
        self.notifyObservers(notificationObject)

    def isRunning(self) -> bool:
        return self.__engine.isRunning()

