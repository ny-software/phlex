from Model.AbstractModel import NotificationObject

class PhlexModelNotificationObject(NotificationObject):
    def __init__(self, simulationRunning:bool = False, simulationReset:bool = False):
        self.simulationRunning = simulationRunning
        self.simulationReset = simulationReset
