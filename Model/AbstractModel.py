import abc
import copy

class NotificationObject(object):
    __metaclass__ = abc.ABCMeta

class ModelObserver(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def notifyModelChanged(self, notificationObject:NotificationObject):
        pass

class AbstractModel(object):

    __metaclass__ = abc.ABCMeta

    def __init__(self):
        super(AbstractModel, self).__init__()
        self.__observers = []

    def registerObserver(self, observer:ModelObserver) -> None:
        if not observer in self.__observers:
            self.__observers.append(observer)

    def removeObserver(self, observer:ModelObserver) -> None:
        idx = self.__observers.index(observer)
        if (not None == idx) and 0 >= idx:
            self.__observers.remove(observer)

    def notifyObservers(self, notificationObject:NotificationObject):
        for observer in self.__observers:
            observer.notifyModelChanged(copy.deepcopy(notificationObject))

    @abc.abstractmethod
    def buildDefaultNotificationObject(self) -> NotificationObject:
        pass