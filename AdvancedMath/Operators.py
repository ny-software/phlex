#class Name:  Operator
#Author: djk


from enum import Enum
class Operator(Enum):
    MULTIPLY = 1
    DIVIDE = 2
    ADD = 3
    SUBTRACT = 4

    def toString(op):
        if(op==Operator.MULTIPLY):
            return "*"
        if(op==Operator.DIVIDE):
            return "/"
        if(op==Operator.ADD):
            return "+"
        if(op==Operator.SUBTRACT):
            return "-"