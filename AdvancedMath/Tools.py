
from math import *
import numpy as np

def scalar(vec1, vec2):
    return vec1[0]*vec2[0] + vec1[1]*vec2[1]

def norm(vec1):
    return sqrt(scalar(vec1, vec1))

def normal(vec):
    return np.array([-vec[1], vec[0]])

def angleVec(vec):
    return atan2(vec[1], vec[0])/pi*180

def angleVec2(vec1, vec2=np.array([1.0,0.0])):
    return acos(scalar(vec1, vec2)/norm(vec1)/norm(vec2))/pi*180



def listToDict(oldlist:list) -> dict:
    """
    Creates from a list [obj1, obj2,...] and dict in the form
    {obj1.getObjectID(): obj1, obj2.getObjectId(): obj2, ...}
    :param oldlist: input list
    :return: oldlist transformed into dict format
    """
    newDict = {}
    for elm in oldlist:
        newDict.update({elm.getObjectID():elm})
    return newDict