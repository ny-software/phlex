from Asset.AssetVersion import AssetVersion
from Asset.Converter import *
from Asset.Schema import *

__all__ = [
    "AssetVersion",
    "AssetValidator",
    "tryConvertAsset"
]

def tryConvertAsset(sourceVersion:AssetVersion, targetVersion:AssetVersion, assetPath:str) -> (bool, str):
    """
    This function tries to convert an asset from a source to a certain target version

    :param sourceVersion:
    :param targetVersion:
    :param assetPath:
    :return: a tuple (bool, string) signifying the conversion success and the path to the resulting asset file
    """
    if sourceVersion == targetVersion:
        return (True, assetPath)

    converterChain = generateConverterChain(sourceVersion, targetVersion)

    if 0 == len(converterChain):
        return (False, None)

    #Commence converter chain
    sourceAssetPath = assetPath
    targetAssetPath = ""
    #for each converter chain element
    for assetConverter in converterChain:
        #1. generate conversion target asset path
        chainSourceVersion = assetConverter.sourceVersion
        chainTargetVersion = assetConverter.targetVersion
        targetAssetPath = generateConversionTargetPath(sourceAssetPath, chainSourceVersion, chainTargetVersion)
        #2. validate the source asset
        sourceValidationSuccess = validateAsset(sourceAssetPath, chainSourceVersion)
        if not sourceValidationSuccess:
            return (False, None)

        #3. convert the asset
        assetConverter.convert(sourceAssetPath, targetAssetPath)

        #4. validate the target asset
        targetValidationSuccess = validateAsset(targetAssetPath, chainTargetVersion)
        if not targetValidationSuccess:
            return (False, None)

        sourceAssetPath = targetAssetPath

    #do a final asset validation to the target version
    finalValidationSuccess = validateAsset(targetAssetPath, targetVersion)

    if finalValidationSuccess:
        return (True, targetAssetPath)

    return (False, None)
