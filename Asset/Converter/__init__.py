from Asset.Converter import AssetConverter
from Asset.AssetVersion import AssetVersion

__all__ = [
    "generateConverterChain",
    "generateConversionTargetPath"
]

def generateConverterChain(sourceVersion:AssetVersion, targetVersion:AssetVersion) -> [AssetConverter]:
    """
    Finds the correct asset converter chain for a given asset version

    :param sourceVersion: The converter chain start version
    :Param targetVersion: The converter chain end version
    :return: A List of asset converters to be used sequentially
    """
    #TODO: implement
    pass

def generateConversionTargetPath(sourceAssetPath:str, sourceVersion:AssetVersion, targetVersion:AssetVersion) -> str:
    """
    Generates a full path to the target of an asset conversion

    :param sourceAssetPath:
    :param sourceVersion:
    :param targetVersion:
    :return:
    """
    #TODO: implement
    pass
