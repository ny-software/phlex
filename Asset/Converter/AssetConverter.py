import abc
from Asset.AssetVersion import AssetVersion

class AssetConverter(object):
    """
    Abstract base class for asset converter
    """
    __metaclass__ = abc.ABCMeta

    def __init__(self, sourceVersion:AssetVersion, targetVersion:AssetVersion):
        self.__sourceVersion = sourceVersion
        self.__targetVersion = targetVersion

    def __hash__(self):
        return hash("AssetConverter", self.__sourceVersion, self.__targetVersion)

    @property
    def sourceVersion(self) -> AssetVersion:
        return self.__sourceVersion

    @property
    def targetVersion(self) -> AssetVersion:
        return self.__targetVersion

    @abc.abstractmethod
    def convert(self, sourceAssetPath:str, targetAssetPath:str) -> bool:
        """
        Abstract conversion function for an asset of this converter's source version to a new asset of this converter's target version

        :param sourceAssetPath: The path to the asset which will be converted
        :param targetAssetPath: The path the converted asset should be written to
        :return:
        """
        pass