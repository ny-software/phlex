from Asset.AssetVersion import AssetVersion
import os
import os.path as op
from xml.dom.minidom import parse
import xml.dom.minidom
from lxml import etree

__all__ = [
    "getSchemaPath",
    "validateAsset"
]

currentAssetVersion = AssetVersion(versionString="1.0.0.0")

assetSchemaDictionary = dict()

for schemaFile in [
                    op.join(op.dirname(op.realpath(__file__)), f)
                    for f in os.listdir(op.dirname(op.realpath(__file__)))  # list contents of current dir
                    if (not f.startswith('_')) and op.isfile(op.join(op.dirname(op.realpath(__file__)), f)) and f.endswith('.xsd')
                  ]:
    schemaFileDOM = xml.dom.minidom.parse(schemaFile)
    schemaElements = schemaFileDOM.getElementsByTagName("xsd:shema")
    if 1 == schemaElements.length:
        versionAttribute = schemaElements[0].getAttribute("version")
        assetVersion = AssetVersion(versionAttribute)
        assetSchemaDictionary[assetVersion] = schemaFile

def getSchemaPath(version:AssetVersion) -> str:
    """
    Returns the asset schema for a certain asset version

    :param version:
    :return: A string containing the path to the asset schema or None if no schema exists for the given version
    """
    try:
        return assetSchemaDictionary[version]
    except KeyError:
        return None

def getSchemaPath() -> str:
    """
    Returns the current asset schema

    :return: A string containing the path to the current asset schema or None if no schema exists for the current asset version
    """
    return getSchemaPath(currentAssetVersion)

def validateAsset(assetPath:str, version:AssetVersion) -> bool:
    """
    Validates an asset with a schema file

    :param assetPath: Path to the asset which should be validated
    :param version: Asset version to be validated to
    :return: True if validation succeeded, False if not
    """
    schemaPath = getSchemaPath(version)
    if None == schemaPath:
        return False

    assetSchemaFile = open(schemaPath, "r")
    assetSchemaDOC = etree.parse(assetSchemaFile.read())
    assetSchema = etree.XMLSchema(assetSchemaDOC)

    xmlStringDOC = etree.parse(assetPath)

    return assetSchema.validate(xmlStringDOC)
