import re

class AssetVersion(object):
    def __init__(self, versionString:str = None, majorVersion:int = -1, minorVersion:int = -1, patchVersion:int = -1, tweakVersion:int = -1):
        if None != versionString:
            if not self.parseVersionString(versionString):
                raise SyntaxError
            return

        if not (-1 == majorVersion and -1 == minorVersion and -1 == patchVersion and -1 == tweakVersion):
            versionString = str(majorVersion) + "." + str(minorVersion) + "." + str(patchVersion) + "." + str(tweakVersion)
            if not self.parseVersionString(versionString):
                raise SyntaxError

    def __eq__(self, other:'AssetVersion'):
        return self.__majorVersion == other.majorVersion and \
               self.__minorVersion == other.minorVersion and \
               self.__patchVersion == other.patchVersion and \
               self.__tweakVersion == other.patchVersion

    def __hash__(self):
        return hash("AssetVersion" + str(self.__majorVersion) + str(self.__minorVersion) + str(self.__patchVersion) + str(self.__tweakVersion))

    def parseVersionString(self, versionString:str) -> bool:
        """
        Builds this object by parsing a version string in the form of:
            <Major Version Number>.<Minor Version Number>.<Patch Version Number>.<Tweak Version Number>

        :param versionString: The version string
        :return: True if parsing was successful, False otherwise
        """
        patternStr = "(\d+)\.(\d+)\.(\d+)\.(\d+)"
        matcher = re.match(patternStr, versionString)
        if None == matcher:
            return False

        self.__majorVersion = int(matcher.group(1))
        self.__minorVersion = int(matcher.group(2))
        self.__patchVersion = int(matcher.group(3))
        self.__tweakVersion = int(matcher.group(4))

        return True

    @property
    def majorVersion(self) -> int:
        return self.__majorVersion

    @property
    def minorVersion(self) -> int:
        return self.__minorVersion

    @property
    def patchVersion(self) -> int:
        return self.__patchVersion

    @property
    def tweakVersion(self) -> int:
        return self.__tweakVersion
