from Units.Unit import *
from Units.UnitTypes import *

class UnitFactory(object):
    # Create based on class name:
    def generate(type):
        if type == UnitKeys.DISTANCE.METER: return BaseUnit(1, 0, "m")
        if type == UnitKeys.DISTANCE.KILOMETER: return BaseUnit(0.001, 0, "km")

        if type == UnitKeys.TIME.SECOND: return BaseUnit(1, 0, "s")
        if type == UnitKeys.TIME.MINUTE: return BaseUnit(1.0/60, 0, "min")
        if type == UnitKeys.TIME.HOUR: return BaseUnit(1.0/3600, 0, "h")

        if type == UnitKeys.MASS.KILO: return BaseUnit(1, 0, "kg")

        if type == UnitKeys.CURRENT.AMPERE: return BaseUnit(1, 0, "A")

        if type == UnitKeys.AMOUNT.MOL: return BaseUnit(1, 0, "mol")

        if type == UnitKeys.TEMPERATURE.KELVIN: return BaseUnit(1, 0, "K")
        if type == UnitKeys.TEMPERATURE.CELSIUS: return BaseUnit(1, -273.15, "°C")

        if type == UnitKeys.INTENSITY.CANDELA: return BaseUnit(1, 0, "cd")

        if type == UnitKeys.VELOCITY.METER_PER_SECOND: return ComposedUnit(UnitFactory.generate(UnitKeys.DISTANCE.METER), UnitFactory.generate(UnitKeys.TIME.SECOND), Operator.DIVIDE)
        if type == UnitKeys.VELOCITY.KM_PER_HOUR: return ComposedUnit(UnitFactory.generate(UnitKeys.DISTANCE.KILOMETER), UnitFactory.generate(UnitKeys.TIME.HOUR), Operator.DIVIDE)


        assert 0, "Bad unit creation: " + type
    generate = staticmethod(generate)

