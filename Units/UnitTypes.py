#class Name:  UnitType, UnitKeys
#Author: djk

from enum import Enum

class UnitType(Enum):
    OTHER = 0
#base Units
    DISTANCE = 1
    TIME = 2
    MASS = 3
    CURRENT = 4
    AMOUNT = 5
    TEMPERATURE = 6
    INTENSITY = 7
#composed Units
    VELOCITY = 10
    ACCELERATION = 11

    DENSITY=20
    DAMPING=21

    FORCE = 30

    FORCE_OVER_DISTANCE=100


class UnitKeys():
    class DISTANCE(Enum):
        METER = 100
        KILOMETER = 101

    class TIME(Enum):
        SECOND = 200
        MINUTE = 201
        HOUR = 202

    class MASS(Enum):
        KILO = 300

    class CURRENT(Enum):
        AMPERE = 400

    class AMOUNT(Enum):
        MOL = 500

    class TEMPERATURE(Enum):
        CELSIUS = 600
        KELVIN = 601

    class INTENSITY(Enum):
        CANDELA = 700

    class VELOCITY(Enum):
        METER_PER_SECOND = 800
        KM_PER_HOUR = 801

    class DENSITY(Enum):
        KG_PER_SQUARE_METER = 900

    class DAMPING(Enum):
        KG_PER_SECOND = 1000

    class FORCE_OVER_DISTANCE(Enum):
        NETWON_PER_METER = 1100


