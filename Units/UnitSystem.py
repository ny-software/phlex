#class Name:  UnitSystem
#Author: djk

# abstract class which give the units of some physical properties

from Units.Unit import *
from Units.UnitTypes import *
from AdvancedMath.Operators import *

class UnitSystem:

    def __init__(self):
        self._distance = None
        self._time = None
        self._mass = None
        self._current = None
        self._temperature = None
        self._amount = None
        self._intensity = None

    def unit(self, type):
        if(type==UnitType.DISTANCE):
            return self.distance()
        if(type==UnitType.TIME):
            return self.time()
        if(type==UnitType.MASS):
            return self.mass()
        if(type==UnitType.CURRENT):
            return self.current()
        if(type==UnitType.AMOUNT):
            return self.amount()
        if(type==UnitType.TEMPERATURE):
            return self.temperature()
        if(type==UnitType.INTENSITY):
            return self.intensity()

        if(type==UnitType.VELOCITY):
            return self.velocity()
        if(type==UnitType.ACCELERATION):
            return self.acceleration()
        if(type==UnitType.FORCE):
            return self.force()
        if(type==UnitType.DENSITY):
            return self.density()
        if(type==UnitType.DAMPING):
            return self.damping()
        if(type==UnitType.FORCE_OVER_DISTANCE):
            return self.force_over_distance()

        return None


#basic units
    def distance(self) -> BaseUnit:
        return self._distance


    def time(self) -> BaseUnit:
        return self._time


    def mass(self) -> BaseUnit:
        return self._mass


    def current(self) -> BaseUnit:
        return self._current


    def temperature(self) -> BaseUnit:
        return self._temperature


    def amount(self) -> BaseUnit:
        return self._amount


    def intensity(self) -> BaseUnit:
        return self._intensity

#composed units
    def velocity(self, trivia="") -> ComposedUnit:
        return ComposedUnit(self.distance(), self.time(), Operator.DIVIDE, trivia)

    def acceleration(self, trivia="") -> ComposedUnit:
        return ComposedUnit(self.distance(), ComposedUnit(self.time(), self.time(), Operator.MULTIPLY), Operator.DIVIDE, trivia)

    def force(self, trivia="") -> ComposedUnit:
        return ComposedUnit(self.mass(), self.acceleration(), Operator.MULTIPLY, trivia)

    def density(self, trivia="") -> ComposedUnit:
        return ComposedUnit(self.mass(), UnitPower(self.distance(), 3), Operator.DIVIDE, trivia)

    def damping(self, trivia="") -> ComposedUnit:
        return ComposedUnit(self.mass(), UnitPower(self.time(), 3), Operator.DIVIDE, trivia)

    def force_over_distance(self, trivia="") -> ComposedUnit:
        return ComposedUnit(self.force(), self.distance(), Operator.DIVIDE, trivia)