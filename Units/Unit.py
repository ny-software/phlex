#classes:  Unit, BaseUnit, ComposedUnit,  UnitPower
#Author: djk


# The Unit class implements physical units
# They can be a fundamental unit (e.g. for distance, time, ...) or a composed unit (Energy, Power, speed, ...)
# Units are able to obtain the conversion factor between other units (if they are of the same type)
# This is done by defining the conversion factors to the SI unit system (this is the base system used in coding)



from AdvancedMath.Operators import *



class Unit:
    def __init__(self):
        pass

    def getFactorTo(self, unit):
        """
        returns the factor for conversion from this unit to the given one
        be carefull, both units have to habe of the same type to get reasonable results
        :param unit: the other unit
        :return: the factor for conversion from this to the other unit
        """
        return self.getFactorToSI()/unit.getFactorToSI()

class BaseUnit(Unit):
    """
    Baseic Unit used for Distance, Time, Mass, Current, Temperature, amount of substance,  	luminous intensity
    """
    def __init__(self, factor=1, shift=0, rep="", latexrep = ""):
        """
        :param factor: conversion factor to the SI unit system
        :param rep: string for printing the unit
        :param latexrep: latex code to represent the unit
        """
        super().__init__()
        self._factor = factor
        self._shift = shift
        self._string = rep
        if(latexrep==""):
            self._latexrep = rep
        else:
            self._latexrep = latexrep

    @property
    def factor(self):
        return self._factor

    @property
    def shift(self):
        return self._shift

    def convertToSI(self, val):
        """
        Used to convert e.g. celisus into kelvin, but is only needed for absolut values, not for unit factors!
        :param val:
        :return:
        """
        return self._factor*(val+self._shift)

    def getFactorToSI(self):
        return self._factor

    def __eq__(self, other):
        """
        the units are equal if factor and shift is equal
        :param other:
        :return:
        """
        if (not isinstance(other, Unit)):
            return False
        if(self._shift == other.shift and self._factor == other.factor):
            return True

    def toString(self, useTrivia=True):
        return self._string

    def toLatexString(self, useTrivia=True):
        return self._latexrep

class IdentityUnit(BaseUnit):
    def __init__(self, factor):
        super().__init__(factor, 0, rep="1")
        pass




#   def __init__(self, lhs, rhs, op:Operator):
#       if(isinstance(lhs, type(rhs))):
#           if(op==Operator.DIVIDE):
#               return IdentityUnit(lhs.getFactorToSI()/rhs.getFactorToSI())
#       return self.CompsedUnit(lhs, rhs, op)


class ComposedUnit(Unit):
    def __init__(self, lhs, rhs, op:Operator, trivia=""):
        """
        Compose a new unit
        :param lhs: left hand side
        :param rhs: right hand side
        :param op:  operator betwenn lhs and rhs, of type Operator
        """
        super().__init__()
        self._node1 = lhs
        self._node2 = rhs
        self._operator = op
        self._trivialName = trivia


    def getFactorToSI(self):
        factor1 = self._node1.getFactorToSI()
        factor2 = self._node2.getFactorToSI()
        if(self._operator==Operator.MULTIPLY):
            return factor1 * factor2
        if(self._operator==Operator.DIVIDE):
            return factor1 / factor2
        if(self._operator==Operator.ADD):
            return factor1 + factor2
        if(self._operator==Operator.SUBTRACT):
            return factor1 - factor2

    def toString(self, useTrivia=True):
        if(self._trivialName != "" and useTrivia):
            return self._trivialName

        #simplify expression for squares
        if(self._operator==Operator.MULTIPLY):
            if(isinstance(self._node1, BaseUnit) and isinstance(self._node2, BaseUnit)):
                if(self._node1.toString()==self._node2.toString()):
                    return self._node2.toString() + "^2"
        return self._node1.toString() + Operator.toString(self._operator) + self._node2.toString()


    def toLatexString(self, useTrivia=True):
        if(self._trivialName != "" and useTrivia):
            return self._trivialName
        if(self._operator==Operator.DIVIDE):
            return "\\frac{"+ self._node1.toLatexString() + "}{" + self._node2.toLatexString() + "}"
        return self._node1.toLatexString() + Operator.toString(self._operator) + self._node2.toLatexString()


class UnitPower(Unit):
    def __init__(self, unit, n):
        super().__init__()
        self._unit = unit
        self._n = n

    def getFactorToSI(self):
        factor = self._unit.getFactorToSI()
        return pow(factor, self._n)

    def toString(self, useTrivia=True):
        return self._unit.toString()+"^"+"{:1.0f}".format(self._n)

    def toLatexString(self, useTrivia=True):
        return self._unit.toString()+"^"+"{:1.0f}".format(self._n)