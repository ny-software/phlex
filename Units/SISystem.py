#class Name:  UnitSystem
#Author: djk

# abstract class which give the units of some physical properties

from Units.UnitSystem import *
from Units.Unit import *
from Units.UnitFactory import *


class SISystem(UnitSystem):

    def __init__(self):
        super().__init__()
        self._distance = UnitFactory.generate(UnitKeys.DISTANCE.METER)
        self._time = UnitFactory.generate(UnitKeys.TIME.SECOND)
        self._mass = UnitFactory.generate(UnitKeys.MASS.KILO)
        self._temperature = UnitFactory.generate(UnitKeys.TEMPERATURE.KELVIN)
        self._intensity = UnitFactory.generate(UnitKeys.INTENSITY.CANDELA)
        self._amount = UnitFactory.generate(UnitKeys.AMOUNT.MOL)
        self._current = UnitFactory.generate(UnitKeys.CURRENT.AMPERE)

    def force(self) -> ComposedUnit:
        return super().force("N")