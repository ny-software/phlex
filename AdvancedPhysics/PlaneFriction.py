#class Name:  PlaneFirction
#Author: djk

from AdvancedMath.Tools import *

#some values:
#Stahl auf Stahl 	0,2 	0,1
#Stahl auf Holz 	0,5 	0,4
#Stahl auf Stein 	0,8 	0,7
#Stein auf Holz 	0,9 	0,7
#Leder auf Metall 	0,6 	0,4
#Holz auf Holz 	0,5 	0,4
#Stein auf Stein 	1,0 	0,9
#Stahl auf Eis 	0,03 	0,01
#Stahl auf Beton 	0,35 	0,20

class FrictionCoefficients:
    def __init__(self, mu_static:float=0.95, mu_kinetic:float=0.9, mu_roll:float=1.0):
        self.mu_static = mu_static
        self.mu_kinetic = mu_kinetic
        self.mu_roll = mu_roll

    def getStaticCoefficient(self) -> float:
        return self.mu_static

    def getKineticCoefficient(self) -> float:
        return self.mu_kinetic

    def getRollCoefficient(self) -> float:
        return self.mu_roll

    def __mul__(self, other):
        """
        Overload of the multiplication operator for friction coefficients
        This is very usefull in calculating Frictions between two objects
        :param other:
        :return:
        """
        return FrictionCoefficients(self.mu_static*other.mu_static, self.mu_kinetic*other.mu_kinetic, self.mu_roll*other.mu_roll)

class PlaneFriction:
    """
    This abstract class handles all friction properties of an object
    ToDo: move most of the friction calculation into this class
    """
    def calculateStaticFriction(coeff:FrictionCoefficients, normalForce):
        return normal(coeff.getStaticCoefficient()*normalForce)

    def calculateKineticFriction(coeff:FrictionCoefficients, normalForce):
        return normal(coeff.getKineticCoefficient()*normalForce)


