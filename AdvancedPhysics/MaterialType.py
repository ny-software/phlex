#class Name:  MaterialType
#Author: djk

from AdvancedPhysics.PlaneFriction import *


class MaterialType:
    """
    This class holds all Physical properties of an material, like density and other mecanical and physical properties
    """

    def __init__(self):
        pass

        self.textureID = None

        self.frictionCoefficients = FrictionCoefficients()

    def hasTexture(self) -> bool:
        return self.textureID is not None


    def getTextureID(self):
        return self.textureID

    def getFrictionCoefficients(self) -> FrictionCoefficients:
        return self.frictionCoefficients


    def setTextureID(self, id):
        self.textureID = id

    def setFrictionCoefficients(self, friction:FrictionCoefficients):
        if friction is not None:
            self.frictionCoefficients = friction