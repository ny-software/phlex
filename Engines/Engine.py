from threading import  Thread, Semaphore
import time
import copy
from GlobalConstants import GlobalConstants
from Sandbox.BasicSandbox import BasicSandbox
import abc


class Engine(Thread):
    def __init__(self, identifier:str):
        super(Engine, self).__init__()

        self.identifier = identifier

        self.runSemaphore = Semaphore(1)

        self.simulationMode = GlobalConstants.SIMULATION_MODE_STOPPED

        self.sandbox = None
        self.savedSandbox = None
        self.globalTime = 0.0
        self.daemon = True
        self.runSemaphore.acquire()
        print("Simple Mechanics Engine Created")

    def getIdentifier(self) -> str:
        return self.identifier

    def start(self, sandbox:BasicSandbox) -> None:
        self.sandbox = sandbox
        self.savedSandbox = copy.deepcopy(sandbox)
        super(Engine,self).start()


    def getSimulationMode(self) -> int:
        return self.simulationMode

    def getSimulationTime(self) -> float:
        return self.globalTime

    def startSimulation(self) -> None:
        if self.simulationMode != GlobalConstants.SIMULATION_MODE_RUNNING:
            self.runSemaphore.release()
            #wait for thread to start up
            self.sandbox.initializeObjects()
            while self.simulationMode != GlobalConstants.SIMULATION_MODE_RUNNING:
                time.sleep(0.01)
            print("Simmulation Started")

    def isRunning(self) -> bool:
        return self.simulationMode == GlobalConstants.SIMULATION_MODE_RUNNING

    def pauseSimulation(self) -> None:
        if self.simulationMode == GlobalConstants.SIMULATION_MODE_RUNNING:
            self.runSemaphore.acquire()
            self.simulationMode = GlobalConstants.SIMULATION_MODE_PAUSED
            print("Simmulation Paused")

    def stopSimulation(self) -> None:
        if self.simulationMode == GlobalConstants.SIMULATION_MODE_RUNNING:
            self.pauseSimulation();
            self.simulationMode = GlobalConstants.SIMULATION_MODE_STOPPED
            print("Simmulation Stopped")

    def resetSimulation(self):
        self.stopSimulation()
        self.sandbox.reset(copy.deepcopy(self.savedSandbox))
        self.globalTime = 0.0
        print("Simmulation Reseted")

    def run(self):
        while True:
            self.runSemaphore.acquire()

            if self.simulationMode != GlobalConstants.SIMULATION_MODE_RUNNING:
                self.simulationMode = GlobalConstants.SIMULATION_MODE_RUNNING

            #try:
                # do simulation
            self.doSimulationStep()
            #except Exception as inst:
            #    print("An error occurred during the simulation step. ")
            #    print(type(inst))     # the exception instance
            #    print(inst.args)      # arguments stored in .args
            #    print(inst)

            self.runSemaphore.release()
            time.sleep(0.01) # slows down calculation and allows window updates

    @abc.abstractmethod
    def doSimulationStep(self):
        pass
