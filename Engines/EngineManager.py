from threading import Thread, Semaphore
from enum import Enum
from Engines.Engine import Engine
from Sandbox import BasicSandbox
from Objects import BasicObject


class LockT(Enum):
    RUN = 1
    QUEUE = 2
    MOD = 3
    SLEEP = 4


class EngineManager(Thread):
    def __init__(self, sandbox:BasicSandbox) :
        super(EngineManager, self).__init__()
        self.engineDict = {}
        self.sandbox = sandbox
        self.objDeletionQueue = []

        self.locks = []
        #ToDo: FIX THIS!
#        for stitem in LockT.__members__.items():
#            self.locks[stitem.index()] = Semaphore(1)

        self.locks.append(0)
        self.locks.append(Semaphore(1)) #RUN
        self.locks.append(Semaphore(1)) #QUEUE
        self.locks.append(Semaphore(1)) #MOD
        self.locks.append(Semaphore(1)) #SLEEP


        self.daemon = True
#        self.locks[LockT.RUN].aquire()
#       self.locks[LockT.RUN.value].aquire()
        self.start()

    def attachEngine(self, engine:Engine) -> None:
        if engine.getIdentifier() not in self.engineDict.keys():
            self.locks[LockT.MOD.value].acquire()
            self.engineDict[engine.getIdentifier()] = engine
            engine.start(self.sandbox)
            self.locks[LockT.MOD.value].release()

    def pauseEngine(self, engineId:str) -> None:
        self.locks[LockT.MOD.value].acquire()
        if engineId in self.engineDict.keys():
            self.engineDict[engineId].pauseSimulation()
            self.locks[LockT.MOD.value].release()
        else:
            self.locks[LockT.MOD.value].release()
            raise Exception("Cannot pause, Engine does not exist")

    def pauseAllEngines(self) -> None:
        self.locks[LockT.MOD.value].acquire()
        for engine in self.engineDict:
            engine.pauseSimulation()
        self.locks[LockT.MOD.value].release()

    def stopEngine(self, engineId:str) -> None:
        self.locks[LockT.MOD.value].acquire()
        if engineId in self.engineDict.keys():
            self.engineDict[engineId].stopSimulation()
            self.locks[LockT.MOD.value].release()
        else:
            self.locks[LockT.MOD.value].release()
            raise Exception("Cannot stop, Engine does not exist")

    def startEngine(self, engineId:str) -> None:
        self.locks[LockT.MOD.value].acquire()
        if engineId in self.engineDict.keys():
            self.engineDict[engineId].startSimulation()
            self.locks[LockT.MOD.value].release()
        else:
            self.locks[LockT.MOD.value].release()
            raise Exception("Cannot start engine, engine does not exist")

    def startAllEngines(self) -> None:
        self.locks[LockT.MOD.value].acquire()
        for engine in self.engineDict:
            engine.startSimulation()
        self.locks[LockT.MOD.value].release()

    def queueObjDeletion(self, obj:BasicObject) -> None:
        if obj not in self.objDeletionQueue:
            self.locks[LockT.QUEUE.value].acquire()
            self.objDeletionQueue.append(obj)

            self.locks[LockT.RUN.value].release()

            self.locks[LockT.QUEUE.value].release()

    def run(self):
        while True:
            self.locks[LockT.RUN.value].acquire()
            if 0 < len(self.objDeletionQueue):
                self.locks[LockT.QUEUE.value].acquire()
                #require all engines to stop
                self.pauseAllEngines()

                self.locks[LockT.MOD.value].acquire()
                # process modification queue
                self.sandbox.deleteObjects(self.objDeletionQueue)
                self.locks[LockT.MOD.value].release()

                #resume all processors
                self.startAllEngines()

                self.objDeletionQueue.clear()
                self.locks[LockT.QUEUE.value].release()






