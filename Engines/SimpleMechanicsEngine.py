# class Name:  SimpleMechanicSEngine
# Author: djk


from GlobalConstants import *
from Sandbox.SimpleMechanicSandbox import *
from Objects.Interactions.BasicInteraction import *
from Objects.Floor import *
from Engines.Engine import *
import time
import copy


class SimpleMechanicsEngine(Engine):
    # SimpleMechanicEngine

    def __init__(self):
        super(SimpleMechanicsEngine, self).__init__("SimpleMechanicsEngine")

        self.timeSpeed = GlobalConstants.TIME_SPEED
        self.lastTimeStamp = time.time() * self.timeSpeed

        #in case of a slow mashien, this value is used as deltaT
        self.maxDeltaT = 0.02

        print("SimpleMechanicsEngine Created")

    def updatePositions(self, obj, deltaT):
        if (isinstance(obj, MassPoint)):
            try:
                if (obj.isMoveable()):
                    obj.setCenterOfMass(obj.getCenterOfMass() + obj.getCenterOfMassVelocity() * deltaT)
            except:
                print("Error in run of Engine with Masspoint")

    def updateVelocities(self, obj, deltaT):
        if (isinstance(obj, MassPoint)):
            try:
                if (obj.isMoveable()):
                    obj.setCenterOfMassVelocity(
                        obj.getCenterOfMassVelocity() + obj.getCenterOfMassForce() * deltaT / obj.getMass())
            except:
                print("Error in run of Engine with Masspoint")

    def calculateGlobalForcesOnObject(self, obj):
        # calculate forcesfields
        """
        calculates the forces on the object based on all global forcefields
        :param obj: the object on which the forces will act
        """
        for forcefield in self.sandbox.getForceFieldList().values():
            if (isinstance(forcefield, BasicForceField)):
                forcefield.updateForceOnObject(obj)

    def calculateParticleCollisions(self, obj):
        # particle pair interaction
        """
        calculates the collision of the object with the oder objects in the sandbox, this method loop over all other objects
        :param obj: first object of the collision
        """
        if isinstance(obj, CircleObject):
            for objTwo in self.sandbox.getObjectList().values():
                if (isinstance(objTwo, CircleObject)):
                    try:
                        obj.collision(objTwo)
                    except:
                        print("Error in particle colision")

    def calculateFixedPositionCollisions(self, obj):
        """collisions with infintly heavy objects, like walls
        calculatesthe collisions with infinit force potential particles like walls or hard bars. this have to be done at last step in the force calculateion
        :param obj: the object which may will collide
        """
        for objIn in self.sandbox.objectList.values():
            # search for collisions with floors
            if (isinstance(objIn, Floor)):
                #try:
                    objIn.collision(obj)
                #except:
                #    print("Error in run of Engine with Floor")

    def calculateInteractions(self):
        """
        soft particle interactions like harmonic springs will be calculated here
        """
        for interaction in self.sandbox.getInteractionForceList():
            interaction.updateInteractionForces()

    def doSimulationStep(self):
        actualTime = time.time() * self.timeSpeed
        # if some problems occur, use the minimum default deltaT value, this leads to some leaking, but the simulations stays still quite accurate
        deltaT = min(actualTime - self.lastTimeStamp, self.maxDeltaT)
        self.globalTime += deltaT

        self.lastTimeStamp = actualTime



        # ---- calculation with maps ----------#
        # ---- remove that...does not work well --- #
        #object = self.sandbox.getObjectList().values()

        #masspoints = filter(lambda obj: isinstance(obj, MassPoint), object)

        #map(lambda obj: self.updateVelocities(obj, deltaT / 2.0), object)
        #map(lambda obj: self.updatePositions(obj, deltaT / 2.0), object)
        #map(lambda obj: self.updatePositions(obj, deltaT / 2.0), object)
        #map(lambda obj: obj.resetCenterOfMassForce(), masspoints)


        # ---- calculation with iteration -----#
        # do the first steps in the leap frog algorithm
        for obj in self.sandbox.getObjectList().values():
             # leap frog algorithm
            self.updateVelocities(obj, deltaT / 2.0)
            self.updatePositions(obj, deltaT / 2.0)
            self.updatePositions(obj, deltaT / 2.0)
             # resetForces
            if isinstance(obj, MassPoint):
                obj.resetCenterOfMassForce()

        # calculate interaction forces
        self.calculateInteractions()

        for obj in self.sandbox.getObjectList().values():
            # calculate forces
            self.calculateGlobalForcesOnObject(obj)
            self.calculateParticleCollisions(obj)
            self.calculateFixedPositionCollisions(obj)
            # set background force to real value
            if (isinstance(obj, MassPoint)):
                obj.updateCenterOfMassForce()
            # do final velocity step
            self.updateVelocities(obj, deltaT / 2.0)

            if (isinstance(obj, MassPoint)):
                obj.time += deltaT
            pass
