from Objects.BasicObject import *


class ObjectAnchor(BasicObject):

    objectAnchorCounter=0


    def __init__(self, parent:BasicObject, id=None, relativeCoordinates=[0.0,0.0]):

        ObjectAnchor.objectAnchorCounter+=1
        if(id==None):
            id="object_anchor_"+str(ObjectAnchor.objectAnchorCounter)

        super(ObjectAnchor, self).__init__(id)

        self.hide()

        self.parentObject=parent
        self.relativeCoordinates = relativeCoordinates

    def setRelatvieCoordinates(self, cor:list):
        self.relativeCoordinates = cor

    def getRelativeCoordinates(self):
        return self.relativeCoordinates
