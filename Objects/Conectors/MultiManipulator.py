#class Name:    BasicWatcher
#Author: djk

from Objects.BasicObject import *
from Objects.EventSubject import *

class MultiManipulator(BasicObject):
    """
    Basic Class to watch an physical Object
    Watcher does not have a strict visualisation, they only contain the collected data
    The visualisation is then created by the view!
    """

    def __init__(self, id:str):
        """ constructor         """
        super( MultiManipulator, self ).__init__(id)

        self.slave = None
        #list of data, each line represents one measurement enforced by notifyEvent
        #self.listOfData = []
        #list of all keys which denotes the observables of the observed object
        self.manipulationKey = None


        self.slaves = []

        self.value = None
        print("BasicWatcher created")

    def addObject(self, slave):
        self.slaves.append(slave)

    def getObservableType(self):
        try:
            return self.slaves[0].getObservableType(self.manipulationKey)
        except:
            return UnitType.OTHER

    def getValueFromObject(self):
        self.value = self.slaves[0].getObservable(self.manipulationKey)
        return self.value

    def increment(self, delta):
        for slave in self.slaves:
            slave.setObservable(self.manipulationKey, slave.getObservable(self.manipulationKey) + delta )
        self.getValueFromObject()


    def decrement(self, delta):
        for slave in self.slaves:
            slave.setObservable(self.manipulationKey, slave.getObservable(self.manipulationKey) - delta )
        self.getValueFromObject()

    def setValue(self, val):
        for slave in self.slaves:
            slave.setObservable(self.manipulationKey, val)
        self.getValueFromObject()

    def setManipulationKey(self, key:str):
        self.manipulationKey = key



    def getDescription(self) -> str:
        """ @brief  intern description for debugging  """
        return "[" + self.getObjectID()+"]: This is a basic Manipulator!"

    def __str__(self):
        return self.getDescription()

