
from Objects.Conectors.BasicAnchor import *


class MechanicAnchor(BasicAnchor):
    """
    A special type of anchors, which are able to change the possition of the attached anchors. As well, it is possible
    to manipulate forces on objects which are attached through their MechanicAnchors. Moreover, mechanic values can be
    calculated from all attached anchors.
    With his anchor, it is also posible to manipulate the owner of the anchor.
    """

    def __init__(self, owner:BasicObject):

        BasicAnchor.__init__(self, owner)

    def move(self, dx, dy) -> None:
        """
        Move the center of mass possiton of the owner of this anchor by an ammount [dx,dy]
        :param dx: displacement in x direction
        :param dy: displacement in y direction
        """
        print("anchor move: ", dx, dy)
        self.getOwnerObject().move(dx,dy)

    def getCenterOfMass(self) -> np.array:
        #ToDo: only one call is needed here
        """
        Returns the center of mass of the owners of the attached anchors
        :return: np.array([x,y]): center of mass possition of the owner(s)
        """
        for anchor in self.getAttachedAnchors():
            return anchor.getOwnerObject().getCenterOfMass()
        return np.array([0,0])

    def getCenterOfMassVelocity(self) -> np.array:
        """
        Returns the center of mass velocity of the owners of the attached anchors
        :return: np.array([x,y]): center of mass velocity of the owner(s)
        """
        #ToDo: only one call is needed here
        for anchor in self.getAttachedAnchors():
            return anchor.getOwnerObject().getCenterOfMassVelocity()

    def addCenterOfMassForce(self, force: np.array) -> None:
        """
        Send an additonal force vector to all objects attached through their MechanicsAnchors
        :param force: np.array([fx, fy]): force vector which will be send to the objects
        """
        for anchor in self.getAttachedAnchors():
            anchor.getOwnerObject().addCenterOfMassForce(force)
        pass





