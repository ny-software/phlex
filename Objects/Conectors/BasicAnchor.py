from Objects.BasicObject import *


class BasicAnchor(BasicObject):
    """
    Anchors are used to attache objects to each other, this anchors should make the GUI much easier to use.
    If an object which can be attached to an other is created, the anchors of this object and the other can be connected
    """
    anchorCounter=0

    def __init__(self, owner:BasicObject, id=None):
        BasicAnchor.anchorCounter+=1
        if(id==None):
            id="anchor_"+str(BasicAnchor.anchorCounter)

        BasicObject.__init__(self, id)

        #maximal numbers of anchors which can be attached, -1 means infinity
        self.maxNumberOfAnchors = -1

        #the owner of the anchor
        self.ownerObject = owner

        #the relative possition of the anchor based on the object coordinates of the owner
        self.relativePos = np.array([0,0])

        #Normaly, the anchors are not visible
        self.visible = True

        #dictionary for the attached anchors
        self.attachedAnchors = {}

    def setOwnerCoordinates(self, coord) -> None:
        """
        Move the center of mass possiton of the owner of this anchor to coord
        :param coord: new possition as np.array([x,y])
        """
        self.getOwnerObject().setCoordinates(coord)

    def getOwnerObject(self) -> BasicObject:
        """
        returns a reference the owner of this anchor
        :return: a reference of the owner of this anchor (BasicObject)
        """
        return self.ownerObject

    def setOwnerObject(self, obj:BasicObject) -> None:
        """
        Sets the new owner of the Anchor
        :param obj: new owner of the anchor
        """
        self.ownerObject = obj

    def getCoordinates(self) -> np.array:
        """
        calculates the "real" coordinates of this anchor based on the relative coordiantes plus the coordinates of the owner
        note: this must not be the coordinates of the anchor which is connected to this one!
        :return: np.array([x,y]): relative + absulute coordinates depending on the owner
        """
        return self.relativePos + self.getOwnerObject().coordinates

    def getRelativeCoordinates(self) -> np.array:
        """
        returns only the relative cooridnates based on the position of the owner object
        :return: np.array([x,y]): relative coordinates
        """
        return self.relativePos

    def setCoordinates(self, coord:list) -> None:
        """
        sets the relative coordinates of the anchor, where coord are real coordinates
        :param coord: real coordinates, this method calculates the relative once
        """
        self.relativePos=np.array(coord)-self.getOwnerObject().getCoordinates()

    def setRelativeCoordinates(self, relcoord:list) -> None:
        """
        sets the relative coordinates of the anchor
        :param coord: real coordinates, this method calculates the relative once
        """
        self.relativePos=np.array(relcoord)

    def getAttachedAnchors(self) -> list:
        """
        returns a list of all anchors which are attached to this one
        :return: list of all attached anchors
        """
        return self.attachedAnchors.values()

    def hasAttachedAnchor(self) -> bool:
        """
        If any anchor is attached to this one, the result will be true, false otherwise
        :return: if any anchor is attached: true, false otherwise
        """
        if(len(self.getAttachedAnchors())==0):
            return False
        else:
            return True

    def dettachAnchor(self, anchor)->bool:
        """
        removes an anchor from the dictionarry of attached anchors
        it allso removes this anchor from the oponnent anchor list
        :param anchor: anchor which will be removed
        :return: succsess?
        """
        result = self.attachedAnchors.pop(anchor.getObjectID())
        if(result is not None):
            return anchor.dettachAnchor(self)
        return False

    def attachAnchor(self, anchor) -> bool:
        """
        This method attaches a new anchor to this one. If the anchor is already attached, then nothing will happen.
        If the maximum number of anchors is reached, then also nothing happens. in both cases, false is returned as errorcode.
        :param anchor: the new anchor (type: BasicAnchor)
        :return: succseed? if the anchor was attached successfully: true, otherwise: false
        """
        if(not isinstance(anchor, BasicAnchor)):
           return False
        #do nothing, if anchor is allready in the list
        if(self.attachedAnchors.get(anchor.getObjectID()) is not None):
            return False
        if(self.maxNumberOfAnchors==-1 or self.maxNumberOfAnchors>len(self.attachedAnchors)):
            if(anchor is not None):
                self.attachedAnchors.update({anchor.getObjectID() : anchor})
                anchor.attachAnchor(self)
                return True
        return False


    def getDescription(self) -> str:
        ''' intern description for debugging '''
        return "This is a basic Anchor!"