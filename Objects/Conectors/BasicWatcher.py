# class Name:    BasicWatcher
# Author: djk

from Objects.BasicObject import *
from Objects.EventSubject import *


class BasicWatcher(BasicObject):
    """
    Basic Class to watch an physical Object
    Watcher does not have a strict visualisation, they only contain the collected data
    The visualisation is then created by the view!
    """

    def __init__(self, observed: BasicObject, id: str):
        """ constructor         """
        super(BasicWatcher, self).__init__(id)

        self.observed = None
        # list of data, each line represents one measurement enforced by notifyEvent
        # self.listOfData = []
        # list of all keys which denotes the observables of the observed object
        self.observableKeys = []

        # dictonary of observed data
        self.variableList = {}

        self.eventCounter = 0
        #how many events will be neglected, here we only take every 50th
        self.eventLoss = 50

        self.observed = observed
        self.observed.registerObserver(self)
        print("BasicWatcher created")

    def addObservableKey(self, key: str):
        print("add key: " + str(self) + "  " + key)
        self.observableKeys.append(key)
        print("new list: " + str(self.observableKeys))
        self.variableList.update({key: []})

    def reset(self):
        self.variableList.clear()
        self.eventCounter = 0

    def notifyEvent(self, subject: BasicObject):
        # if subject is self.observed:
        self.eventCounter += 1
        if (self.eventCounter > self.eventLoss):
            # print("call in "+str(self.getObjectID()))
            # tmpList = [self.observed.getObservable(self.observableKeys[0])]
            # tmpList = map(lambda x: self.observed.getObservable(self, x), ["TIME"])
            for x in self.observableKeys:
                self.variableList[x].append(self.observed.getObservable(x))
            # tmpList = [self.observed.getObservable(x) for x in self.observableKeys]
            # self.listOfData.append(tmpList)
            # print(self.observableKeys)
            # print(self.tmpList)
            # print("Call from "+ str(subject) + "in " + str(self))
            self.eventCounter = 0

    def getObservedList(self, key: str):
        return self.variableList[key]

    def getObservableType(self, key:str):
        try:
            return self.observed.getObservableType(key)
        except:
            return UnitType.OTHER

    def getLastObservalbel(self, key: str):
        try:
            return self.variableList[key][-1]
        except:
            return 0.0

    def getDescription(self) -> str:
        """ @brief  intern description for debugging  """
        return "[" + self.getObjectID() + "]: This is a basic Watcher!"

    def __str__(self):
        return self.getDescription()
