#class Name:  BasicObject
#Author: djk

from Objects.BasicObject import *
from Objects.MassPoint import *
from Objects.CircleObject import *
from GlobalConstants import *
import numpy as np

class Floor(BasicObject):
    """ Prototpye of an Object for a Sanbox Unit
    """

    def __init__(self, id:str):
        """ constructor         """
        super( Floor, self ).__init__(id)


        #physical properties:
        #some point in the plain
        self.pointOnPlain = np.array([0.,0.])

        #direction vector
        self.directionVector = np.array([0.,0.])
        self.parallelVector = normal(self.directionVector)

        self.parameterD = 0

        self.bouncyness = 2

        #dictionarry which contains all atributes for this object
        self.floorDictionary = {}

        self.observableDictionary = self.observableDictionary.update(self.floorDictionary)

        print("MassObject created")

    def update(self):
        self.parameterD = -sum(self.pointOnPlain*self.directionVector)

    def getObservable(self, key:str):
            return 0

    def setBouncyness(self, bn):
        if(bn >= 0.0):
            self.bouncyness = bn

    def setPointOnPlain(self, pop:list):
        """
        :param com: new point on plane
        :return: none
        """
        self.pointOnPlain = np.array(pop)
        self.update()

    def setDirectionVector(self, dV:list):
        """
        :param com: new point on plane
        :return: none
        """
        norm = np.array(dV) / np.linalg.norm(dV)
        self.directionVector = norm
        self.parallelVector = normal(self.directionVector)
        self.update()

    def getDirectionVector(self):
        return self.directionVector

    def getPlaneParameter(self):
        return self.parameterD

    def getBouncyness(self):
        return self.bouncyness

    def getSpecialPointPlain(self):
        return self.pointOnPlain

    def move(self, dx, dy):
        self.pointOnPlain[0] += dx
        self.pointOnPlain[1] += dy
        self.update()

    def collision(self, obj:BasicObject) -> None:
        if(type(obj) is MassPoint):
            projectionPostition = sum(obj.getCenterOfMass()*self.directionVector)
            projectionVelocity = sum(obj.getCenterOfMassVelocity()*self.directionVector)
            #check if behind plane and velocity still towards the plane
            if(projectionPostition < -self.parameterD and projectionVelocity < 0):
                obj.flipVelocityAlongPlane(self.directionVector,self.bouncyness)
        if(type(obj) is CircleObject):
            projectionPostition = scalar(obj.getCenterOfMass(), self.directionVector)
            if(projectionPostition-obj.radius > -self.parameterD+GlobalConstants.RESTING_DISTANCE):
                return
            #check if behind plane and velocity still towards the plane
            projectionVelocity = scalar(obj.getCenterOfMassVelocity(), self.directionVector)
            if(projectionVelocity <= 0.0):
                #do soft edges
                if(-projectionVelocity < GlobalConstants.RESTING_VELOCITY):
                    #the object now "sticks" on the plane
                    obj.setCenterOfMassVelocity(self.parallelVector*scalar(self.parallelVector,obj.getCenterOfMassVelocity()))
                else:
                    obj.flipVelocityAlongPlane(self.directionVector,self.bouncyness)
                #move center of mass for resting
                obj.setCenterOfMass(obj.getCenterOfMass()-self.directionVector*(projectionPostition+self.parameterD-obj.radius))
                #calculate normal force and subtract them from the particle
                normalForce = self.directionVector*scalar(self.directionVector, obj.getCenterOfMassForceBackground())
                obj.addCenterOfMassForce(-normalForce)
                #force is directed in plane direction? otherwise, don't be sticky!
                if(scalar(self.directionVector,normalForce)>=0.0):
                    return
                #is there friction?
                if(obj.hasMaterial and self.hasMaterial):
                    #ToDo: this have to be much more efficient!!!!
                    coeff = obj.material.getFrictionCoefficients() * self.material.getFrictionCoefficients()
                    parallelVelocity = scalar(obj.getCenterOfMassVelocity(), self.parallelVector)*self.parallelVector
                    normParalleVelocity = norm(parallelVelocity)
                    if(normParalleVelocity <= GlobalConstants.RESTING_VELOCITY):
                        frictionForce = PlaneFriction.calculateStaticFriction(coeff, normalForce)
                        #friction can not be stronger than parallel force, if it is -> total resting!
                        if(norm(frictionForce) > fabs(scalar(obj.getCenterOfMassForceBackground(), self.parallelVector))):
                            obj.setCenterOfMassForce(np.array([0.0,0.0]))
                            obj.setCenterOfMassVelocity(np.array([0.0,0.0]))
                        return
                    #if we get up to here, the static friction is not strong enought
                    frictionForce = PlaneFriction.calculateKineticFriction(coeff, normalForce)
                    obj.addCenterOfMassForce(--self.parallelVector*scalar(frictionForce, parallelVelocity)/normParalleVelocity)



    def getDescription(self) -> str:
        ''' intern description for debugging '''
        return "This is the " + self.objectId +  " Floor Object"

    def __str__(self):
        return self.getDescription()

