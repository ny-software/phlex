#class Name:    EventSubject
#Author: djk

from Objects.EventObserver import *

class EventSubject(EventSubjectPrototype):
    """
    Subject which is observed by an EventObserver
    This class contains all methods needed for this pattern
    """

    def __init__(self):
        """ constructor         """
        super( EventSubject, self ).__init__()
        self.listOfRegisteredObservers = []

    def notifyObservers(self):
        """ notifies all registered observers that something has changed
        :return: none
        """
        for ob in self.listOfRegisteredObservers:
            ob.notifyEvent(self)

    def registerObserver(self, observer:EventObserver):
        """ register a new observer in the observer list
        :param observer: observer object which will be added
        :return: none
        """
        self.listOfRegisteredObservers.append(observer)
        print("Observer <"+str(observer) +"> registered at <"+ str(self)+">")

    def updateObserverList(self, listOfObjects):
        newlistOfRegisteredObservers = []
        for obj in listOfObjects.values():
            for observer in self.listOfRegisteredObservers :
                print(obj, observer)
                if obj.getObjectID() == observer.getObjectID():
                    print("object updated: ", obj.getObjectID())
                    newlistOfRegisteredObservers.append(obj)
        self.listOfRegisteredObservers.extend(newlistOfRegisteredObservers)


    def unregisterObserver(self, observer:EventObserver):
        """ unregister a observer in the observer list
        :param observer: observer object which will be removed
        :return: none
        """
        try:
            self.listOfRegisteredObservers.remove(observer)
        except:
            print("Exception caught in"+str(self))
