from Engines.SimpleMechanicsEngine import *
from PySideGui.GuiTools.ValueIndicator import *
from Objects.ComplexObjects.Mechanics import *
from Objects.Conectors.BasicManipulator import *
from Objects.Conectors.MultiManipulator import *
from Objects.Interactions.HarmonicSpring import *
from Objects.forceFields.AmbiousAir import *
from Objects.forceFields.CentralGravity import *
from Objects.forceFields.HomogenousGravity import *
from PySideGui.BackgroundDecorators.ImageBackgroundDecorator import *
from PySideGui.BackgroundDecorators.Clouds import *
from PySideGui.GuiTools.ValueIndicator import *
from PySideGui.GuiTools.SimulationControl import *
from PySideGui.GuiTools.Label import *
from PySideGui.GuiTools.SimpleInlinePlots import *
from PySideGui.GuiTools.SimpleSlider import *
from PySideGui.GuiTools.SimpleTracer import *



########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################

def createTestSetupXmas(mainSandbox):


    gravity = HomogenousGravity(5*2, [0,-1],"ff02",)
    mainSandbox.addNewForceField(gravity)


    createXmasTree(mainSandbox)



    ground = Floor("Ground01")
    ground.setDirectionVector([0.0,1.0])
    ground.setPointOnPlain([0.0,-300.0])
    ground.setBouncyness(0.5)
    #ground.setTextureId(GlobalConstants.TEXTURE_ID_TEX_LEASH)
    ground.setTextureId(GlobalConstants.TEXTURE_ID_MAT_ROCK)
    mainSandbox.addNewObject(ground)




def createTestSetupXmas_GUI(engingemanager, guiSettings, sandbox):
    plotableObjectList = []
        #set view
    windowProperties = guiSettings.getWindowProperties()
    windowProperties.scale=[0.3, -0.3]
    windowProperties.offset = [400, 600]

    #guiSettings.setBackgroundDecorator(BackgroundDecoratorClouds(windowProperties, N=15))

    plotableObjectList.append(Label("Frohe Weihnachten", guiSettings, [200, 100]))

    return plotableObjectList

########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################

def createTestSetupLinearCollision(mainSandbox):

    p01 = CircleObject(300)
    p01.setCenterOfMass([-500,0])
    p01.setMass(50)
    p01.centerOfMassInitialVelocity = np.array([50.0, 0.0])
    p01.setTextureId(GlobalConstants.TEXTURE_ID_BILLARD_08_BALL)
    p01.showCenterOfMassVelocityVector()
    mainSandbox.addNewObject(p01)

    p02 = CircleObject(300)
    p02.setCenterOfMass([1200,0])
    p02.setMass(50)
    p02.centerOfMassInitialVelocity = np.array([-50.0, 0.0])
    p02.setTextureId(GlobalConstants.TEXTURE_ID_BILLARD_14_BALL)
    p02.showCenterOfMassVelocityVector()
    mainSandbox.addNewObject(p02)

    initialVelManipulator01 = BasicManipulator(p01, "manipulator01")
    initialVelManipulator01.setManipulationKey("COM_X_INITIAL_VEL")

    initialVelManipulator02 = BasicManipulator(p02, "manipulator02")
    initialVelManipulator02.setManipulationKey("COM_X_INITIAL_VEL")

    massManipulator01 = BasicManipulator(p01, "manipulator03")
    massManipulator01.setManipulationKey("MASS")
    mainSandbox.addNewModifier(massManipulator01)

    massManipulator02 = BasicManipulator(p02, "manipulator04")
    massManipulator02.setManipulationKey("MASS")
    mainSandbox.addNewModifier(massManipulator02)

    mainSandbox.addNewModifier(initialVelManipulator01)
    mainSandbox.addNewModifier(initialVelManipulator02)



    massPointWatcher01 = BasicWatcher(p01, "watcher01")
    massPointWatcher01.addObservableKey("COM_XVEL")
    mainSandbox.addNewWatcher(massPointWatcher01)

    massPointWatcher02 = BasicWatcher(p02, "watcher02")
    massPointWatcher02.addObservableKey("COM_XVEL")
    mainSandbox.addNewWatcher(massPointWatcher02)

    ground = Floor("Ground01")
    ground.setDirectionVector([0.0,1.0])
    ground.setPointOnPlain([0.0,-300.0])
    ground.setBouncyness(0.5)
    #ground.setTextureId(GlobalConstants.TEXTURE_ID_TEX_LEASH)
    ground.setTextureId(GlobalConstants.TEXTURE_ID_MAT_ROCK)
    mainSandbox.addNewObject(ground)




def createTestSetupLinearCollision_GUI(engingemanager, guiSettings, sandbox):
    plotableObjectList = []
        #set view
    windowProperties = guiSettings.getWindowProperties()
    windowProperties.scale=[0.3, -0.3]
    windowProperties.offset = [400, 600]

    guiSettings.setBackgroundDecorator(BackgroundDecoratorClouds(windowProperties, N=15))

    slider01 = SimpleSlider(sandbox.getModifierByName("manipulator01"), guiSettings, [100, 200])
    slider01.addChild(Label("Startgeschwindigkeit 1", guiSettings, [30, -30]))
    slider01.setBounds(-100, 100)
    plotableObjectList.append(slider01)
    slider02 = SimpleSlider(sandbox.getModifierByName("manipulator02"), guiSettings, [600, 200])
    slider02.addChild(Label("Startgeschwindigkeit 2", guiSettings, [30, -30]))
    slider02.setBounds(-100, 100)
    plotableObjectList.append(slider02)

    slider03 = SimpleSlider(sandbox.getModifierByName("manipulator03"), guiSettings, [100, 300])
    slider03.addChild(Label("Masse 1", guiSettings, [100, -30]))
    slider03.setBounds(0, 100)
    plotableObjectList.append(slider03)
    slider04 = SimpleSlider(sandbox.getModifierByName("manipulator04"), guiSettings, [600, 300])
    slider04.addChild(Label("Masse 2", guiSettings, [100, -30]))
    slider04.setBounds(0, 100)
    plotableObjectList.append(slider04)

    valueIndicator1 = ValueIndicator(sandbox.getWatcherByName("watcher01"), guiSettings, [200, 420])
    valueIndicator1.setObservableKey("COM_XVEL")
    valueIndicator1.setWidth(200)
    plotableObjectList.append(valueIndicator1)

    valueIndicator1B = ValueIndicator(sandbox.getWatcherByName("watcher01"), guiSettings, [200, 350])
    valueIndicator1B.setObservableKey("COM_XVEL")
    valueIndicator1B.setWidth(200)
    valueIndicator1B.setUnit(UnitFactory.generate(UnitKeys.VELOCITY.KM_PER_HOUR))
    plotableObjectList.append(valueIndicator1B)

    valueIndicator2 = ValueIndicator(sandbox.getWatcherByName("watcher02"), guiSettings, [700, 420])
    valueIndicator2.setObservableKey("COM_XVEL")
    valueIndicator2.setWidth(200)
    plotableObjectList.append(valueIndicator2)

    valueIndicator2B = ValueIndicator(sandbox.getWatcherByName("watcher02"), guiSettings, [700, 350])
    valueIndicator2B.setObservableKey("COM_XVEL")
    valueIndicator2B.setWidth(200)
    valueIndicator2B.setUnit(UnitFactory.generate(UnitKeys.VELOCITY.KM_PER_HOUR))
    plotableObjectList.append(valueIndicator2B)



    return plotableObjectList


########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################

def createTestSetupEmptyBox(mainSandbox):
    ground = Floor("Ground01")
    ground.setDirectionVector([0.0,1.0])
    ground.setPointOnPlain([0.0,-1000.0])
    ground.setBouncyness(0.5)
    #ground.setTextureId(GlobalConstants.TEXTURE_ID_TEX_LEASH)
    ground.setTextureId(GlobalConstants.TEXTURE_ID_MAT_ROCK)
    mainSandbox.addNewObject(ground)

def createTestSetupEmptyBox_GUI(engingemanager, guiSettings, sandbox):
    plotableObjectList = []
        #set view
    windowProperties = guiSettings.getWindowProperties()
    windowProperties.scale=[0.3, -0.3]
    windowProperties.offset = [850, 400]

    return plotableObjectList

########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################

def createTestSetupMechanicsSpace01(mainSandbox):
    """
    This sets a simple friction, rolling test
    :param mainSandbox:
    """

    ball = CircleObject(200)
    ball.setMass(400)
    ball.setCenterOfMass([0, 3000+800])
    ball.centerOfMassInitialVelocity = np.array([0.0, 0.0])
    ball.setTextureId(GlobalConstants.TEXTURE_ID_SPUTNIK)
    mainSandbox.addNewObject(ball)

    massPointWatcher = BasicWatcher(ball, "watcher01")
    massPointWatcher.addObservableKey("COM_XCOORD")
    massPointWatcher.addObservableKey("COM_YCOORD")
    mainSandbox.addNewWatcher(massPointWatcher)

    initialVelManipulator01 = BasicManipulator(ball, "manipulator01")
    initialVelManipulator01.setManipulationKey("COM_X_INITIAL_VEL")
    mainSandbox.addNewModifier(initialVelManipulator01)

    p01 = CircleObject(2500)
    p01.setMoveable(False)
    p01.setCenterOfMass([0,0])
    p01.centerOfMassInitialVelocity = np.array([0.0, 0.0])
    p01.setTextureId(GlobalConstants.TEXTURE_ID_EARTH)
    mainSandbox.addNewObject(p01)

    gravity = CentralGravity(600000, [0,0],"ff0",)


    mainSandbox.addNewForceField(gravity)


def createTestSetupMechanicsSpace01_GUI(engingemanager, guiSettings, sandbox):
    plotableObjectList = []
        #set view
    windowProperties = guiSettings.getWindowProperties()
    windowProperties.scale=[0.05, -0.05]
    windowProperties.offset = [500, 500]

    guiSettings.setBackgroundDecorator(ImageBackgroundDecorator(windowProperties, guiSettings, GlobalConstants.IMAGE_ID_BACKGROUND_SPACE_01))

    tracer = SimpleTracer(sandbox.getWatcherByName("watcher01"), guiSettings)
    plotableObjectList.append(tracer)

    slider01 = SimpleSlider(sandbox.getModifierByName("manipulator01"), guiSettings, [100, 200])
    slider01.addChild(Label("Startgeschwindigkeit", guiSettings, [50, -30]))
    slider01.setBounds(0, 1500, 10)
    plotableObjectList.append(slider01)

    return plotableObjectList


########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################

def createTestSetupMechanicsAirDragg(mainSandbox):
    """
    This sets a simple friction, rolling test
    :param mainSandbox:
    """
    #createPolygon(mainSandbox,N=7, x0=300, y0=600, R=350, damping=5, springconstant = 100, r0=90, mass=10, texture=GlobalConstants.TEXTURE_ID_BOWLINGBALL)
    #createTower(mainSandbox,N=8, x0=-2600, y0=-910, deltaH=300, width=300, damping=50, springconstant = 2000, withborder=True, r0=90, mass=100, texture=GlobalConstants.TEXTURE_ID_VOLLEYBALL)


    ball = CircleObject(100)
    ball.setMass(40)
    ball.setCenterOfMass([0, 1000])
    ball.centerOfMassInitialVelocity = np.array([0.0, 0.0])
    ball.setTextureId(GlobalConstants.TEXTURE_ID_FOOTBALL)
    mainSandbox.addNewObject(ball)

    ball2 = CircleObject(100)
    ball2.setMass(200)
    ball2.setCenterOfMass([-250, 1000])
    ball2.centerOfMassInitialVelocity = np.array([0.0, 0.0])
    ball2.setTextureId(GlobalConstants.TEXTURE_ID_BOWLINGBALL)
    mainSandbox.addNewObject(ball2)

    ball3 = CircleObject(100)
    ball3.setMass(4)
    ball3.setCenterOfMass([250, 1000])
    ball3.centerOfMassInitialVelocity = np.array([0.0, 0.0])
    ball3.setTextureId(GlobalConstants.TEXTURE_ID_BUBBLE_BALL)
    mainSandbox.addNewObject(ball3)

    ground = Floor("Ground01")
    ground.setDirectionVector([0.0,1.0])
    ground.setPointOnPlain([0.0,0.0])
    ground.setBouncyness(0.5)
    ground.setTextureId(GlobalConstants.TEXTURE_ID_MAT_BROWN_STONE)
    mainSandbox.addNewObject(ground)

    gravity = HomogenousGravity(50*2, [0,-1],"ff01",)
    mainSandbox.addNewForceField(gravity)

    air = AmbiousAir(0.0, "air01")
    air.setDragType("DT_NEWTON") #  "DT_NEWTON",  "DT_STOKES"
    mainSandbox.addNewForceField(air)

    massPointWatcher = BasicWatcher(ball, "watcher01")
    massPointWatcher.addObservableKey("TIME")
    massPointWatcher.addObservableKey("COM_YVEL")
    mainSandbox.addNewWatcher(massPointWatcher)

    massPointWatcher2 = BasicWatcher(ball2, "watcher02")
    massPointWatcher2.addObservableKey("TIME")
    massPointWatcher2.addObservableKey("COM_YVEL")
    mainSandbox.addNewWatcher(massPointWatcher2)

    massPointWatcher3 = BasicWatcher(ball3, "watcher03")
    massPointWatcher3.addObservableKey("TIME")
    massPointWatcher3.addObservableKey("COM_YVEL")
    mainSandbox.addNewWatcher(massPointWatcher3)

    massManipulator01 = BasicManipulator(air, "manipulator01")
    massManipulator01.setManipulationKey("DENSITY")
    mainSandbox.addNewModifier(massManipulator01)


def createTestSetupMechanicsAirDragg_GUI(engingemanager, guiSettings, sandbox):
    plotableObjectList = []


    #set view
    windowProperties = guiSettings.getWindowProperties()
    windowProperties.scale=[0.4, -0.4]
    windowProperties.offset = [200, 600]

    guiSettings.setBackgroundDecorator(ImageBackgroundDecorator(windowProperties, guiSettings, GlobalConstants.IMAGE_ID_BACKGROUND_02))

    firstPlot = SimpleInlinePlot(guiSettings, [620, 20])
    firstPlot.setKeys("TIME", "COM_YVEL")
    firstPlot.addChild(Label("Geschwindigkeit / Zeit", guiSettings, [100, 25], QColor(0, 0, 0, 255)))
    firstPlot.addWatcher(sandbox.getWatcherByName("watcher01"), QtGui.QColor(152,24,46))
    firstPlot.addWatcher(sandbox.getWatcherByName("watcher02"))
    firstPlot.addWatcher(sandbox.getWatcherByName("watcher03"), QtGui.QColor(169,180,236))
    plotableObjectList.append(firstPlot)

    slider01 = SimpleSlider(sandbox.getModifierByName("manipulator01"), guiSettings, [280, 80])
    slider01.addChild(Label("Luftdichte", guiSettings, [100, -30]))
    slider01.setBounds(0, 1, 0.01)
    plotableObjectList.append(slider01)

    return plotableObjectList

########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################

def createTestSetupMechanicsManyParticle(mainSandbox):
    """
    This sets a simple friction, rolling test
    :param mainSandbox:
    """
    #createPolygon(mainSandbox,N=7, x0=300, y0=600, R=350, damping=5, springconstant = 100, r0=90, mass=10, texture=GlobalConstants.TEXTURE_ID_BOWLINGBALL)
    #createTower(mainSandbox,N=8, x0=-2600, y0=-910, deltaH=300, width=300, damping=50, springconstant = 2000, withborder=True, r0=90, mass=100, texture=GlobalConstants.TEXTURE_ID_VOLLEYBALL)

    createTower(mainSandbox,N=5, x0=-2800, y0=-510, deltaH=300, width=300, damping=50, springconstant = 2000, withborder=True, r0=90, mass=100, texture=GlobalConstants.TEXTURE_ID_MAT_BRICK_WALL)
    createPyramid(mainSandbox,N=4, x0=-1000, y0=-1000,withborder=True, r0=40, mass=3, texture=GlobalConstants.TEXTURE_ID_TENISBALL)
    createPyramid(mainSandbox,N=2, x0=-1900, y0=-1000,withborder=True, r0=90, mass=20, texture=GlobalConstants.TEXTURE_ID_VOLLEYBALL)

    ball = CircleObject(90)
    ball.setMass(200)
    ball.setCenterOfMass([-200, 100])
    ball.centerOfMassInitialVelocity = np.array([-800.0, -800.0])
    ball.setTextureId(GlobalConstants.TEXTURE_ID_BOWLINGBALL)
    mainSandbox.addNewObject(ball)

    ground = Floor("Ground01")
    ground.setDirectionVector([0.0,1.0])
    ground.setPointOnPlain([0.0,-1000.0])
    ground.setBouncyness(0.5)
    #ground.setTextureId(GlobalConstants.TEXTURE_ID_TEX_LEASH)
    ground.setTextureId(GlobalConstants.TEXTURE_ID_MAT_ROCK)


    plane = Floor("Plane")
    plane.setDirectionVector([-1.0,1.0])
    plane.setPointOnPlain([600.0,0.0])
    plane.setBouncyness(0.5)
    plane.setTextureId(GlobalConstants.TEXTURE_ID_MAT_ROCK)

    gravity = HomogenousGravity(50*2, [0,-1],"ff01",)

    mainSandbox.addNewObject(ground)
    #mainSandbox.addNewObject(plane)
    mainSandbox.addNewForceField(gravity)


def createTestSetupMechanicsManyParticle_GUI(engingemanager, guiSettings, sandbox):
    plotableObjectList = []


    #set view
    windowProperties = guiSettings.getWindowProperties()
    windowProperties.scale=[0.2, -0.2]
    windowProperties.offset = [850, 400]

    return plotableObjectList

########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################

def createTestSetupMechanicsSandbox_GUI(controller, guiSettings, sandbox):
        plotableObjectList = []

        # self.controller.getWatcherByName()
        firstPlot = SimpleInlinePlot(guiSettings, [620, 20])
        firstPlot.setKeys("TIME", "COM_YCOORD")
        firstPlot.addWatcher(sandbox.getWatcherByName("watcher01"))
        plotableObjectList.append(firstPlot)

        secondPlot = SimpleInlinePlot(guiSettings, [620, 230])
        secondPlot.setKeys("COM_XCOORD", "COM_YCOORD")
        secondPlot.addWatcher(sandbox.getWatcherByName("watcher02"))
        plotableObjectList.append(secondPlot)

        slider01 = SimpleSlider(sandbox.getModifierByName("manipulator01"), guiSettings, [600, 700])
        slider01.addChild(Label("Sping Constant", guiSettings, [50, -30]))
        plotableObjectList.append(slider01)

        slider02 = SimpleSlider(sandbox.getModifierByName("manipulator02"), guiSettings, [600, 600])
        slider02.addChild(Label("Sping Damping", guiSettings, [50, -30]))
        slider02.setBounds(0, 10)
        plotableObjectList.append(slider02)

        valueIndicator = ValueIndicator(sandbox.getWatcherByName("watcher01"), guiSettings,
                                             [500, 100])
        valueIndicator.setObservableKey("COM_YCOORD")
        plotableObjectList.append(valueIndicator)

        windowProperties = guiSettings.getWindowProperties()
        windowProperties.scale=[0.5, -0.5]
        windowProperties.offset = [300, 700]

        sandbox.showForceField()

        return plotableObjectList

def createTestSetupMechanicsSanbox(mainSandbox):

    """
    creates a default test setup with objects and forces
    :return: None
    """
    #load textures
    footballTexture = Texture("./images/balls/football.png")
    bowlingball = Texture("./images/balls/bowlingball.png")
    tennisball =  Texture("./images/balls/tennisball.png")

    mp = CircleObject(15, "mp0")
    mp.setCenterOfMass([-50.0,150.0])
    mp.centerOfMassInitialVelocity = np.array([5.0, -10.5])
    mp.setBouncyness(1.0)
    #mp.setTexture(tennisball)
    mp.setTextureId(GlobalConstants.TEXTURE_ID_TENISBALL)

    mp2 = CircleObject(15,"mp02")
    mp2.setCenterOfMass([-50.0,220.0])
    mp2.centerOfMassInitialVelocity = np.array([-5.5, 10.5])
    #mp2.setTexture(tennisball)
    mp2.setTextureId(GlobalConstants.TEXTURE_ID_TENISBALL)

    mp3 = MassPoint("mp03")
    mp3.setCenterOfMass([80.0,900.0])
    #mp3.centerOfMassInitialVelocity = np.array([-5.5, 10.5])
    mp3.setMoveable(False)

    mp4 = CircleObject(80, "mp04")
    mp4.setCenterOfMass([80.0,800.0])
    mp4.centerOfMassInitialVelocity = np.array([0, 0])
    mp4.setMass(50)
    mp4.setTextureId(GlobalConstants.TEXTURE_ID_MACKE_MR_E_ORANGE)

    mp5 = CircleObject(40,"mp05")
    mp5.setCenterOfMass([0.0,300.0])
    mp5.centerOfMassInitialVelocity = np.array([100.0, 100.0])
    mp5.setBouncyness(1.1)
    #mp5.setTexture(footballTexture)
    mp5.setTextureId(GlobalConstants.TEXTURE_ID_MACKE_MR_E_GREEN)

    mp6 = CircleObject(30,"mp06")
    mp6.setCenterOfMass([400.0,600.0])
    mp6.centerOfMassInitialVelocity = np.array([0.0, 0.0])
    mp6.setBouncyness(0.8)
    #mp6.setTexture(bowlingball)
    mp6.setTextureId(GlobalConstants.TEXTURE_ID_MACKE_MR_E_ORANGE)
    mp6.setMass(5)

    #createHexagon(mainSandbox, x0=300, y0=300, R=60, damping=1.5, springconstant = 500)
    #createPolygon(mainSandbox,N=3, x0=300, y0=300, R=200, damping=50, springconstant = 50, r0=20)


    spring01 = HarmonicSpring(springConstant=1, dampingConstant=0.0)
    spring01.setAnchorObject(mp)
    spring01.setAnchorObject(mp2)
    spring01.relax()

    spring02 = HarmonicSpring(springConstant=100, dampingConstant=10.1)
    spring02.setAnchorObject(mp2)
    spring02.setAnchorObject(mp4)
    spring02.relax()
    mp4.setCenterOfMass([80.0,600.0])



    plain = Floor("floor01")
    plain.setDirectionVector([0.5,0.1])
    plain.setPointOnPlain([-1000.0,1.0])
    plain.setBouncyness(1.0)

    plain2 = Floor("floor02")
    plain2.setDirectionVector([-0.5,0.10])
    plain2.setPointOnPlain([1000,1.0])

    plain3 = Floor("seeling01")
    plain3.setDirectionVector([-0.1*0,-1.0])
    plain3.setPointOnPlain([0.0,500.0])
    plain3.setBouncyness(1.2)

    plain4 = Floor("seeling02")
    plain4.setDirectionVector([0.1,-1.0])
    plain4.setPointOnPlain([0.0,500.0])
    plain4.setBouncyness(3.0)

    ground = Floor("Ground01")
    ground.setDirectionVector([0.0,1.0])
    ground.setPointOnPlain([400.0,0.0])
    ground.setBouncyness(0.8)

    massPointWatcher = BasicWatcher(mp4, "watcher01")
    massPointWatcher.addObservableKey("TIME")
    massPointWatcher.addObservableKey("COM_YCOORD")
    mainSandbox.addNewWatcher(massPointWatcher)

    massPointWatcher2 = BasicWatcher(mp5, "watcher02")
    massPointWatcher2.addObservableKey("COM_XCOORD")
    massPointWatcher2.addObservableKey("COM_YCOORD")
    mainSandbox.addNewWatcher(massPointWatcher2)

    springManipulator = BasicManipulator(spring02, "manipulator01")
    springManipulator.setManipulationKey("SPRING_CONSTANT")

    springManipulator2 = BasicManipulator(spring02, "manipulator02")
    springManipulator2.setManipulationKey("DAMPING_CONSTANT")

    mainSandbox.addNewModifier(springManipulator)
    mainSandbox.addNewModifier(springManipulator2)




    gravity = HomogenousGravity(20, [0,-1],"ff01",)
    gravity2 = CentralGravity(2300, [300.0,300.0],"ff02",)
    gravity3 = CentralGravity(-2300, [20.0*4,140.0*4],"ff03",)
    gravity4 = CentralGravity(2300, [40.0*6,200.0*6],"ff04",)

    air = AmbiousAir(0.0, "air01")
    air.setDragType("DT_STOKES") #  "DT_NEWTON",  "DT_STOKES"
    mainSandbox.addNewForceField(air)

    mainSandbox.addNewObject(mp)
    mainSandbox.addNewObject(mp2)
    mainSandbox.addNewObject(mp3)
    mainSandbox.addNewObject(mp4)
    mainSandbox.addNewObject(mp5)
    mainSandbox.addNewObject(mp6)

    #mainSandbox.addNewObject(plain)
    #mainSandbox.addNewObject(plain2)
    #self.mainSandbox.addNewObject(plain3)
    #self.mainSandbox.addNewObject(plain4)
    mainSandbox.addNewObject(ground)
    #mainSandbox.addNewForceField(gravity)
    mainSandbox.addNewForceField(gravity2)
    mainSandbox.addNewForceField(gravity3)
    mainSandbox.addNewForceField(gravity4)



    mainSandbox.addNewForceField(spring01)
    mainSandbox.addNewForceField(spring02)

########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################


def createTestSetupMechanicsForcefield(mainSandbox):

    gravity1 = CentralGravity(2300, [300.0,300.0],"ff02",)
    gravity2 = CentralGravity(-2300, [20.0*4,140.0*4],"ff03",)
    mainSandbox.addNewForceField(gravity1)
    mainSandbox.addNewForceField(gravity2)


########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################

def createTestSetupMechanicsMacke_GUI(engingemanager, guiSettings, sandbox):
    plotableObjectList = []




    #set view
    windowProperties = guiSettings.getWindowProperties()
    windowProperties.scale=[0.3, -0.3]
    windowProperties.offset = [850, 500]

    slider01 = SimpleSlider(sandbox.getModifierByName("manipulator01"), guiSettings, [200, 150])
    slider01.addChild(Label("Schwach", guiSettings, [-40, -20], QtGui.QColor(12, 12, 12, 255)))
    slider01.addChild(Label("Mittel", guiSettings, [120, -20], QtGui.QColor(12, 12, 12, 255)))
    slider01.addChild(Label("Stark", guiSettings, [280, -20], QtGui.QColor(12, 12, 12, 255)))
    slider01.setBounds(0, 20)
    plotableObjectList.append(slider01)

    firstPlot = SimpleInlinePlot(guiSettings, [620, 20], QSizeF(400, 250))
    firstPlot.setKeys("TIME", "COM_XCOORD")
    firstPlot.addWatcher(sandbox.getWatcherByName("watcher01"))
    firstPlot.addWatcher(sandbox.getWatcherByName("watcher02"), QtGui.QColor(152,24,46))
    firstPlot.addWatcher(sandbox.getWatcherByName("watcher03"), QtGui.QColor(169,180,236))
    firstPlot.addChild(Label("Auslenkung / Zeit", guiSettings, [100, 25], QColor(0, 0, 0, 255)))
    plotableObjectList.append(firstPlot)

    return plotableObjectList
    #return []


def createTestSetupMechanicsMacke(mainSandbox):

    """
    creates a default test setup with objects and forces
    :return: None
    """
    delta = 500
    x0 = -3000
    y0 = 900
    x1 = x0
    y1 = y0-900
    mass = 100

    springConstantString = 10000
    dampingConstantString = 1000

    sprintConstant = 0*100/50

    tenis = CircleObject(50)
    tenis.setCenterOfMass([x1+3500,y1+680])
    tenis.centerOfMassInitialVelocity = np.array([-1100, -200])
    #tenis.setCenterOfMass([x1+3500,y1-580])
    #tenis.centerOfMassInitialVelocity = np.array([-700, 500])
    tenis.setMass(6)
    tenis.setTextureId(GlobalConstants.TEXTURE_ID_TENISBALL)


    mp1 = MassPoint()
    mp1.setCenterOfMass([x0,y0])
    mp1.setMoveable(False)
    mp2 = CircleObject(100)
    mp2.setCenterOfMass([x1,y1])
    mp2.centerOfMassInitialVelocity = np.array([0, 0])
    mp2.setMass(mass)
    mp2.setTextureId(GlobalConstants.TEXTURE_ID_MACKE_MR_E_ORANGE)


    mp3 = MassPoint()
    mp3.setCenterOfMass([x0+1*delta, y0])
    #mp3.centerOfMassInitialVelocity = np.array([-5.5, 10.5])
    mp3.setMoveable(False)
    mp4 = CircleObject(100)
    mp4.setCenterOfMass([x1+1*delta, y1])
    mp4.centerOfMassInitialVelocity = np.array([0, 0])
    mp4.setMass(mass)
    mp4.setTextureId(GlobalConstants.TEXTURE_ID_MACKE_MR_E_GREEN)

    mp5 = MassPoint()
    mp5.setCenterOfMass([x0+2*delta, y0])
    mp5.setMoveable(False)
    mp6 = CircleObject(100)
    mp6.setCenterOfMass([x1+2*delta, y1])
    mp6.centerOfMassInitialVelocity = np.array([0, 0])
    mp6.setMass(mass)
    mp6.setTextureId(GlobalConstants.TEXTURE_ID_MACKE_MR_E_ORANGE)

    mp7 = MassPoint()
    mp7.setCenterOfMass([x0+3*delta, y0])
    mp7.setMoveable(False)
    mp8 = CircleObject(100)
    mp8.setCenterOfMass([x1+3*delta, y1])
    mp8.centerOfMassInitialVelocity = np.array([0, 0])
    mp8.setMass(mass)
    mp8.setTextureId(GlobalConstants.TEXTURE_ID_MACKE_MR_E_ORANGE)
    mp9 = MassPoint()
    mp9.setCenterOfMass([x0+4*delta, y0])
    mp9.setMoveable(False)
    mp10 = CircleObject(100)
    mp10.setCenterOfMass([x1+4*delta, y1])
    mp10.centerOfMassInitialVelocity = np.array([0, 0])
    mp10.setMass(mass)
    mp10.setTextureId(GlobalConstants.TEXTURE_ID_MACKE_MR_E_GREEN)

    spring01 = HarmonicSpring(springConstant=springConstantString, dampingConstant=dampingConstantString)
    spring01.setAnchorObject(mp1)
    spring01.setAnchorObject(mp2)
    spring01.relax()
    spring02 = HarmonicSpring(springConstant=springConstantString, dampingConstant=dampingConstantString)
    spring02.setAnchorObject(mp3)
    spring02.setAnchorObject(mp4)
    spring02.relax()
    spring03 = HarmonicSpring(springConstant=springConstantString, dampingConstant=dampingConstantString)
    spring03.setAnchorObject(mp5)
    spring03.setAnchorObject(mp6)
    spring03.relax()
    spring04 = HarmonicSpring(springConstant=springConstantString, dampingConstant=dampingConstantString)
    spring04.setAnchorObject(mp7)
    spring04.setAnchorObject(mp8)
    spring04.relax()
    spring05 = HarmonicSpring(springConstant=springConstantString, dampingConstant=dampingConstantString)
    spring05.setAnchorObject(mp9)
    spring05.setAnchorObject(mp10)
    spring05.relax()


    springd1 = HarmonicSpring(springConstant=sprintConstant, dampingConstant=0)
    springd1.setAnchorObject(mp2)
    springd1.setAnchorObject(mp4)
    springd1.setTextureId(GlobalConstants.TEXTURE_ID_MACKE_SPRING_ORANGE_GREEN)
    springd1.relax()
    springd2 = HarmonicSpring(springConstant=sprintConstant, dampingConstant=0)
    springd2.setAnchorObject(mp4)
    springd2.setAnchorObject(mp6)
    springd2.setTextureId(GlobalConstants.TEXTURE_ID_MACKE_SPRING_GREEN_ORANGE)
    springd2.relax()
    springd3 = HarmonicSpring(springConstant=sprintConstant, dampingConstant=0)
    springd3.setAnchorObject(mp6)
    springd3.setAnchorObject(mp8)
    springd3.setTextureId(GlobalConstants.TEXTURE_ID_MACKE_SPRING_ORANGE)
    springd3.relax()
    springd4 = HarmonicSpring(springConstant=sprintConstant, dampingConstant=0)
    springd4.setAnchorObject(mp8)
    springd4.setAnchorObject(mp10)
    springd4.setTextureId(GlobalConstants.TEXTURE_ID_MACKE_SPRING_ORANGE_GREEN)
    springd4.relax()

    springManipulator = MultiManipulator("manipulator01")
    springManipulator.setManipulationKey("SPRING_CONSTANT")
    springManipulator.addObject(springd1)
    springManipulator.addObject(springd2)
    springManipulator.addObject(springd3)
    springManipulator.addObject(springd4)
    mainSandbox.addNewModifier(springManipulator)


    massPointWatcher = BasicWatcher(mp8, "watcher01")
    massPointWatcher.addObservableKey("TIME")
    massPointWatcher.addObservableKey("COM_XCOORD")
    mainSandbox.addNewWatcher(massPointWatcher)

    massPointWatcher2 = BasicWatcher(mp10, "watcher02")
    massPointWatcher2.addObservableKey("TIME")
    massPointWatcher2.addObservableKey("COM_XCOORD")
    mainSandbox.addNewWatcher(massPointWatcher2)

    massPointWatcher3 = BasicWatcher(mp6, "watcher03")
    massPointWatcher3.addObservableKey("TIME")
    massPointWatcher3.addObservableKey("COM_XCOORD")
    mainSandbox.addNewWatcher(massPointWatcher3)

    mainSandbox.addNewObject(tenis)


    mainSandbox.addNewObject(mp1)
    mainSandbox.addNewObject(mp2)
    mainSandbox.addNewObject(mp3)
    mainSandbox.addNewObject(mp4)
    mainSandbox.addNewObject(mp5)
    mainSandbox.addNewObject(mp6)
    mainSandbox.addNewObject(mp7)
    mainSandbox.addNewObject(mp8)
    mainSandbox.addNewObject(mp9)
    mainSandbox.addNewObject(mp10)

    gravity = HomogenousGravity(200, [0,-1],"ff01",)
    mainSandbox.addNewForceField(gravity)
    mainSandbox.addNewForceField(spring01)
    mainSandbox.addNewForceField(spring02)
    mainSandbox.addNewForceField(springd1)
    mainSandbox.addNewForceField(spring03)
    mainSandbox.addNewForceField(springd2)
    mainSandbox.addNewForceField(spring04)
    mainSandbox.addNewForceField(springd3)
    mainSandbox.addNewForceField(spring05)
    mainSandbox.addNewForceField(springd4)


    ground = Floor("Ground01")
    ground.setDirectionVector([0.0,1.0])
    ground.setPointOnPlain([0.0,-700.0])
    ground.setBouncyness(0.5)
    #ground.setTextureId(GlobalConstants.TEXTURE_ID_TEX_LEASH)
    ground.setTextureId(GlobalConstants.TEXTURE_ID_MAT_ROCK)
    mainSandbox.addNewObject(ground)


########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################