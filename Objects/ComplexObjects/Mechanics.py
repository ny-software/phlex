import math
import random

from Objects.CircleObject import *
from Objects.Interactions.HarmonicSpring import *

def createXmasTree(sandbox,N=10, x0=300, y0=1500, deltaH=200, width=200, springconstant=50, damping=10):
    list0 = []
    list1 = []

    #first create all mass points
    for i in range(N):
        list0.append(MassPoint())
        list1.append(MassPoint())

    list0.append(MassPoint())
    list1.append(MassPoint())
    list0[N].setMass(10)
    list0[N].setCenterOfMass([x0-width/2.0,y0-(N+1)*deltaH])
    list0[N].centerOfMassVelocity = np.array([0.0, 0.0])
    list1[N].setMass(10)
    list1[N].setCenterOfMass([x0+width/2.0,y0-(N+1)*deltaH])
    list1[N].centerOfMassVelocity = np.array([0.0, 0.0])
    #sandbox.addNewObject(list0[N])
    #sandbox.addNewObject(list1[N])

    #set positions
    for i in range(N):
        if(i%2==1): deltax = 0
        else:   deltax = (i)/N*width*3
        list0[i].setMass(1)
        list0[i].setCenterOfMass([x0-width/2.0-deltax,y0-(i-i%2)*deltaH])
        list0[i].centerOfMassVelocity = np.array([0.0, 0.0])
        #list0[i].setTextureId(texture)
        if(i>0):
            sandbox.addNewObject(list0[i])
        list1[i].setMass(1)
        list1[i].setCenterOfMass([x0+width/2.0+deltax,y0-(i-i%2)*deltaH])
        list1[i].centerOfMassVelocity = np.array([0.0, 0.0])
        #list1[i].setTextureId(texture)
        if(i>0):
            sandbox.addNewObject(list1[i])

    list0[0].setCenterOfMass([x0,y0])
    list1[0].setCenterOfMass([x0,y0+deltaH*5])


    #create springs
    for i in range(N):
        trsp01 = HarmonicSpring(springconstant, dampingConstant=damping)
        trsp01.setAnchorObject(list0[i])
        trsp01.setAnchorObject(list1[i])
        trsp01.relax()
        if(i>0):
            sandbox.addNewForceField(trsp01)

        if(i>0 and i<N-1):
            trsp02 = HarmonicSpring(springconstant, dampingConstant=damping)
            trsp02.setAnchorObject(list0[i])
            trsp02.setAnchorObject(list0[i+1])
            trsp02.relax()
            sandbox.addNewForceField(trsp02)

            trsp03 = HarmonicSpring(springconstant, dampingConstant=damping)
            trsp03.setAnchorObject(list1[i])
            trsp03.setAnchorObject(list1[i+1])
            trsp03.relax()
            sandbox.addNewForceField(trsp03)

        if(i%2==1 and i<N-2):
            trsp = HarmonicSpring(springconstant, dampingConstant=damping)
            trsp.setAnchorObject(list0[i])
            trsp.setAnchorObject(list0[i+2])
            trsp.relax()
            sandbox.addNewForceField(trsp)
            trsp = HarmonicSpring(springconstant, dampingConstant=damping)
            trsp.setAnchorObject(list1[i])
            trsp.setAnchorObject(list1[i+2])
            trsp.relax()
            sandbox.addNewForceField(trsp)
            trsp = HarmonicSpring(springconstant, dampingConstant=damping)
            trsp.setAnchorObject(list0[i])
            trsp.setAnchorObject(list1[i+2])
            trsp.relax()
            sandbox.addNewForceField(trsp)
            trsp = HarmonicSpring(springconstant, dampingConstant=damping)
            trsp.setAnchorObject(list1[i])
            trsp.setAnchorObject(list0[i+2])
            trsp.relax()
            sandbox.addNewForceField(trsp)

    #attach balls
    for i in range(1, N-2):
            if(i%2==1): deltax = 0
            else:   deltax = (i)/N*width*3
            tr0 = CircleObject(40)
            tr0.setMass(1)
            tr0.setCenterOfMass([x0-width/2.0-deltax,y0-(i-i%2+0.8)*deltaH])
            tr0.centerOfMassVelocity = np.array([random.randint(-500, 500), random.randint(-500, 500)])
            sandbox.addNewObject(tr0)

            tr1 = CircleObject(40)
            tr1.setMass(1)
            tr1.setCenterOfMass([x0+width/2.0+deltax,y0-(i-i%2+0.8)*deltaH])
            tr1.centerOfMassVelocity = np.array([random.randint(-500, 500), random.randint(-500, 500)])
            sandbox.addNewObject(tr1)

            trsp = HarmonicSpring(springconstant, dampingConstant=damping)
            trsp.setAnchorObject(tr0)
            trsp.setAnchorObject(list0[i])
            trsp.relax()
            sandbox.addNewForceField(trsp)

            trsp = HarmonicSpring(springconstant, dampingConstant=damping)
            trsp.setAnchorObject(tr1)
            trsp.setAnchorObject(list1[i])
            trsp.relax()
            sandbox.addNewForceField(trsp)

def createPyramid(sandbox,N=6, x0=300, y0=300, damping=1.5, springconstant = 500, withborder=True, r0=10, mass=10,texture=None):

    for i in range(N):
        for j in range(N-i):
            tr0 = CircleObject(r0)
            tr0.setMass(mass)
            tr0.setCenterOfMass([x0-(N/2-i)*2*r0+j*r0,y0+r0+2*j*r0*sqrt(3)/2])
            tr0.centerOfMassVelocity = np.array([0.0, 0.0])
            tr0.setTextureId(texture)
            sandbox.addNewObject(tr0)


def createTower(sandbox,N=6, x0=300, y0=300, deltaH=50, width=50, damping=1.5, springconstant = 500, withborder=True, r0=10, mass=10,texture=None):
    list0 = []
    list1 = []

    for i in range(N):
        list0.append(CircleObject(r0))
        list1.append(CircleObject(r0))

    for i in range(N):
        list0[i].setMass(mass)
        list0[i].setCenterOfMass([x0-width/2.0,y0+i*deltaH])
        list0[i].centerOfMassVelocity = np.array([0.0, 0.0])
        list0[i].setTextureId(texture)
        sandbox.addNewObject(list0[i])
        list1[i].setMass(mass)
        list1[i].setCenterOfMass([x0+width/2.0,y0+i*deltaH])
        list1[i].centerOfMassVelocity = np.array([0.0, 0.0])
        list1[i].setTextureId(texture)
        sandbox.addNewObject(list1[i])

        trsp01 = HarmonicSpring(springconstant, dampingConstant=damping)
        trsp01.setAnchorObject(list0[i])
        trsp01.setAnchorObject(list1[i])
        trsp01.relax()
        sandbox.addNewForceField(trsp01)

    for i in range(1,N):
        trsp01 = HarmonicSpring(springconstant, dampingConstant=damping)
        trsp01.setAnchorObject(list0[i])
        trsp01.setAnchorObject(list0[i-1])
        trsp01.relax()
        sandbox.addNewForceField(trsp01)

        trsp01 = HarmonicSpring(springconstant, dampingConstant=damping)
        trsp01.setAnchorObject(list1[i])
        trsp01.setAnchorObject(list1[i-1])
        trsp01.relax()
        sandbox.addNewForceField(trsp01)

        trsp01 = HarmonicSpring(springconstant, dampingConstant=damping)
        trsp01.setAnchorObject(list1[i])
        trsp01.setAnchorObject(list0[i-1])
        trsp01.relax()
        sandbox.addNewForceField(trsp01)

        trsp01 = HarmonicSpring(springconstant, dampingConstant=damping)
        trsp01.setAnchorObject(list0[i])
        trsp01.setAnchorObject(list1[i-1])
        trsp01.relax()
        sandbox.addNewForceField(trsp01)



def createPolygon(sandbox, N=6, x0=300, y0=300, R=60, damping=1.5, springconstant = 500, withborder=True, r0=10, mass=10,texture=None):
    phi = 1.0*math.pi/N

    #center
    tr0 = CircleObject(r0)
    tr0.setMass(mass)
    tr0.setCenterOfMass([x0,y0])
    tr0.centerOfMassVelocity = np.array([0.0, 0.0])
    tr0.setTextureId(texture)
    sandbox.addNewObject(tr0)

    pointlist = []

    for i in range(N) :
        tr1 = CircleObject(r0)
        tr1.setMass(mass)
        tr1.setCenterOfMass([x0+R*math.cos(2*math.pi/N*i+phi),y0+R*math.sin(2*math.pi/N*i+phi)])
        tr1.centerOfMassVelocity = np.array([0.0, 0.0])
        tr1.setTextureId(texture)
        sandbox.addNewObject(tr1)
        pointlist.append(tr1)
        #spring to center
        trsp01 = HarmonicSpring(springconstant, dampingConstant=damping)
        #trsp01.setAnchorObject(tr0)
        trsp01.setAnchorObject(tr1)
        trsp01.relax()
        sandbox.addNewForceField(trsp01)

    if(withborder):
        for i in range(N-1):
            trsp01 = HarmonicSpring(springconstant, dampingConstant=damping)
            trsp01.setAnchorObject(pointlist[i])
            trsp01.setAnchorObject(pointlist[i+1])
            trsp01.relax()
            sandbox.addNewForceField(trsp01)
        trsp01 = HarmonicSpring(springconstant, dampingConstant=damping)
        trsp01.setAnchorObject(pointlist[0])
        trsp01.setAnchorObject(pointlist[N-1])
        trsp01.relax()
        sandbox.addNewForceField(tr0)

def createHexagon(sandbox, x0=300, y0=300, R=60, damping=1.5, springconstant = 500):
    N = 6
    phi = 1.0*math.pi/N
    tr0 = CircleObject(5)
    tr0.setCenterOfMass([x0,y0])
    tr0.centerOfMassVelocity = np.array([0.0, 0.0])
    tr1 = CircleObject(5)
    tr1.setCenterOfMass([x0+R*math.cos(2*math.pi/N*0+phi),y0+R*math.sin(2*math.pi/N*0+phi)])
    tr1.centerOfMassVelocity = np.array([0.0, 0.0])
    tr2 = CircleObject(5)
    tr2.setCenterOfMass([x0+R*math.cos(2*math.pi/N*1.0+phi),y0+R*math.sin(2*math.pi/N*1.0+phi)])
    tr2.centerOfMassVelocity = np.array([0.0, 0.0])
    tr3 = CircleObject(5)
    tr3.setCenterOfMass([x0+R*math.cos(2*math.pi/N*2.0+phi),y0+R*math.sin(2*math.pi/N*2.0+phi)])
    tr3.centerOfMassVelocity = np.array([0.0, 0.0])
    tr4 = CircleObject(5)
    tr4.setCenterOfMass([x0+R*math.cos(2*math.pi/N*3.0+phi),y0+R*math.sin(2*math.pi/N*3.0+phi)])
    tr4.centerOfMassVelocity = np.array([0.0, 0.0])
    tr5 = CircleObject(5)
    tr5.setCenterOfMass([x0+R*math.cos(2*math.pi/N*4.0+phi),y0+R*math.sin(2*math.pi/N*4.0+phi)])
    tr5.centerOfMassVelocity = np.array([0.0, 0.0])
    tr6 = CircleObject(5)
    tr6.setCenterOfMass([x0+R*math.cos(2*math.pi/N*5.0+phi),y0+R*math.sin(2*math.pi/N*5.0+phi)])
    tr6.centerOfMassVelocity = np.array([0.0, 0.0])

    trsp12 = HarmonicSpring(springconstant, dampingConstant=damping)
    trsp23 = HarmonicSpring(springconstant, dampingConstant=damping)
    trsp34 = HarmonicSpring(springconstant, dampingConstant=damping)
    trsp45 = HarmonicSpring(springconstant, dampingConstant=damping)
    trsp56 = HarmonicSpring(springconstant, dampingConstant=damping)
    trsp61 = HarmonicSpring(springconstant, dampingConstant=damping)
    trsp12.setAnchorObject(tr1)
    trsp12.setAnchorObject(tr2)
    trsp23.setAnchorObject(tr2)
    trsp23.setAnchorObject(tr3)
    trsp34.setAnchorObject(tr3)
    trsp34.setAnchorObject(tr4)
    trsp45.setAnchorObject(tr4)
    trsp45.setAnchorObject(tr5)
    trsp56.setAnchorObject(tr5)
    trsp56.setAnchorObject(tr6)
    trsp61.setAnchorObject(tr6)
    trsp61.setAnchorObject(tr1)
    trsp12.relax()
    trsp23.relax()
    trsp34.relax()
    trsp45.relax()
    trsp56.relax()
    trsp61.relax()
    trsp01 = HarmonicSpring(springconstant, dampingConstant=damping)
    trsp02 = HarmonicSpring(springconstant, dampingConstant=damping)
    trsp03 = HarmonicSpring(springconstant, dampingConstant=damping)
    trsp04 = HarmonicSpring(springconstant, dampingConstant=damping)
    trsp05 = HarmonicSpring(springconstant, dampingConstant=damping)
    trsp06 = HarmonicSpring(springconstant, dampingConstant=damping)
    trsp01.setAnchorObject(tr0)
    trsp01.setAnchorObject(tr1)
    trsp02.setAnchorObject(tr0)
    trsp02.setAnchorObject(tr2)
    trsp03.setAnchorObject(tr0)
    trsp03.setAnchorObject(tr3)
    trsp04.setAnchorObject(tr0)
    trsp04.setAnchorObject(tr4)
    trsp05.setAnchorObject(tr0)
    trsp05.setAnchorObject(tr5)
    trsp06.setAnchorObject(tr0)
    trsp06.setAnchorObject(tr6)
    trsp01.relax()
    trsp02.relax()
    trsp03.relax()
    trsp04.relax()
    trsp05.relax()
    trsp06.relax()

    sandbox.addNewObject(tr0)
    sandbox.addNewObject(tr1)
    sandbox.addNewObject(tr2)
    sandbox.addNewObject(tr3)
    sandbox.addNewObject(tr4)
    sandbox.addNewObject(tr5)
    sandbox.addNewObject(tr6)

    sandbox.addNewForceField(trsp12)
    sandbox.addNewForceField(trsp23)
    sandbox.addNewForceField(trsp34)
    sandbox.addNewForceField(trsp45)
    sandbox.addNewForceField(trsp56)
    sandbox.addNewForceField(trsp61)
    sandbox.addNewForceField(trsp01)
    sandbox.addNewForceField(trsp02)
    sandbox.addNewForceField(trsp03)
    sandbox.addNewForceField(trsp04)
    sandbox.addNewForceField(trsp05)
    sandbox.addNewForceField(trsp06)