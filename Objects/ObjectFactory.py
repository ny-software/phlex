from . import *
from DataIO import Serializable



def produceSerializableObject(name:str) -> Serializable:
    constructor = globals()[id]
    instance = constructor()
    if not isinstance(instance, Serializable):
        raise TypeError
    return instance