#class Name:  BasicObject
#Author: djk

from Objects.BasicObject import *
import numpy as np
from Objects.forceFields.BasicForceField import *
from Objects.Interactions.BasicInteraction import *
from math import *
from AdvancedMath.Tools import *

class HarmonicSpring(BasicInteraction):
    """ Prototpye of an Object for a Sanbox Unit
    """
    harmonicSpringCounter=0
    #dictionarry which contains all atributes for this object
    harmonicSpringDictionary = {}
    #"TIME":"Time", "MASS":"Mass", "COM_XCOORD":"x-Coordinate", "COM_YCOORD":"y-Coordinate", "COM_XVEL":"x-Velocity", "COM_YVEL":"y-Velosicty"}

    def __init__(self, springConstant=1.3, id:str=None, dampingConstant = 0.0):
        """ constructor         """

        HarmonicSpring.harmonicSpringCounter+=1
        if(id==None):
            id="h-spring_"+str(HarmonicSpring.harmonicSpringCounter)
        super( HarmonicSpring, self ).__init__(id)

        #physical properties:
        #local time
        self.time=0.0
        self.springConstant = springConstant
        self.dampingConstant = dampingConstant
        self.relaxedLength = 1.0

        self.anchor01 = [0.0,0.0]
        self.anchor02 = [0.0,0.0]
        self.boundToObjects=False
        self.objectList = []

        self.observableDictionary = self.observableDictionary.update(self.harmonicSpringDictionary)

        self.anchorA = MechanicAnchor(self)
        self.anchorB = MechanicAnchor(self)
        self.anchors = {self.anchorA.getObjectID(): self.anchorA, self.anchorB.getObjectID():self.anchorB}

        print("MassObject created")

    def getObservableType(self, key:str):
        if key=="TIME":
            return UnitType.TIME
        elif key=="SPRING_CONSTANT":
            return UnitType.FORCE_OVER_DISTANCE
        elif key=="DAMPING_CONSTANT":
            return UnitType.DAMPING
        else:
            return super().getObservableType(key)

    def getObservable(self, key:str):
        return 0

    def getObservable(self, key:str):
        if key=="TIME":
            return self.time
        elif key=="SPRING_CONSTANT":
            return self.springConstant
        elif key=="DAMPING_CONSTANT":
            return self.dampingConstant
        else:
            return 0

    def isAttached(self) -> bool:
        """
        are all anchors attached to an object?
        """
        #run through this anchors
        for anchor in self.anchors.values():
            if(anchor.hasAttachedAnchor()==False):
                return False
        #if all anchors are attached to an object, everything is fine
        return True

    def attachAnchor(self, anchor) -> None:
        if(anchor is None):
            return
        if(self.anchorA.hasAttachedAnchor()==False):
            print("attach a")
            self.anchorA.attachAnchor(anchor)
            return
        if(self.anchorB.hasAttachedAnchor()==False):
            print("attach b")
            self.anchorB.attachAnchor(anchor)
            self.boundToObjects=True  #list is full!
            return

    def getCoordinateList(self) -> list:
        coords = []
        #run through this anchors
        for anchor in self.anchors.values():
            if(anchor.hasAttachedAnchor()):
                #run through anchors which are attached to anchor
                for opjAnchor in anchor.getAttachedAnchors():
                    coords.append(opjAnchor.getCoordinates())

    def setAnchorObject(self, obj:BasicObject, id:int=None) -> None:
        """
        deprecated!
        if an object has only one anchor, it is easieer to use this method, but instead, the method
        attachAnchor shoudl be used!
        :param obj: object to attache
        :param id: deprecated
        """
        if(isinstance(obj, MassPoint)):
            for anchor in obj.anchors.values():
                self.attachAnchor(anchor)


    def setObservable(self, key:str, val):
        if key=="TIME":
            self.time = val
        elif key=="SPRING_CONSTANT":
            self.springConstant = val
            print("spring   ", self.springConstant)
        elif key=="DAMPING_CONSTANT":
            self.dampingConstant= val
        else:
            return False

        return True

    def getAnchorPos(self, id:int):
        #ToDo: generalize!!!
        try:
            if(id==0):
                return self.anchorA.getCenterOfMass()
            if(id==1):
                return self.anchorB.getCenterOfMass()
            #return self.objectList[id].getCenterOfMass()
        except:
            return np.array([0.0,0.0])

    def relax(self):
        if(self.boundToObjects):
            print("Spring ", self, " relaxed to ", self.relaxedLength)
            deltaX = self.getAnchorPos(0) - self.getAnchorPos(1)
            self.relaxedLength = norm(deltaX)

    def calculateForce(self):
        if(self.boundToObjects):
            com1 = self.anchorA.getCenterOfMass()
            com2 = self.anchorB.getCenterOfMass()
            vec = com1 - com2
            lengthVec = norm(vec)
            dvabs=0.0
            if(self.dampingConstant != 0.0):
                try:
                    dv = (self.anchorA.getCenterOfMassVelocity()-self.anchorB.getCenterOfMassVelocity())
                    dvabs = scalar(dv, vec)/lengthVec
                except:
                    pass
            return -( self.springConstant*(lengthVec - self.relaxedLength) +  dvabs*self.dampingConstant ) * vec/lengthVec

    def updateInteractionForces(self):
        if(self.boundToObjects):
            force = self.calculateForce()
            self.anchorA.addCenterOfMassForce(force)
            self.anchorB.addCenterOfMassForce(-force)

    def move(self, dx, dy):
        """
        has this method any sence? the possition of an string is definded by its anchors
        :param dx:
        :param dy:
        """
        if(not self.boundToObjects):
            self.anchorA.move(dx,dy)
            self.anchorB.move(dx,dy)


    def getDescription(self) -> str:
        ''' intern description for debugging '''
        return "This is the " + self.objectId +  " Harmonic SPring Object "

    def __str__(self):
        return self.getDescription()

