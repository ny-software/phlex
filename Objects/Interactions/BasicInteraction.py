#class Name:  BasicForceField
#Author: djk

import numpy as np
from Objects.BasicObject import *
from Objects.MassPoint import *
from Objects.Conectors.MechanicAnchor import *

class BasicInteraction(BasicObject):
    """ Prototpye of a ForceField used in Mechanicla Sandboxes
    """

    def __init__(self, id:str):
        """ constructor         """
        super( BasicInteraction, self ).__init__(id)
        print("BasicForceField created")

 #       self._anchors = {}

    def getObjectID(self) -> str:
        return self.objectId

    def getDescription(self) -> str:
        ''' intern description for debugging '''
        return "This is a basic Interaction!"

    def getForce(self, pos):
        return np.array([0.0,0.0])

#    def getAnchors(self):
#        return self.anchors

    def updateInteractionForces(self):
        pass

    def __str__(self):
        return self.getDescription()

