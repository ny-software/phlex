from AdvancedMath.Tools import *
from Objects.MassPoint import *


class CircleObject(MassPoint):

    def __init__(self, rad=10.0, id=None):
        super(CircleObject, self).__init__(id)

        self.__radius = rad
        self.__bounciness = 1

    # ------------------ Properties ----------------------------- #

    @property
    def radius(self) -> float:
        """
        Returns the radius of this circular object
        :return: the radius of the object
        """
        return self.__radius

    @radius.setter
    def radius(self, rad:float) -> None:
        """
        Set the radius of the object (have to be >= 0)
        :param rad: the new radius of the object (have to be >0)
        """
        if(rad >= 0):
            self.__radius = rad


    @property
    def bounciness(self) -> float:
        """
        Returns the bounciness of the object used in hits
        :return: the bouncy coefficient of this object
        """
        return self.__bounciness
    @bounciness.setter
    def bounciness(self, bounce:float) -> None:
        """
        Sets the bounciness of the object used in hits
        :param bounce: the new bouncy coefficient of this object
        """
        self.__bounciness = bounce

    # ------------------------------------------------------------ #


    def collision(self, obj:BasicObject) -> None:
        """
        Performes the collision of this object, with an other object (obj)
        Implemented wrigth now:
         - collision with other CircleObject

        :param obj: The other object
        """
        if(isinstance(obj, CircleObject)):
            #fast, rought test:
            deltaR = obj.radius+self.radius
            dist =   obj.getCenterOfMass() - self.getCenterOfMass()  #vector pointing to opposite
            if(norm(dist) > deltaR):
                return
            m1 = obj.getMass()
            m2 = self.getMass()
            deltaV = obj.getCenterOfMassVelocity() - self.getCenterOfMassVelocity() #relatvie velosity
            absDist = norm(dist)
            #unit direction vector

            if 0 == absDist: #avoids division by zero
                direction = dist
            else:
                direction = dist/absDist

            procejctedDeltaV = scalar(direction, deltaV)
            #if the distance is smaller than the radius and
            if(procejctedDeltaV < 0.0):
                #perpendicular vector
                plain = np.array([direction[1], -direction[0]])

                v1 = scalar(direction, obj.getCenterOfMassVelocity())
                v2 = scalar(direction, self.getCenterOfMassVelocity())
                if(self.isMoveable() and obj.isMoveable()):
                    obj.setCenterOfMassVelocity( ( 2*m2*v2 + (m1-m2)*v1  )/(m1+m2) * direction + scalar(plain, obj.getCenterOfMassVelocity())*plain  )
                    self.setCenterOfMassVelocity( ( 2*m1*v1 + (m2-m1)*v2  )/(m1+m2) * direction + scalar(plain, self.getCenterOfMassVelocity())*plain  )

                    #set new distance, but ceep center of mass
                    obj.setCenterOfMass(obj.getCenterOfMass()-m1/(m1+m2)*dist*(1.0-deltaR/absDist))
                    self.setCenterOfMass(self.getCenterOfMass()+m2/(m1+m2)*dist*(1.0-deltaR/absDist))
                else:
                    if(self.isMoveable() is not True):
                        obj.setCenterOfMassVelocity( ( -v1  ) * direction + scalar(plain, obj.getCenterOfMassVelocity())*plain  )
                        obj.setCenterOfMass(obj.getCenterOfMass()-dist*(1.0-deltaR/absDist))




    def flipVelocityAlongPlane(self, nv:list, factor:float=1.0) -> None:
        """
        If this objects hits a plane (which is not checkt in this method) its velocity flipts along the plane given by a normalvector nv. An addition bounciness factor can be given here to enhance or damp the flip
        :param nv: the planes normal vector
        :param factor: additional bounciness factor
        """
        super(CircleObject, self).flipVelocityAlongPlane(nv, factor*self.bounciness)