#class Name:  BasicObject
#Author: djk


import numpy as np
from AdvancedPhysics.MaterialType import *
from Units.UnitTypes import *
import abc
from DataIO.Serializable import Serializeable

class BasicObject(Serializeable):
    """ Prototpye of an Object for a Sanbox Unit
    """
    __metaclass__ = abc.ABCMeta

    def __init__(self, id:str):
        """ constructor         """
        super(BasicObject, self).__init__()
        #universal object id
        self.__objectId = id

        self.__moveable = True
        self.__visible = True
        self.__selected = False
        self.__coordinates = np.array([0.0,0.0])

        self.__hasTexture=False
        self.__texture = None
        self.__textureId = 0

        #set test material
        self.__hasMaterial=True
        self.__material = MaterialType()

        #dictonary which contains all Observables and the correspondin identification Names
        self.observableDictionary = {}
        print("Basic Object created with id: ", self.getObjectID())

        #dictionarry of the anchors of the object
        self.__objectAnchors = {}


    # ------------------ Initialization ----------------------------- #


    def initialize(self) -> None:
        """
        Here values can be set before the simulation starts
        This method is called each time before the simulation begins
        """
        pass


    # ------------------ Properties ----------------------------- #

    @property
    def coordinates(self) -> np.array:
        """
        Returns the real coordinates of the object as np.array(xcoord, ycoord)
        :return: An np.array of the actual real coordinates of the object
        """
        return self.__coordinates
    @coordinates.setter
    def coordinates(self, coo:np.array):
        """
        Sets the real coordinates of the object
        :param coo: An np.array of the new real coordinates of the object
        """
        if(coo is not None):
            self.__coordinates = coo


    @property
    def moveable(self) -> bool:
        """
        Is the object affected by the mechanics engine, therefore will it move during the simulation steps
        :return: True: the objects moves during simulation, False: otherwise
        """
        return self.__moveable
    @moveable.setter
    def moveable(self, ma:bool) -> None:
        """
        Is the object affected by the mechanics engine, therefore will it move during the simulation steps
        :param ma: True: the objects shoulde move during simulation, False: otherwise
        """
        self.__moveable = ma

    @property
    def objectId(self) -> str:
        """
        the object id is a unique string which identifys the object in the sandbox and can be used for referencing
        :return: the id of the object
        """
        return self.__objectId

    @property
    def visible(self) -> bool:
        """
        Is the object shown in the visualisation of the sandbox, if false, the object will be hidden
        :return: True: object is visible, False: otherwise
        """
        return self.__visible
    @visible.setter
    def visible(self, vis:bool) -> None:
        """
        Is the object shown in the visualisation of the sandbox, if false, the object will be hidden
        :param vis: True: object will be visible, False: otherwise
        """
        self.__visible = vis

    @property
    def selected(self) -> bool:
        """
        Is the object selected by the user, this may lead to a different visual interpretataion of the object (marked)
        :return: True: the object is selected, False: otherwise
        """
        return self.__selected
    @selected.setter
    def selected(self, sel:bool) -> None:
        """
        Is the object selected by the user, this may lead to a different visual interpretataion of the object (marked)
        :param sel: True: the object will be selected, False: otherwise
        """
        self.__selected=sel

    @property
    def hasTexture(self) -> bool:
        """
        Is a Texture id set for this object
        :return: True: a texture is set, False: otherwise
        """
        return self.__hasTexture

    @property
    def textureId(self) -> int:
        """
        Returns the global id of the texture for this object, if there is no texture set, the hasTexture property is  false
        :return: The global Id of the texture which can be found in GlobalConstants.py,
        """
        return self.__textureId
    @textureId.setter
    def textureId(self, id) ->None:
        """
        The global id of the texture for this object,
        :param id:  a texture id from GlobalConstants.py,  if None the hasTexture property is cleared
        """
        if(id is not None):
            self.__textureId = id
            self.__hasTexture=True
        else:
            self.__hasTexture=False

    @property
    def hasMaterial(self) -> bool:
        """
        Is a material set for this object
        :return: True: a material is set, False: otherwise
        """
        return self.__hasMaterial

    @property
    def material(self) -> MaterialType:
        """
        Returns a MaterialType instance for this object, None if ther is no type. to check one can use the hasMaterial property
        :return: the materialType instance for this object
        """
        return self.__material
    @material.setter
    def material(self, mat:MaterialType) -> None:
        """
        Set the MaterialType instance for this object, if none, the matrerial is cleared, check the hasMertial property
        :param mat: the new MaterialType instance
        """
        if(mat is not None):
            self.__material = mat
            self.__hasMaterial=True
        else:
            self.__hasMaterial=False

    @property
    def hasAnchors(self) -> bool:
        """
        has the object some anchors?
        :return: are there anchors on the object?
        """
        if(len(self.__objectAnchors) == 0):
            return False
        else:
            return True

    @property
    def anchors(self):
        """
        Returns a Dictonary of all anchors of this object
        :return: dict of all the anchors of this object
        """
        return self.__objectAnchors

    @anchors.setter
    def anchors(self, anch:dict) -> None:
        """
        updates the dictonary of the anchors of this object
        :param anch: new dict of anchors
        """
        return self.__objectAnchors.update(anch)


    # ------------------------------------------------------------ #

    def getObjectID(self) -> str:
        return self.objectId

#    def getCoordinates(self):
#        return self.coordinates

    def getObservable(self, key:str):
        return 0

    def getObservableType(self, key:str):
        return UnitType.OTHER


    def show(self):
        self.visible=True

    def hide(self):
        self.visible=False

    def setTextureId(self, id):
        if(id is not None):
            self.textureId = id
            self.__hasTexture=True

    def addAnchor(self, anchor) -> None:
        if(anchor is not None):
            self.__objectAnchors.update({anchor.objectId : anchor})


    def getObservableDictionary(self):
        return self.observableDictionary

    def getDescription(self) -> str:
        ''' intern description for debugging '''
        return "This is a basic Object!"

    def isMoveable(self):
        return self.moveable

    def setMoveable(self, ma:bool):
        self.moveable = ma

    def __str__(self):
        return self.getDescription()

