#class Name:  HomogenousGravity
#Author: djk

import numpy as np
from Objects.forceFields.BasicForceField import *

class HomogenousGravity(BasicForceField):
    """ Prototpye of a ForceField used in Mechanicla Sandboxes
    """



    def __init__(self, strength:float, direction:list,  id:str):
        """ constructor         """
        super( HomogenousGravity, self ).__init__(id)
        #unit vector of the field
        self.forceDirection = None
        #strenght of the field
        self.strength = None

        self.strength = strength
        self.forceDirection = np.array(direction)#/np.linalg.norm(direction)
        print("HomogenousGravity created")

    def getForce(self, pos):
        return self.strength*self.forceDirection

    def getDescription(self) -> str:
        ''' intern description for debugging '''
        return "This is a homogenous gravitational field in direction: " + np.array_str(self.forceDirection) + " with the strength " + str(self.strength)

    def __str__(self):
        return self.getDescription()

