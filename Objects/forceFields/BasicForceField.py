#class Name:  BasicForceField
#Author: djk

import numpy as np
from Objects.BasicObject import *
from Objects.MassPoint import *

class BasicForceField(BasicObject):
    """ Prototpye of a ForceField used in Mechanicla Sandboxes
    """

    def __init__(self, id:str):
        """ constructor         """
        super( BasicForceField, self ).__init__(id)
        print("BasicForceField created")

    def getObjectID(self) -> str:
        return self.objectId

    def getDescription(self) -> str:
        ''' intern description for debugging '''
        return "This is a basic ForceField!"

    def getForce(self, pos):
        return np.array([0.0,0.0])

    def updateForceOnObject(self, obj):
        if(isinstance(obj, MassPoint)):
            obj.addCenterOfMassForce(obj.getMass()*self.getForce(obj.getCenterOfMass()))

    def __str__(self):
        return self.getDescription()

