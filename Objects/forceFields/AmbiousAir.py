#class Name:  HomogenousGravity
#Author: djk

import numpy as np
from Objects.forceFields.BasicForceField import *
from math import *

class AmbiousAir(BasicForceField):
    """ Prototpye of a ForceField used in Mechanicla Sandboxes
    """



    def __init__(self, density:float,  id:str):
        """ constructor         """
        super( AmbiousAir, self ).__init__(id)
        self.density=density
        self.dragType =  "DT_STOKES"  #  "DT_NEWTON",  "DT_STOKES"


        print("Ambious Air created")

    def getObservableType(self, key:str):
        if key=="DENSITY":
            return UnitType.DENSITY
        else:
            return super().getObservableType(key)

    def getObservable(self, key:str):
        if key=="DENSITY":
            return self.density
        else:
            return 0

    def setObservable(self, key:str, val):
        if key=="DENSITY":
            self.density = val
        else:
            return False

        return True

    #ToDo: set the correct prefactors!
    def getStokesDrag(self, velocity):
        #print(velocity, -12*self.density*velocity)
        return -12*self.density*velocity

    #ToDo: set the correct prefactors!
    def getNewtonDrag(self, velocity):
        absV = sqrt(velocity[0]*velocity[0]+velocity[1]*velocity[1])
        #print(velocity, -self.density*absV*velocity)
        return -self.density*absV*velocity

    def setDragType(self, dt:str):
        if(dt == "DT_STOKES" or dt == "DT_NEWTON"):
            self.dragType = dt


    def updateForceOnObject(self, obj):
        if(isinstance(obj, MassPoint)):
            if(self.dragType=="DT_STOKES"):
                obj.addCenterOfMassForce(self.getStokesDrag(obj.getCenterOfMassVelocity()))
            if(self.dragType=="DT_NEWTON"):
                obj.addCenterOfMassForce(self.getNewtonDrag(obj.getCenterOfMassVelocity()))

    def getDescription(self) -> str:
        ''' intern description for debugging '''
        return "This is a homogenous ambous air with the density " + str(self.density)

    def __str__(self):
        return self.getDescription()

