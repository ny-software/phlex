#class Name:  CentralGravity
#Author: djk

import numpy as np
from Objects.forceFields.BasicForceField import *

class CentralGravity(BasicForceField):
    """ Prototpye of a ForceField used in Mechanicla Sandboxes
    """



    def __init__(self, strength:float, center:list,  id:str):
        """ constructor         """
        super( CentralGravity, self ).__init__(id)
        #unit vector of the field
        self.center = None
        #strenght of the field
        self.strength = None

        self.strength = strength
        self.center = np.array(center)
        print("CentralGravity created")


    def move(self, dx, dy):
        self.center[0] += dx
        self.center[1] += dy

    def getCenter(self):
        return self.center

    def getStrength(self):
        return self.strength

    def getForce(self, pos):
        vec = self.center-pos
        dist2 = sum(vec*vec)
        return np.array(self.strength*vec)/dist2

    def getDescription(self) -> str:
        ''' intern description for debugging '''
        return "This is a central gravitational field with center: " + np.array_str(self.center) + " and the strength " + str(self.strength)

    def __str__(self):
        return self.getDescription()