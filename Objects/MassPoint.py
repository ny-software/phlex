#class Name:  BasicObject
#Author: djk

from Objects.BasicObject import *
from Objects.EventSubject import *
import numpy as np
from math import *
from Objects.Conectors.MechanicAnchor import *
from Units.UnitTypes import *

class MassPoint(BasicObject,EventSubject):
    """ Prototpye of an Object for a Sanbox Unit
    """
    massPointCounter=0
    #dictionarry which contains all atributes for this object
    massPointDictionary = {"TIME":"Time", "MASS":"Mass", "COM_XCOORD":"x-Coordinate", "COM_YCOORD":"y-Coordinate", "COM_XVEL":"x-Velocity", "COM_YVEL":"y-Velosicty", "COM_X_INITIAL_VEL":"x-Initial-Velocity", "COM_Y_INITIAL_VEL":"y-Initial-Velosicty"}

    def __init__(self, id:str=None):
        """ constructor         """
        EventSubject.__init__(self)

        MassPoint.massPointCounter+=1
        if(id==None):
            id="mp_"+str(MassPoint.massPointCounter)
        super( MassPoint, self ).__init__(id)

        #physical properties:
        #local time
        self.time=0.0
        #mass of the object in kg
        self.mass=1.0
        #center of mass
        self.coordinates = np.array([0.,0.])
        #initial velocity
        self.centerOfMassInitialVelocity = np.array([0.,0.])
        #current velocity
        self.centerOfMassVelocity = np.array([0.,0.])
        #current Force acting on the particle
        self.centerOfMassForce = np.array([0.,0.])
        #just before the final simulation step, the background force is copied into centerOfMassForce
        #this prevents jittering of the force vectors due to ploting!
        self.centerOfMassForceBackground = np.array([0.,0.])

        #visulaisation properties
        self.centerOfMassVelocityVectorVisibleSplitting = False
        self.centerOfMassVelocityVectorVisible = False
        self.centerOfMassForceVectorVisibleSplitting = False
        self.centerOfMassForceVectorVisible = False

        self.observableDictionary = self.observableDictionary.update(self.massPointDictionary)

        #central mechanic anchor
        anchor = MechanicAnchor(self)
        anchor.setRelativeCoordinates(np.array([0,0]))
        self.addAnchor(anchor)

    def initialize(self) -> None:
        """
        Here values can be set before the simulation starts
        """
        #set the particle velocity to the initial velocity
        print("init in", self)
        self.setCenterOfMassVelocity([self.centerOfMassInitialVelocity[0], self.centerOfMassInitialVelocity[1]])
        print(self.getCenterOfMassVelocity())

    def getObservableType(self, key:str):
        if key=="TIME":
            return UnitType.TIME
        elif key=="MASS":
            return UnitType.MASS
        elif key=="COM_XCOORD" or key=="COM_YCOORD":
            return UnitType.DISTANCE
        elif key=="COM_XVEL" or key=="COM_YVEL" or key=="COM_X_INITIAL_VEL" or key=="COM_Y_INITIAL_VEL":
            return UnitType.VELOCITY
        else:
            return super().getObservableType(key)

    def getObservable(self, key:str):
        if key=="TIME":
            return self.time
        elif key=="MASS":
            return self.mass
        elif key=="COM_XCOORD":
            return self.coordinates[0]
        elif key=="COM_YCOORD":
            return self.coordinates[1]
        elif key=="COM_XVEL":
            return self.getCenterOfMassVelocity()[0]
        elif key=="COM_YVEL":
            return self.getCenterOfMassVelocity()[1]
        elif key=="COM_X_INITIAL_VEL":
            return self.getCenterOfMassInitalVelocity()[0]
        elif key=="COM_Y_INITIAL_VEL":
            return self.getCenterOfMassInitalVelocity()[1]
        else:
            return 0

    def getMass(self):
        return self.mass

    def getCenterOfMass(self):
        return self.coordinates

    def getCenterOfMassVelocity(self):
        return self.centerOfMassVelocity

    def getCenterOfMassInitalVelocity(self):
        return self.centerOfMassInitialVelocity

    def getCenterOfMassForce(self):
        return self.centerOfMassForce

    def getCenterOfMassForceBackground(self):
        return self.centerOfMassForceBackground

    def setCenterOfMassForceVectorSplitting(self, sp):
        self.centerOfMassForceVectorVisibleSplitting = sp

    def setCenterOfMassVelocityVectorSplitting(self, sp):
        self.centerOfMassVelocityVectorVisibleSplitting = sp

    def isCenterOfMassForceSplitted(self):
        return self.centerOfMassForceVectorVisibleSplitting

    def isCenterOfMassVelocitySplitted(self):
        return self.centerOfMassVelocityVectorVisibleSplitting

    def showCenterOfMassVelocityVector(self):
        self.centerOfMassVelocityVectorVisible = True

    def hideCenterOfMassVelocityVector(self):
        self.centerOfMassVelocityVectorVisible = False

    def isCenterOfMassVelocityVectorVisible(self):
        return self.centerOfMassVelocityVectorVisible

    def showCenterOfMassForceVector(self):
        self.centerOfMassForceVectorVisible = True

    def hideCenterOfMassForceVector(self):
        self.centerOfMassForceVectorVisible = True

    def isCenterOfMassForceVectorVisible(self):
        return self.centerOfMassForceVectorVisible

    def setObservable(self, key:str, val):
        if key=="TIME":
            self.time = val
        elif key=="MASS":
            self.mass = val
        elif key=="COM_XCOORD":
            self.coordinates[0] = val
        elif key=="COM_YCOORD":
            self.coordinates[1] = val
        elif key=="COM_XVEL":
            self.getCenterOfMassVelocity()[0] = val
        elif key=="COM_YVEL":
            self.getCenterOfMassVelocity()[1] = val
        elif key=="COM_X_INITIAL_VEL":
            self.getCenterOfMassInitalVelocity()[0] = val
        elif key=="COM_Y_INITIAL_VEL":
            self.getCenterOfMassInitalVelocity()[1] = val
        else:
            return False

        return True

    def setMass(self, mass):
        self.mass = mass

    def setCenterOfMass(self, com:list):
        """
        changes the center of mass and afterwards sends a notification to all observers
        :param com: new center of mass as a list
        :return: none
        """
        self.coordinates = np.array(com)
        self.notifyObservers()

    def setCenterOfMassVelocity(self, vel:list):
        self.centerOfMassVelocity = np.array(vel)
        self.notifyObservers()

    def resetCenterOfMassForce(self):
        self.centerOfMassForceBackground = np.array([0,0])

    def setCenterOfMassForce(self, comF:list):
        if(self.isMoveable()):
            self.centerOfMassForce = np.array(comF)
            self.centerOfMassForceBackground = np.array(comF)
            self.notifyObservers()

    def updateCenterOfMassForce(self):
        self.centerOfMassForce = self.centerOfMassForceBackground
        self.notifyObservers()

    def flipVelocityAlongPlane(self, nv:list, factor:float=1.0) -> None:
        """
        If this objects hits a plane (which is not checkt in this method) its velocity flipts along the plane given by a normalvector nv. An addition bounciness factor can be given here to enhance or damp the flip
        :param nv: the planes normal vector
        :param factor: additional bounciness factor
        """
        nv = nv/sqrt(nv[0]*nv[0]+nv[1]*nv[1])
        self.centerOfMassVelocity  -=  np.array([2.0*nv[0], (1.0+factor)*nv[1]])*sum(nv*self.centerOfMassVelocity)

    def move(self, dx, dy):
        self.coordinates[0] += dx
        self.coordinates[1] += dy

    def addCenterOfMassForce(self, comF:list):
        if(self.isMoveable()):
            self.centerOfMassForceBackground = np.add(self.centerOfMassForceBackground,np.array(comF))

    def getDescription(self) -> str:
        ''' intern description for debugging '''
        return "This is the " + self.objectId +  " MassPoint Object with the coordinates " + np.array_str(self.getCenterOfMass())

    def __str__(self):
        return self.getDescription()

