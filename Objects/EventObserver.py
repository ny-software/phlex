#class Name:    EventObserver
#Author: djk

#from objects.EventSubject import *

class EventSubjectPrototype:
    pass

class EventObserver:
    """
    Basic Observer class for an Event
    This class contains all methods needed for this pattern, but may have to be overwritten
    """

    def notifyEvent(self, subject:EventSubjectPrototype):
        pass

