
from Units.Unit import *



class BasicQuantity:
    """
    This class will be used for representation of physical quantities which allway are composed by a number and a coresponding unit
    There will be conversions for basic types as float for easy handling of the data structure, as well as overloaded operators for basic math
    """
    def __init__(self, val:float, un:Unit) -> None:
        """
        Creates a new Basic Quantity
        :param val: float value for this quantity
        :param un: the unit of this quantity
        """
        self._value=val
        self._unit=un


    @property
    def value(self) -> float:
        return self._value
    @value.setter
    def value(self, val: float) -> None:
        self._value = val

    @property
    def unit(self) -> Unit:
        return self._unit
    @unit.setter
    def unit(self, un: Unit) -> None:
        self._unit = un

    def __add__(self, other):
        if (not isinstance(other, BasicQuantity)):
            raise TypeError
        #if(self._unit==other.unit):
        return BasicQuantity(self._value+other.value, self._unit)

    def __radd__(self, other):
        return self.__add__(other)

    def __eq__(self, other):
        """
        Two Quantities are equal, if the unit is the same and the value is equal, otherwise we have to convert them to the same unit(if possible) and compare the values aggain
        ToDo: implement conversion part
        :param other: the Quantity to compare with
        :return: boolean: true if equal, otherwise false
        """
        if(isinstance(other, BasicQuantity)):
            if(self._value == other.value):
                return True
        return False
        #ToDo: implement conversion