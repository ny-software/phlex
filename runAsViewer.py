from PySideGui.mainViewer import *
import sys


def main():
    app = QtGui.QApplication(sys.argv)
    ex = mainViewer()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
